#!/bin/bash

clear
mkdir Build
cd Build
cmake ../Source
make simple_mesh -j9
cd Examples
./simple_mesh

