## CoST

This codebase contains functionality for generating, manipulating, and analyzing CoST microstructure.

#### Dependencies

[Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page)

[CMake](https://cmake.org/download/)

#### Building

On Unix systems, the project can be built and ran by running the script `build_and_run.sh`.

If you wish to manually build, follow these steps:

```bash
mkdir Build
cd Build
cmake ../Source
make simple_mesh -j9
cd Examples
./simple_mesh
```

This project has been tested on Ubuntu 18.04 and Mac OSX.

The output is the single executable `Build/Examples/simple_mesh`.  It takes no arguments and must be called from the `Build/Examples` directory for it to find its dependencies.

#### Output

All the output can be found in the `Build/Examples/demo` directory.

`tetrahedralCoST.obj` and `tetrahedralCoSTLarger.obj` are two different sizes of a tetrahedral domain which has been filled with a CoST microstructure.

`boundaryStiffening.obj` shows the boundary stiffening algorithm applied to the CoST object represented in `tetrahedralCoST.obj`.

In folder `Refinement` there are

- `refinement1Once.obj`, `refinement1Twice.obj` which show the effect of performing the first refinement strategy of subdividing each tetrahedra.
- `refinement2.obj` which shows that effect of performing the second refinement strategy of replacing each tetrahedron of the original CoST microstructure with another CoST object.

In folder `RigidityMatrices` there are

- `dense_rigidity_matrix.txt`, `sparse_rigidity_matrix.txt` which are two different file formats for the rigidity matrix of the object represented in `tetrahedralCoST.obj`.
- `forces.txt` which is the force vector we apply to the CoST.  Each vertex of the CoST object has three rows in `forces.txt`, one each for the `x`, `y`, and `z` components.  The force vector here simply pushes down on a single vertex.
- `solution.txt` , `solution_normalized.txt` which are the solutions to the system F + omega*R = 0 for omega where F is the applied force vector and R is the rigidity matrix.  The solution omega is then the stress per unit length on each edge.
- `edges.csv` represents the edges of the CoST we did this rigidity analysis on.  It is used for visualization purposes and is a custom file format.
- `stress_visualization.m` is a Matlab script which reads in `edges.csv` and `solution.txt` and then outputs a figure of the CoST with edges colored corresponding to the stress on them such that edges in tension are colored green and edges in compression are colored red.

In folder `tetComplex` there are

- `tetrahedralization.obj` which is an obj file of the input tetrahedral complex.  Here we fill the tetrahedral complex by putting a CoST within each of its tetrahedra.  Its original file can be found in `Build/Examples/test_files/degree_3_no_negative_detJ_0052.hom`.
- Folder `Bracket` , which contains
  - `tet_*.obj` which is the CoST for tetrahedra `*`.  Together they make up a CoST microstructure for the input file `tetrahedralization.obj`.  These can be [batch imported into Blender](https://blender.stackexchange.com/a/31825) for viewing.