
hold on;

edges = csvread('edges.csv');
X = edges( :, 1);
Y = edges( :, 2);
Z = edges( :, 3);

[num_edges, dim] = size(edges);
num_edges = num_edges/2;

stresses = csvread('solution.txt');

true_stresses = zeros(num_edges, 1);
for i = 1:num_edges
    dX = X(2*i) - X(2*i-1);
    dY = Y(2*i) - Y(2*i-1);
    dZ = Z(2*i) - Z(2*i-1);
    length = sqrt(dX*dX + dY*dY + dZ*dZ);
    true_stresses(i) = stresses(i) * length;
end

max_stress = max(true_stresses);
min_stress = min(true_stresses);

num_colors = num_edges;
scaled_true_stresses = true_stresses - min(true_stresses);
scaled_true_stresses = scaled_true_stresses / max(scaled_true_stresses);
color_idx = num_colors * scaled_true_stresses;
color_idx = ceil(color_idx);

colors = jet(num_colors);

red = [1 0 0];   % compression
green = [0 1 0]; % tension
grey = [0.5 0.5 0.5];
for i=1:num_edges
    color = [0 0 0];
    if true_stresses(i) > 0 % tension = green
        alpha = true_stresses(i)/max_stress;
        color = alpha^2*green + 2*alpha*(1-alpha)*green + (1-alpha)^2*[0.2 0.6 0.2];
    else  % compression = red
        alpha = true_stresses(i)/min_stress;
        color = alpha^2*red + 2*alpha*(1-alpha)*red + (1-alpha)^2*[0.6 0.2 0.2];
    end
    
    plot3(X(2*i-1:2*i), Y(2*i-1:2*i), Z(2*i-1:2*i), 'Color', color, 'LineWidth', 2.0);
end


hold off;