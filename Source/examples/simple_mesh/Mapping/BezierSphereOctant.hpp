/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHERE_OCT_HH
#define SPHERE_OCT_HH

#include <iostream>
#include <array>
#include <vector>
#include <tuple>
#include <math.h>

#include <OpenVolumeMesh/Geometry/VectorT.hh>

typedef OpenVolumeMesh::Geometry::Vec3f Vec3f;

/*
 *  This class constructs a total-degree rational Bezier
 *  volume which exactly fits an octant of the sphere.
 *
 *  It is based on the paper "The octanct of a sphere as a
 *  non-degenerate triangular Bezier patch" by G. Farin et al
 *  https://sci-hub.tw/10.1016/0167-8396(87)90007-0
 */

class BezierSphereOctant
{
public:
    BezierSphereOctant(double a_Radius = 1.0);
    Vec3f operator()(std::tuple<double, double, double> a_Coords, std::tuple<int, int, int> a_Octant={1,1,1});
private:
    const double m_Radius;
    const int m_Degree;
    std::vector<double> m_Weights;
    std::vector<Vec3f> m_Cpts;

    template<typename T>
    T deCasteljau(std::vector<T> a_Cpts, int a_Degree, double u, double v);
    template<typename T>
    std::vector<T> deCasteljauStep(std::vector<T> a_Cpts, int a_Degree, double u, double v);
};


#endif // SPHERE_OCT_HH
