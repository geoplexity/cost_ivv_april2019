/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/


#include "BezierSphereOctant.hpp"
#include <iomanip>

using namespace std;

/*
 *  Layout of indexing:
 *
 *                       0=(0,0,4)
 *
 *                  1=(3,1,0)  2=(3,0,1)
 *
 *            3=(2,2,0)  4=(2,1,1)  5=(2,0,2)
 *
 *        6=(1,3,0) 7=(1,2,1) 8=(1,1,2) 9=(1,0,3)
 *
 * 10=(0,4,0) 11=(0,3,1) 12=(0,2,2) 13=(0,1,3) 14=(0,0,4)
 */
BezierSphereOctant::BezierSphereOctant(double a_Radius) : m_Radius(a_Radius), m_Degree(5)
{
    // Make some space
    m_Weights.resize(15);
    m_Cpts.resize(15);

    /*
     *  Initialize the weight array
     */
    const double t_w040 = 4.0*sqrt(3.0)*(sqrt(3.0)-1.0);
    const double t_w031 = 3.0*sqrt(2.0);
    const double t_w202 = 4.0;
    const double t_w121 = (sqrt(2)/sqrt(3.0))*(3.0 + 2.0*sqrt(2.0) - sqrt(3.0));

    m_Weights[0]  = t_w040;
    m_Weights[1]  = t_w031;
    m_Weights[2]  = t_w031;
    m_Weights[3]  = t_w202;
    m_Weights[4]  = t_w121;
    m_Weights[5]  = t_w202;
    m_Weights[6]  = t_w031;
    m_Weights[7]  = t_w121;
    m_Weights[8]  = t_w121;
    m_Weights[9]  = t_w031;
    m_Weights[10] = t_w040;
    m_Weights[11] = t_w031;
    m_Weights[12] = t_w202;
    m_Weights[13] = t_w031;
    m_Weights[14] = t_w040;

    /*
     *  Initialize the control points
     */
    const double t_Alpha = (sqrt(3.0)-1.0) / sqrt(3.0);
    const double t_Beta  = (sqrt(3.0)+1.0) / (2.0*sqrt(3.0));
    const double t_Gamma = 1 - ((5.0-sqrt(2.0))*(7.0-sqrt(3.0)))/46.2;
    
    m_Cpts[0]  = m_Radius*Vec3f{ 0.0    , 0.0    , 1.0     };
    m_Cpts[1]  = m_Radius*Vec3f{ t_Alpha, 0.0    , 1.0     };
    m_Cpts[2]  = m_Radius*Vec3f{ 0.0    , t_Alpha, 1.0     };
    m_Cpts[3]  = m_Radius*Vec3f{ t_Beta , 0.0    , t_Beta  };
    m_Cpts[4]  = m_Radius*Vec3f{ t_Gamma, t_Gamma, 1.0     };
    m_Cpts[5]  = m_Radius*Vec3f{ 0.0    , t_Beta , t_Beta  };
    m_Cpts[6]  = m_Radius*Vec3f{ 1.0    , 0.0    , t_Alpha };
    m_Cpts[7]  = m_Radius*Vec3f{ 1.0    , t_Gamma, t_Gamma };
    m_Cpts[8]  = m_Radius*Vec3f{ t_Gamma, 1.0    , t_Gamma };
    m_Cpts[9]  = m_Radius*Vec3f{ 0.0    , 1.0    , t_Alpha };
    m_Cpts[10] = m_Radius*Vec3f{ 1.0    , 0.0    , 0.0     };
    m_Cpts[11] = m_Radius*Vec3f{ 1.0    , t_Alpha, 0.0     };
    m_Cpts[12] = m_Radius*Vec3f{ t_Beta , t_Beta , 0.0     };
    m_Cpts[13] = m_Radius*Vec3f{ t_Alpha, 1.0    , 0.0     };
    m_Cpts[14] = m_Radius*Vec3f{ 0.0    , 1.0    , 0.0     };

    // Apply weighting
    for(int i=0; i<15; ++i){
        m_Cpts[i] *= m_Weights[i];
    }

} // Constructor

/*
 *  Evaluates the volumetric function.
 */
Vec3f BezierSphereOctant::operator()(tuple<double, double, double> a_Coords, std::tuple<int, int, int> a_Octant)
{
    double u = get<0>(a_Coords);
    double v = get<1>(a_Coords);
    double w = get<2>(a_Coords);

    double octX = (double)get<0>(a_Octant);
    double octY = (double)get<1>(a_Octant);
    double octZ = (double)get<2>(a_Octant);

    Vec3f t_Point = deCasteljau(m_Cpts, m_Degree, u, v);
    t_Point[0] *= octX;
    t_Point[1] *= octY;
    t_Point[2] *= octZ;

    double t_Weight= deCasteljau(m_Weights, m_Degree, u, v);

    return w*(t_Point/t_Weight);
    //return (t_Point/t_Weight);
}

/*
 *  Applys deCasteljau's algorithm.
 *
 *  Follows https://plot.ly/ipython-notebooks/bezier-triangular-patches/#the-de--casteljau-algorithm-for-computing---points-on-a-triangular-bzier-patch
 */
template<typename T>
T BezierSphereOctant::deCasteljau(vector<T> a_Cpts, int a_Degree, double u, double v)
{
    assert(a_Degree > 0);
    //assert(u >= 0.0 && u <= 1.0);
    //assert(v >= 0.0 && v <= 1.0);
    //assert(u+v <= 1.0);
    
    if(a_Cpts.size() > 1){
        return deCasteljau(deCasteljauStep(a_Cpts, a_Degree-1, u, v), a_Degree-1, u, v);
    }
    else{
        return a_Cpts[0];
    }
}

/*
 *  Recursive step in deCasteljau
 */
template<typename T>
vector<T> BezierSphereOctant::deCasteljauStep(vector<T> a_Cpts, int a_Degree, double u, double v)
{
    int i=0;
    int j=1;

    for(int nr = 1; nr < a_Degree+1; ++nr)
    {
        for(int k = 0; k < nr; ++k)
        {
            a_Cpts[i] = u*a_Cpts[i] + v*a_Cpts[j] + (1.0-u-v)*a_Cpts[j+1];
            i++;
            j++;
        }
        j++;
    }
    
    vector<T> t_NewCpts;
    for(size_t idx = 0; idx < a_Cpts.size()-(a_Degree+1); ++idx){
        t_NewCpts.push_back(a_Cpts[idx]);
    }

    return t_NewCpts;
}





















