/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAPPING_HH
#define MAPPING_HH

#include <cmath>

#include "../connectivity.hpp"
#include "../stiffening.hpp"

#include <OpenVolumeMesh/Mesh/PolyhedralMesh.hh>
#include <OpenVolumeMesh/Mesh/TetrahedralMesh.hh>

#include "BezierSphereOctant.hpp"

// Make some typedefs to facilitate your life
typedef OpenVolumeMesh::Geometry::Vec3f         Vec3f;
typedef OpenVolumeMesh::Geometry::Vec4f         Vec4f;
typedef OpenVolumeMesh::GeometricPolyhedralMeshV3f   TetMeshV3f;
typedef OpenVolumeMesh::GeometricTetrahedralMeshV3f  TetrahedralMesh;

using namespace std;
using namespace OpenVolumeMesh;

namespace Mapping {


    
double scalar_triple_product(Vec3f a_Vec1, Vec3f a_Vec2, Vec3f a_Vec3)
{
    return dot( a_Vec1, cross(a_Vec2, a_Vec3));
    //return dot( cross(a_Vec1, a_Vec2), a_Vec3);
}

Vec3f cross(Vec3f a_Vec1, Vec3f a_Vec2)
{
    Vec3f t_CrossProduct;
    t_CrossProduct[0] = a_Vec1[1]*a_Vec2[2] - a_Vec1[2]*a_Vec2[1];
    t_CrossProduct[1] = a_Vec1[2]*a_Vec2[0] - a_Vec1[0]*a_Vec2[2];
    t_CrossProduct[2] = a_Vec1[0]*a_Vec2[1] - a_Vec1[1]*a_Vec2[0];

    return t_CrossProduct;
}

/*
 *  Given a ray with its origin and a plane defined by a triangle,
 *  find the point on the plane which intersects with the ray.
 *
 *  See:
 *  https://stackoverflow.com/questions/7168484/3d-line-segment-and-plane-intersection
 *  https://math.stackexchange.com/questions/83990/line-and-plane-intersection-in-3d
 */
Vec3f find_intersection(const Vec3f a_Ray, const Vec3f a_RayOrigin, const vector<Vec3f> a_Triangle)
{
    Vec3f t_Normal = cross(a_Triangle[1]-a_Triangle[0], a_Triangle[2]-a_Triangle[0]);
    double t_Distance = dot(t_Normal, a_Triangle[0]); // Distance from origin
    double t_nDotOrigin = dot(t_Normal, a_RayOrigin);
    double t_nDotRay = dot(t_Normal, a_Ray);
    if(abs(t_nDotRay) < 10e-6){
        return {0.0, 0.0, 0.0};
    }
    Vec3f t_Intersection = a_RayOrigin + (((t_Distance - t_nDotOrigin)/t_nDotRay)*a_Ray);
    return t_Intersection;
}

/*
 *  Given a triangle and a point lying on the plane defined by
 *  that triangle, compute its barycentric coordinates.
 *
 *  See:
 *  https://gamedev.stackexchange.com/questions/23743/whats-the-most-efficient-way-to-find-barycentric-coordinates
 */
pair<double, double> find_triangle_bary_coords(const Vec3f a_Point, const vector<Vec3f> a_Triangle)
{
    Vec3f t_Normal = cross(a_Triangle[1]-a_Triangle[0], a_Triangle[2]-a_Triangle[0]);
    Vec3f a = a_Triangle[0];
    Vec3f b = a_Triangle[1];
    Vec3f c = a_Triangle[2];
    Vec3f p = a_Point;
    // scalar_triple_product(a, b, c) = (axb)*c
    double t_AreaABC = scalar_triple_product(b-a, c-a, t_Normal);
    double t_AreaPBC = scalar_triple_product(b-p, c-p, t_Normal);
    double t_AreaPCA = scalar_triple_product(c-p, a-p, t_Normal);
    double u = t_AreaPBC / t_AreaABC;
    double v = t_AreaPCA / t_AreaABC;
    return {u, v};
}

/*
 *  See https://stackoverflow.com/questions/38545520/barycentric-coordinates-of-a-tetrahedron
 */
array<double, 4> find_tetrahedron_bary_coords(const Vec3f a_Point, const vector<Vec3f> a_Tet)
{
    Vec3f a = a_Tet[0];
    Vec3f b = a_Tet[1];
    Vec3f c = a_Tet[2];
    Vec3f d = a_Tet[3];
    Vec3f p = a_Point;

    Vec3f vap = p-a;
    Vec3f vbp = p-b;

    Vec3f vab = b-a;
    Vec3f vac = c-a;
    Vec3f vad = d-a;

    Vec3f vbc = c-b;
    Vec3f vbd = d-b;
    float va6 = scalar_triple_product(vbp, vbd, vbc);
    float vb6 = scalar_triple_product(vap, vac, vad);
    float vc6 = scalar_triple_product(vap, vad, vab);
    float vd6 = scalar_triple_product(vap, vab, vac);
    float v6 = 1.0 / scalar_triple_product(vab, vac, vad);

    return {va6*v6, vb6*v6, vc6*v6, vd6*v6};
}

/*
 *  This does not compute traditional barycentric coordinates since
 *  the sphere mapping does not use traditional barycentric coordinates.
 *
 *  Instead, view the tetrahedron as a triangular cone with vertex ("origin")
 *  a_TetCorners[0].  This can be viewed as taking the triangle formed
 *  by the other three corners and scaling it by it's distance from the vertex.
 *
 *  Given a point a_Vec, the coordinates we are then looking for are the
 *  traditional barycentric coordinated of it with respect to the swept triangle
 *  when a_Vec lies on it for the u,v coords and then the proportion of the distance
 *  from the vertex to a_Vec with respect to the distance between the vertex and
 *  the far face of the tetrahedron for the w coord.
 */
tuple<double, double, double> find_sphere_map_coords(const Vec3f a_Vec, const vector<Vec3f> a_TetCorners)
{
    /*
     *  Initialize some data.
     */
    vector<Vec3f> t_FarFace{a_TetCorners[1], a_TetCorners[2], a_TetCorners[3]};
    Vec3f t_Origin = a_TetCorners[0];
    Vec3f t_Ray = a_Vec - t_Origin;

    if(t_Ray.norm() < 10e-8){
        return {0.0, 0.0, 0.0};
    }

    /*
     *  Find intersection with far face
     */
    Vec3f t_Intersection = find_intersection(t_Ray, t_Origin, t_FarFace);

    /*
     *  Find barycentric coordinates of the intersection point
     *  on the far face.
     */
    auto [u, v] = find_triangle_bary_coords(t_Intersection, t_FarFace);

    /*
     *  Find relative distance of a_Vec along
     *  line segment from t_Origin to t_Intersection
     */
    double w = (a_Vec - t_Origin).norm() / (t_Intersection-t_Origin).norm();

    return {u, v, w};
}


/*
 *  Maps
 */

/*
 *  Map into a sphere.
 */
TetMeshV3f sphere_octant(const TetMeshV3f& a_Mesh, const tuple<int, int, int> a_Octant = {1,1,1}, const double a_Radius = 1.0)
{
    BezierSphereOctant t_SphereMap{a_Radius};
    TetMeshV3f t_SphereMesh = a_Mesh;

 
    /*
     * From blender:
     *      Multiply y by -1
     *      Then swap y and z
     */

    vector<Vec3f> t_TetCorners;
    t_TetCorners.push_back({ 5.5, 4.33012,  10.38716});
    t_TetCorners.push_back({ 11.99604,  0.57962, -0.86431});
    t_TetCorners.push_back({ 5.5,  11.8311, -0.86431});
    t_TetCorners.push_back({ -0.99604,  0.57962, -0.86431});

    for(auto v_it = t_SphereMesh.vertices_begin(); v_it != t_SphereMesh.vertices_end(); ++v_it)
    {
        Vec3f t_Vert = t_SphereMesh.vertex(*v_it);
        auto [u, v, w] = find_sphere_map_coords(t_Vert, t_TetCorners);
        t_Vert = t_SphereMap({u, v, w}, a_Octant); // apply mapping
        t_SphereMesh.set_vertex(*v_it, t_Vert);
    }

    return t_SphereMesh;
}

/*
 *  Given the u, v, w coordinates of the preimage and the control
 *  points of a cubic Bezier tetrahedra, this evaluates the Bezier.
 */
Vec3f deCasteljau(const int a_Deg, const array<double, 4> a_uvw, const vector<Vec3f> a_Cpts)
{
    Vec3f t_MappedPoint;
    vector<Vec3f> t_Cpts = a_Cpts;
    auto [u, v, w, x] = a_uvw;

    //cout << "\n\n\n\ndeCasteljau:\n\n\n";
 
    for(int t_Deg = 3; t_Deg > 0; --t_Deg)
    {
        int idx=0;
        int i=0;
        int j=1;
        int k=3 + (t_Deg-1)*3 + (t_Deg-1)*(t_Deg-2)/2; // num control points in a face of the tet
                             // num vertices + (deg-1 * num edges in a triangle) + num cpts in a triangle
        for(int t_Lvl0 = 0; t_Lvl0 < t_Deg+1; ++t_Lvl0)
        {
            for(int t_Lvl1 = 1; t_Lvl1 < t_Deg+1 - t_Lvl0; ++t_Lvl1)
            {
                for(int t_Lvl2 = 0; t_Lvl2 < t_Lvl1; ++t_Lvl2)
                {
                    t_Cpts[idx] = u*t_Cpts[i] + v*t_Cpts[j] + w*t_Cpts[j+1] + x*t_Cpts[k];
                    //cout << i << ", " << j << ", " << j+1 << ", " << k << endl;
                    idx++;
                    i++;
                    k++;
                    j++;
                }
                //cout << endl;
                j++;
            }
            //cout << endl;
            i += t_Deg+1 - t_Lvl0;
            j = i+1;
        }
        //cout << endl;
    }
    t_MappedPoint = t_Cpts[0];

    return t_MappedPoint;
}

/*
 *  This should not have to be here
 */
template <typename MESHTYPE>
void write_obj(const string out_name, const MESHTYPE& mesh)
{
  std::ofstream out_file;
  out_file.open( out_name+".obj" );

  // Print name of mesh
  out_file << out_name << endl;

  // Print positions of vertices
  for( VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it )
  {
      out_file << "v " << mesh.vertex( *v_it ) << std::endl;
  }

  /*
     Print vertex indices for each face
     Must make sure to not hit faces twice.  Because the polycube mesh does
     not have any cells in it, just faces, we must use a look-up table to
     ensure we don't have duplicate faces.
  */
  set<FaceHandle> visited_faces;
  for( HalfFaceIter hf_it = mesh.halffaces_begin(); hf_it!=mesh.halffaces_end(); ++hf_it )
  {
      // Check for duplicates
      FaceHandle face = mesh.face_handle( *hf_it );
      if( visited_faces.find( face ) != visited_faces.end() ){ // Already wrote out this face
          continue;
      }
      visited_faces.insert( face );

      // Write out
      out_file << "f ";

      for( HalfFaceVertexIter v_it=mesh.hfv_iter( *hf_it ); v_it.valid(); ++v_it )
      {
          out_file << ( ( *v_it ).idx()+1 ) << " ";
      }

      out_file << std::endl;
  }

  for(auto t_EdgeIt = mesh.edges_begin(); t_EdgeIt != mesh.edges_end(); ++t_EdgeIt)
  {
      // If there are any adjacent halffaces, break
      HalfEdgeHandle t_HalfEdge = mesh.halfedge_handle(*t_EdgeIt, 0);
      for(auto t_HalfFace = mesh.hehf_iter(t_HalfEdge); t_HalfFace.valid(); ++t_HalfFace){
          break;
      }
      // This edge is part of the boundary stiffening!
      // Write it out
      OpenVolumeMeshEdge t_Edge = mesh.edge(*t_EdgeIt);
      out_file << "f " << (t_Edge.to_vertex())+1 << " " << (t_Edge.from_vertex())+1 << endl;
  }


  out_file.close();
}

/*
 *  Returns a vector of strings where each string
 *  is a word in the input string.
 */
vector<string> split_string_by_whitespace(string a_String)
{
    vector<string> t_Words;
    istringstream t_ISS(a_String);

    for(string t_Word; t_ISS >> t_Word; ){
        t_Words.push_back(t_Word);
    }

    return t_Words;
}



/****************************************************
 *                                                  *
 *                  MSH Files                       *
 *                                                  *
 ****************************************************/


/*
 *  Reads in a .msh file and returs a set of vertices along
 *  with a set of tetrahedra that index into the vector
 *  of vertices.
 */
pair<vector<Vec3f>, vector<vector<int>>> read_msh_file(const string a_MshFileName)
{
    vector<Vec3f> t_Vertices;
    vector<vector<int>> t_Tets;

    ifstream t_MshFile(a_MshFileName);
    string t_Line;
    // Drop the first two lines
    getline(t_MshFile, t_Line);
    getline(t_MshFile, t_Line);
    getline(t_MshFile, t_Line);

    /*
     *  Process vertices
     */
    while( (t_Line.find("$ENDNOD") == string::npos) )
    {
        vector<string> t_Words = split_string_by_whitespace(t_Line);
        Vec3f t_Vec{stod(t_Words[1]), stod(t_Words[2]), stod(t_Words[3])};
        t_Vertices.push_back(t_Vec);
        getline(t_MshFile, t_Line);
    }

    // Drop the next two lines
    getline(t_MshFile, t_Line);
    getline(t_MshFile, t_Line);
    getline(t_MshFile, t_Line);

    /*
     *  Process tetrahedra
     */
    while( (t_Line.find("$ENDELM") == string::npos) )
    {
        vector<string> t_Words = split_string_by_whitespace(t_Line);
        vector<int> t_Tet;
        for(int t_Vert = 5; t_Vert < 9; ++t_Vert){
            t_Tet.push_back( stoi(t_Words[t_Vert])-1 );
        }
        t_Tets.push_back(t_Tet);

        /*
         *  Now we have the vertices of the tetrahedra.
         *  Time to apply linear map.
         */

        getline(t_MshFile, t_Line);
    }

    return {t_Vertices, t_Tets};
}

/*
 *  Given a linear tetrahedron, fill it with a CoST
 */
TetMeshV3f fill_linear_tet_with_CoST(const TetMeshV3f& a_Mesh, const vector<Vec3f> a_Tet)
{
    TetMeshV3f t_CostTet = a_Mesh;

    /*
     * From blender:
     *      Multiply y by -1
     *      Then swap y and z
     */
    vector<Vec3f> t_TetCorners;
    t_TetCorners.push_back({ 5.5, 4.33012,  10.38716});
    t_TetCorners.push_back({ 11.99604,  0.57962, -0.86431});
    t_TetCorners.push_back({ 5.5,  11.8311, -0.86431});
    t_TetCorners.push_back({ -0.99604,  0.57962, -0.86431});

    for(auto v_it=t_CostTet.vertices_begin(); v_it!=t_CostTet.vertices_end(); ++v_it)
    {
        Vec3f t_Vert = t_CostTet.vertex(*v_it);
        // Find barycentric coordinates with respect to original tetrahedron
        array<double, 4> t_BaryCoords = find_tetrahedron_bary_coords(t_Vert, t_TetCorners);

        // Find new vertex position with respect to new tetrahedron
        Vec3f t_NewVert{0.0, 0.0, 0.0};
        for(int i=0; i<4; ++i)
        {
            t_NewVert += t_BaryCoords[i]*a_Tet[i];
        }
        t_CostTet.set_vertex(*v_it, t_NewVert);
    }

    return t_CostTet;
}

/*
 *  Given a cubic tetrahedron, fill it with a CoST
 *  TODO
 */
TetMeshV3f fill_cubic_tet_with_CoST(const TetMeshV3f& a_CoST, const vector<Vec3f> a_Cpts)
{
    const int t_Deg=3;
    TetMeshV3f t_CostTet = a_CoST;

    /*
     * From blender:
     *      Multiply y by -1
     *      Then swap y and z
     */

    vector<Vec3f> t_TetCorners;
    t_TetCorners.push_back({ 5.5, 4.33012,  10.38716});
    t_TetCorners.push_back({ 11.99604,  0.57962, -0.86431});
    t_TetCorners.push_back({ 5.5,  11.8311, -0.86431});
    t_TetCorners.push_back({ -0.99604,  0.57962, -0.86431});

/*
    t_TetCorners.push_back({ 20.5,  12.99041, 36.22926});
    t_TetCorners.push_back({ 41.88928,  0.64127, -0.81809});
    t_TetCorners.push_back({ 20.5, 37.68856,  -0.81809});
    t_TetCorners.push_back({ -0.88929,  0.64126, -0.81809});
*/

    /*t_TetCorners.push_back({ 20.5, 37.68856,  -0.81809});
    t_TetCorners.push_back({ -0.88929,  0.64126, -0.81809});
    t_TetCorners.push_back({ 41.88928,  0.64127, -0.81809});
    t_TetCorners.push_back({ 20.5,  12.99041, 36.22926});*/

    for(auto v_it=t_CostTet.vertices_begin(); v_it!=t_CostTet.vertices_end(); ++v_it)
    {
        Vec3f t_Vert = t_CostTet.vertex(*v_it);
        // Find barycentric coordinates with respect to original tetrahedron
        array<double, 4> t_BaryCoords = find_tetrahedron_bary_coords(t_Vert, t_TetCorners);

        // Find new vertex position with respect to new tetrahedron
        Vec3f t_NewVert = deCasteljau(t_Deg, t_BaryCoords, a_Cpts);
        t_CostTet.set_vertex(*v_it, t_NewVert);
    }

    return t_CostTet;
}

/*
 *  Reads in a .msh file and writes an obj file with a CoST
 *  for each tetrahedron.
 */
void fill_msh_file_with_CoST(const TetMeshV3f& a_Mesh, const string a_MshFileName)
{
    auto [t_Vertices, t_Tets] = read_msh_file(a_MshFileName);
    string t_OutFileBase{"Bracket/tet_"};

    int t_TetCount = 0;
    for(auto t_Tet : t_Tets)
    {
        vector<Vec3f> t_TetVerts;
        for(auto t_VertID : t_Tet){
            t_TetVerts.push_back(t_Vertices[t_VertID]);
        }
        TetMeshV3f t_CostTet = fill_linear_tet_with_CoST(a_Mesh, t_TetVerts);
        write_obj(t_OutFileBase+to_string(t_TetCount), t_CostTet);
        t_TetCount++;
    }
 
}


/****************************************************
 *                                                  *
 *                  HOM Files                       *
 *                                                  *
 ****************************************************/

/*
 *  Reads in an hom file into a TetMeshV3f which contains
 *  the tetrahedralization and a set of maps from
 *  geometric primitives to control points.
 *
 *  The code is adapted from
 *  https://github.com/sofa-framework/plugin.HighOrder/blob/master/SofaHighOrderTopology/MeshHOMFLoader.cpp
 */
tuple<TetrahedralMesh, map<array<int, 3>, VertexHandle>, map<array<int, 4>, VertexHandle>> read_hom_file(const string a_HomFileName)
{
    const int t_Deg = 3;

    TetrahedralMesh t_HomMesh;
    map<array<int, 3>, VertexHandle> t_EdgePoints;
    map<array<int, 4>, VertexHandle> t_FacePoints;
 
    ifstream t_HomFile(a_HomFileName);

    string t_Line;
    // Drop the first 4 lines
    // We assume that the HOM file version is correct,
    // the embedding dimension and simplex dimesion
    // are both 3, and that the file represents Beziers
    getline(t_HomFile, t_Line);
    getline(t_HomFile, t_Line);
    getline(t_HomFile, t_Line);
    getline(t_HomFile, t_Line);


    // --- Loading Vertices of macro simplicial mesh ---
    getline(t_HomFile, t_Line);
    size_t t_NumVerts = stoi(t_Line);

    // read each input coordinate for each vertex;
    size_t i;
    for (i = 0; i < t_NumVerts; ++i)
    {
        getline(t_HomFile, t_Line);
        auto t_SplitString = split_string_by_whitespace(t_Line);
        vector<double> t_Point;
        for(auto t_Coord : t_SplitString){
            t_Point.push_back(stod(t_Coord));
        }
        t_HomMesh.add_vertex({t_Point[0], t_Point[1], t_Point[2]});
    }


    // --- Loading Simplices of macro simplicial mesh ---

    // ----- load edges ------ 
    getline(t_HomFile, t_Line);
    size_t t_NumEdges = stoi(t_Line);

    for (i = 0; i < t_NumEdges; ++i)
    {
        getline(t_HomFile, t_Line);
        auto t_SplitString = split_string_by_whitespace(t_Line);
        auto t_Vert1 = (VertexHandle)stoi(t_SplitString[0]);
        auto t_Vert2 = (VertexHandle)stoi(t_SplitString[1]);
        t_HomMesh.add_edge(t_Vert1, t_Vert2);
    }

    // ----- load triangles ------ 
    getline(t_HomFile, t_Line);
    size_t t_NumTriangles = stoi(t_Line);

    for (i = 0; i < t_NumTriangles; ++i)
    {
        getline(t_HomFile, t_Line);
        auto t_SplitString = split_string_by_whitespace(t_Line);
        auto t_Vert1 = (VertexHandle)stoi(t_SplitString[0]);
        auto t_Vert2 = (VertexHandle)stoi(t_SplitString[1]);
        auto t_Vert3 = (VertexHandle)stoi(t_SplitString[2]);
        t_HomMesh.add_face({t_Vert1, t_Vert2, t_Vert3});
    }

    // ----- load tetrahedra ------ 
    getline(t_HomFile, t_Line);
    size_t t_NumTets = stoi(t_Line);

    for (i = 0; i < t_NumTets; ++i)
    {
        getline(t_HomFile, t_Line);
        auto t_SplitString = split_string_by_whitespace(t_Line);
        auto t_Vert1 = (VertexHandle)stoi(t_SplitString[0]);
        auto t_Vert2 = (VertexHandle)stoi(t_SplitString[1]);
        auto t_Vert3 = (VertexHandle)stoi(t_SplitString[2]);
        auto t_Vert4 = (VertexHandle)stoi(t_SplitString[3]);
        t_HomMesh.add_cell({t_Vert1, t_Vert2, t_Vert3, t_Vert4});
    }

    // --- Loading index position of control points  ---
    /// loading edge control points indices
    size_t t_NumEdgeControlPoints = (t_Deg-1)*t_NumEdges;

    for (i = 0; i < t_NumEdgeControlPoints; ++i)
    {
        getline(t_HomFile, t_Line);
        auto t_SplitString = split_string_by_whitespace(t_Line);
        auto t_CptIndex = (VertexHandle)stoi(t_SplitString[0]);
        int t_EdgeID = stoi(t_SplitString[1]);
        int t_VertOneWeight = stoi(t_SplitString[2]);
        int t_VertTwoWeight = stoi(t_SplitString[3]);
        array<int, 3> t_EdgePointKey = {t_EdgeID, t_VertOneWeight, t_VertTwoWeight};
        pair<array<int, 3>, VertexHandle> t_EdgePoint = {t_EdgePointKey, t_CptIndex};
        t_EdgePoints.insert(t_EdgePoint);
    }
    /// loading triangle control points indices
    size_t t_NumTriangleControlPoints = (t_Deg-1)*(t_Deg-2)*t_NumTriangles/2;

    for (i = 0; i < t_NumTriangleControlPoints; ++i) {
        getline(t_HomFile, t_Line);
        auto t_SplitString = split_string_by_whitespace(t_Line);
        auto t_CptIndex = (VertexHandle)stoi(t_SplitString[0]);
        int t_TriangleID = stoi(t_SplitString[1]);
        int t_VertOneWeight = stoi(t_SplitString[2]);
        int t_VertTwoWeight = stoi(t_SplitString[3]);
        int t_VertThreeWeight = stoi(t_SplitString[4]);
        array<int, 4> t_FacePointKey = {t_TriangleID, t_VertOneWeight, t_VertTwoWeight, t_VertThreeWeight};
        pair<array<int, 4>, VertexHandle> t_FacePoint = {t_FacePointKey, t_CptIndex};
        t_FacePoints.insert(t_FacePoint);
    }
    /// loading triangle control points indices
    // For deg=3, there are no tetrahedral control points,
    // they are all on vertices, edges, or triangles.
    size_t t_NumTetrahedronControlPoints = (t_Deg-1)*(t_Deg-2)*(t_Deg-3)*t_NumTets/ 6;


    t_HomFile.close();
 
    return {t_HomMesh, t_EdgePoints, t_FacePoints};
}


/*
 *  Given the maps which describe a cubic tetrahedra generated from
 *  an *.hom file, construct the control points of a given tetrahedra.
 *
 *  Arrangement for use with deCastejau implementation:
 *  Lowest level:
 *              0
 *          1       2
 *      3       4       5
 *  6       7       8       9
 *  
 *  Next level:
 *          10
 *      11      12
 *  13      14      15
 *  
 *  Next level:
 *      16
 *  17      18
 *  
 *  Final level:
 *  19
 *
 *  Possible problem, should errors arise:
 *  The ordering the of the vertices might not be correct,
 *  i.e. when using the edge map you might actually wat {edgeID,  2, 1}
 *  instead of {edgeID, 1, 2}.
 */
vector<Vec3f> get_cpts_from_cubic_hom_struct(const TetrahedralMesh&                  a_Mesh, 
                                             const map<array<int, 3>, VertexHandle>& a_EdgeMap, 
                                             const map<array<int, 4>, VertexHandle>& a_FaceMap,
                                             const CellHandle                        a_Tet)
{
    vector<Vec3f> t_Cpts;

    vector<VertexHandle> t_TetVerts;
    for(auto t_TetVert_it = a_Mesh.cv_iter(a_Tet); t_TetVert_it.valid(); ++t_TetVert_it){
        t_TetVerts.push_back(*t_TetVert_it);
    }
    swap(t_TetVerts[1], t_TetVerts[2]);

    /*
     *  Reminder: a_Edgemap maps (EdgeHandle, from_vertex_idx, to_vertex_idx) to VertexHandle.
     */

    HalfEdgeHandle t_TempHE = a_Mesh.halfedge(t_TetVerts[0], t_TetVerts[1]);
    EdgeHandle t_EdgeHandle01 = a_Mesh.edge_handle(t_TempHE);
    t_TempHE = a_Mesh.halfedge(t_TetVerts[0], t_TetVerts[2]);
    EdgeHandle t_EdgeHandle02 = a_Mesh.edge_handle(t_TempHE);
    t_TempHE = a_Mesh.halfedge(t_TetVerts[0], t_TetVerts[3]);
    EdgeHandle t_EdgeHandle03 = a_Mesh.edge_handle(t_TempHE);
    t_TempHE = a_Mesh.halfedge(t_TetVerts[1], t_TetVerts[2]);
    EdgeHandle t_EdgeHandle12 = a_Mesh.edge_handle(t_TempHE);
    t_TempHE = a_Mesh.halfedge(t_TetVerts[1], t_TetVerts[3]);
    EdgeHandle t_EdgeHandle13 = a_Mesh.edge_handle(t_TempHE);
    t_TempHE = a_Mesh.halfedge(t_TetVerts[2], t_TetVerts[3]);
    EdgeHandle t_EdgeHandle23 = a_Mesh.edge_handle(t_TempHE);
/*
 *  Lowest level:
 *              0
 *          1       2
 *      3       4       5
 *  6       7       8       9
 */
    // 0
    t_Cpts.push_back(a_Mesh.vertex(t_TetVerts[0]));
 
    // 1
    if((a_Mesh.edge(t_EdgeHandle01)).from_vertex() == t_TetVerts[0]){
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle01, 2, 1})->second));
    }
    else{
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle01, 1, 2})->second));
    }
 
    // 2
    if((a_Mesh.edge(t_EdgeHandle02)).from_vertex() == t_TetVerts[0]){
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle02, 2, 1})->second));
    }
    else{
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle02, 1, 2})->second));
    }
 
    // 3
    if((a_Mesh.edge(t_EdgeHandle01)).from_vertex() == t_TetVerts[0]){
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle01, 1, 2})->second));
    }
    else{
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle01, 2, 1})->second));
    }
 
    // 4
    HalfFaceHandle t_TempHF = a_Mesh.halfface({t_TetVerts[0], t_TetVerts[1], t_TetVerts[2]});
    FaceHandle t_FaceHandle012 = a_Mesh.face_handle(t_TempHF);
    t_Cpts.push_back(a_Mesh.vertex(a_FaceMap.find({(int)t_FaceHandle012, 1, 1, 1})->second));
 
    // 5
    if((a_Mesh.edge(t_EdgeHandle02)).from_vertex() == t_TetVerts[0]){
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle02, 1, 2})->second));
    }
    else{
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle02, 2, 1})->second));
    }

    // 6
    t_Cpts.push_back(a_Mesh.vertex(t_TetVerts[1]));

    // 7 && 8
    if((a_Mesh.edge(t_EdgeHandle12)).from_vertex() == t_TetVerts[1]){
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle12, 2, 1})->second));
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle12, 1, 2})->second));
    }
    else{
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle12, 1, 2})->second));
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle12, 2, 1})->second));
    }
 
    // 9
    t_Cpts.push_back(a_Mesh.vertex(t_TetVerts[2]));
/*
 *  Next level:
 *          10
 *      11      12
 *  13      14      15
 */
    // 10
    if((a_Mesh.edge(t_EdgeHandle03)).from_vertex() == t_TetVerts[0]){
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle03, 2, 1})->second));
    }
    else{
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle03, 1, 2})->second));
    }
 
    // 11
    t_TempHF = a_Mesh.halfface({t_TetVerts[0], t_TetVerts[1], t_TetVerts[3]});
    FaceHandle t_FaceHandle013 = a_Mesh.face_handle(t_TempHF);
    t_Cpts.push_back(a_Mesh.vertex(a_FaceMap.find({(int)t_FaceHandle013, 1, 1, 1})->second));
 
    // 12
    t_TempHF = a_Mesh.halfface({t_TetVerts[0], t_TetVerts[2], t_TetVerts[3]});
    FaceHandle t_FaceHandle023 = a_Mesh.face_handle(t_TempHF);
    t_Cpts.push_back(a_Mesh.vertex(a_FaceMap.find({(int)t_FaceHandle023, 1, 1, 1})->second));

    // 13
    if((a_Mesh.edge(t_EdgeHandle13)).from_vertex() == t_TetVerts[1]){
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle13, 2, 1})->second));
    }
    else{
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle13, 1, 2})->second));
    }

    // 14
    t_TempHF = a_Mesh.halfface({t_TetVerts[1], t_TetVerts[2], t_TetVerts[3]});
    FaceHandle t_FaceHandle123 = a_Mesh.face_handle(t_TempHF);
    t_Cpts.push_back(a_Mesh.vertex(a_FaceMap.find({(int)t_FaceHandle123, 1, 1, 1})->second));
 
    // 15
    if((a_Mesh.edge(t_EdgeHandle23)).from_vertex() == t_TetVerts[2]){
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle23, 2, 1})->second));
    }
    else{
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle23, 1, 2})->second));
    }

/*  
 *  Next level:
 *      16
 *  17      18
 */
    // 16
    if((a_Mesh.edge(t_EdgeHandle03)).from_vertex() == t_TetVerts[0]){
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle03, 1, 2})->second));
    }
    else{
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle03, 2, 1})->second));
    }

    // 17
    if((a_Mesh.edge(t_EdgeHandle13)).from_vertex() == t_TetVerts[1]){
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle13, 1, 2})->second));
    }
    else{
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle13, 2, 1})->second));
    }

    // 18
    if((a_Mesh.edge(t_EdgeHandle23)).from_vertex() == t_TetVerts[2]){
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle23, 1, 2})->second));
    }
    else{
        t_Cpts.push_back(a_Mesh.vertex(a_EdgeMap.find({(int)t_EdgeHandle23, 2, 1})->second));
    }

/*
 *  Final level:
 *  19
 */
    // 19
    t_Cpts.push_back(a_Mesh.vertex(t_TetVerts[3]));
/*
    std::ofstream out_file;
    out_file.open( "Bracket/cpts.obj" );
    for(auto cpt : t_Cpts){
        out_file << "v " << cpt << endl;
    }
    out_file.close();
*/
    return t_Cpts;
}


vector<HalfFaceHandle> get_boundary_faces_in_image(const TetrahedralMesh& a_Mesh, const CellHandle a_Cell)
{
    vector<HalfFaceHandle> t_BoundaryFaces;
    // Find cell
    OpenVolumeMeshCell t_TetCell = a_Mesh.cell(a_Cell);
    // collect FaceHandles from HalffaceHandles
    vector<FaceHandle> t_FaceHandles;
    for(auto t_HalfFace : t_TetCell.halffaces()){
        t_FaceHandles.push_back(a_Mesh.face_handle(t_HalfFace));
    }
    // iterate through halffaces
    for(auto t_Face : t_FaceHandles){
        // check if on boundary, if so add corresponding halfface to vector
        if(a_Mesh.is_boundary(t_Face)){
            t_BoundaryFaces.push_back(a_Mesh.halfface_handle(t_Face, 0));
        }
    }
    return t_BoundaryFaces;
}

vector<VertexHandle> get_tet_domain_corners(CoST& a_CoST)
{
    //auto t_CoSTMesh = a_CoST.get_mesh();
    return Connectivity::get_tet_corners(a_CoST.get_mesh());
    /*vector<VertexHandle> t_CornerHandles = a_CoST.get_tet_corners();
    vector<Vec3f> t_Corners;
    for(auto t_VertHandle : t_CornerHandles){
        t_Corners.push_back(t_VertHandle);
    }
    return t_Corners;*/
}

/*
 *  This method finds the faces in a_CoST which when mapped forward correspond
 *  to boundary faces in the macrodomain.
 *
 *  General idea:  Map the domain tetrahedra forward and then search for
 *                 the faces which align with those that need to be stiffened.
 */
vector<vector<VertexHandle>> get_preimages_of_faces_to_stiffen(CoST& a_CoST, const vector<Vec3f> a_TetCpts, const vector<HalfFaceHandle> a_BoundaryFaces, TetrahedralMesh& a_Mesh)
{
    //  The CoST is a tetrahedron filled with a CoST,
    //  these vertices are the corners of that tetrahedron
    vector<Vec3f> t_EnclosingTet;
    t_EnclosingTet.push_back({ 5.5, 4.33012,  10.38716});
    t_EnclosingTet.push_back({ 11.99604,  0.57962, -0.86431});
    t_EnclosingTet.push_back({ 5.5,  11.8311, -0.86431});
    t_EnclosingTet.push_back({ -0.99604,  0.57962, -0.86431});

    /*
    t_EnclosingTet.push_back({ 20.5,  12.99041, 36.22926});
    t_EnclosingTet.push_back({ 41.88928,  0.64127, -0.81809});
    t_EnclosingTet.push_back({ 20.5, 37.68856,  -0.81809});
    t_EnclosingTet.push_back({ -0.88929,  0.64126, -0.81809});
    */

    //  Preimage of corners of the CoST itself:
    vector<VertexHandle> t_CoSTCorners = get_tet_domain_corners(a_CoST); // Domain, CoST
    //  Map these corners forward
    vector<Vec3f> t_MappedCorners; // Image
    for(auto t_Corner : t_CoSTCorners)
    {
        Vec3f t_Vert = (a_CoST.get_mesh()).vertex(t_Corner);
        // Find uvwt of the corner
        array<double, 4> t_BaryCoords = find_tetrahedron_bary_coords(t_Vert, t_EnclosingTet);
        // Map it forward
        const int t_Deg =  3;
        Vec3f t_MappedCorner = deCasteljau(t_Deg, t_BaryCoords, a_TetCpts);
        // Add it to the vector
        t_MappedCorners.push_back(t_MappedCorner);
    }
    // Now (t_CoSTCorners, t_MappedCorners) form a (domain, range) pair

    //  We now find the preimages of the faces that need to be stiffened
    vector<vector<VertexHandle>> t_FacesToStiffen;
    for(auto t_HalfFace : a_BoundaryFaces)
    {
        // Find vertices of face (recall this is in the image)
        vector<Vec3f> t_FaceVerts; // Image
        vector<VertexHandle> t_FaceVertHandles;
        for(auto t_Vert_it=a_Mesh.hfv_iter(t_HalfFace); t_Vert_it.valid(); ++t_Vert_it){
            t_FaceVerts.push_back(a_Mesh.vertex(*t_Vert_it));
            t_FaceVertHandles.push_back(*t_Vert_it);
        }
        // Find the normal to the face we wish to stiffen
        Vec3f t_Normal = cross(t_FaceVerts[1]-t_FaceVerts[0], t_FaceVerts[2]-t_FaceVerts[0]);
        t_Normal = t_Normal.normalize();
        // For each vertex of t_MappedCorners, compute its distance
        // from the triangular face.  The vertex which is furthest
        // from the face is the vertex which is not on the face.
        vector<double> t_DistFromTriangle;
        for(auto t_Corner : t_MappedCorners)
        {
            double t_Dist = abs(dot((t_Corner - t_FaceVerts[0]), t_Normal));
            t_DistFromTriangle.push_back(t_Dist);
        }
        // find furthest vertex
        auto t_MaxDist = max_element(t_DistFromTriangle.begin(), t_DistFromTriangle.end());
        auto t_MaxDistIndex = distance(t_DistFromTriangle.begin(), t_MaxDist);
        // order the face vertices for the CoST stiffening routine.
        if(t_MaxDistIndex != 3){
            swap(t_CoSTCorners[3], t_CoSTCorners[t_MaxDistIndex]);
        }
        // add it to the faces we need to stiffen
        t_FacesToStiffen.push_back(t_CoSTCorners);
    }
    return t_FacesToStiffen;
}

/*
 *  a_FacesToStiffen contains is a vector whose elements are vectors of VertexHandles
 *  ordered in the correct manner to stiffen the corresponding face.
 *  See CoST::stiffen_one_face for information on this ordering.
 */
void stiffen_preimage_of_boundary_faces(CoST& a_CoST, vector<vector<VertexHandle>> a_FacesToStiffen)
{
    auto mesh = a_CoST.get_mesh();
    for(vector<VertexHandle> t_Face : a_FacesToStiffen){
        Stiffening::boundary_stiffen_one_face(mesh, t_Face);
    }
    a_CoST.set_mesh(mesh);
    return;
}
     
/*
 *  Reads in a .msh file and writes an obj file with a CoST
 *  for each tetrahedron.
 */
void fill_hom_file_with_CoST(CoST a_CoST, const string a_HomFile)
{
    auto [t_Mesh, t_EdgeMap, t_FaceMap] = Mapping::read_hom_file(a_HomFile);
    string t_OutFileBase{"demo/tetComplex/Bracket/tet_"};

    write_obj("demo/tetComplex/tetrahedralization", t_Mesh);

    int t_TetCount = 0;
    for(auto t_Tet_it = t_Mesh.cells_begin(); t_Tet_it != t_Mesh.cells_end(); ++t_Tet_it)
    {
        // Create a locally scoped copy of the input CoST
        // so that we can modify it without changing the
        // original object.
        CoST t_CoST = a_CoST;
     
        // Get mapping control points
        auto t_TetCpts = get_cpts_from_cubic_hom_struct(t_Mesh, t_EdgeMap, t_FaceMap, *t_Tet_it);

        // Stiffen those faces which are on the boundary
        vector<HalfFaceHandle> t_BoundaryFaces = get_boundary_faces_in_image(t_Mesh, *t_Tet_it);
        if(t_BoundaryFaces.size() > 0)
        {
            vector<vector<VertexHandle>> t_FacesToStiffen = get_preimages_of_faces_to_stiffen(a_CoST, t_TetCpts, t_BoundaryFaces, t_Mesh);
            stiffen_preimage_of_boundary_faces(t_CoST, t_FacesToStiffen);
        }
     
        // Fill map the CoST forward and write to file
        auto t_CoSTMesh = t_CoST.get_mesh();
        TetMeshV3f t_CostTet = fill_cubic_tet_with_CoST(t_CoSTMesh, t_TetCpts);
        write_obj(t_OutFileBase+to_string(t_TetCount), t_CostTet);
        t_TetCount++;
        //break;
    }
}


} // end namespace Mapping

#endif //MAPPING_HH
