/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _COST_FILEIO_H_
#define _COST_FILEIO_H_

#include "CoST.h"

class CoST::FileIO
{
    vector< vector<double> > read_vertices( string );
    vector< vector<int> > read_faces( string );
  public:
    TetMeshV3f read_OFF( string ); 
};

vector< vector<double> >
CoST::FileIO::read_vertices( string filename )
{
  vector< vector<double> > vertices;
  std::ifstream file( filename );

  // ignore first line which is just "OFF"
  std::string dummy;
  std::getline( file, dummy );

  int numV;
  file >> numV;

  // to ignore the rest of the V F 0 line
  std::getline( file, dummy );

  for ( int i = 0; i < numV; i++ )
  {
    std::string v_str;
    std::getline( file, v_str );

    vector<double> vertex;
    stringstream ssin( v_str );
    int t = 0;
    while ( ssin.good() && t < 3 )
    {
        double v;
        ssin >> v;
        vertex.push_back( v );
        ++t;
    }
    vertices.push_back( vertex );
  }

  return vertices;
}

vector< vector<int> >
CoST::FileIO::read_faces( string filename )
{
  vector< vector<int> > faces;

  std::ifstream file( filename );

  //ignore first line which is just "OFF"
  std::string dummy;
  std::getline( file, dummy );

  int numV, numF;
  file >> numV >> numF;

  for ( int i = 0; i <= numV; i++ )
    std::getline( file, dummy );

  std::string line;

  while( std::getline( file, line ) )
  {     // '\n' is the default delimiter
    std::istringstream iss( line );

    std::string token;
    std::vector<int> face;
    int count  = 0;
    while( std::getline( iss, token, ' ' ) )
    {
      if ( token.compare( "" ) != 0 && count > 0 ) // ignores first number
        face.push_back( stoi( token ) );
      count++;
    }
    faces.push_back( face );
  }

  return faces;
}

TetMeshV3f
CoST::FileIO::read_OFF( string filename )
{
  TetMeshV3f mesh;
  mesh.enable_bottom_up_incidences( true );

  vector< vector<double> > vertices = read_vertices( filename );
  vector< vector<int> > faces = read_faces( filename );

  vector<int> used_index;
  vector<VertexHandle> used_vh;

  for ( size_t i = 0; i < faces.size(); i++ )
  {
    vector<VertexHandle> new_face;

    for ( size_t j = 0; j < faces[i].size(); j++ )
    {
      if ( find( used_index.begin(), used_index.end(), faces[i][j] ) == used_index.end() )
      // have not already added this vertex to the mesh
      {
        VertexHandle new_vh = mesh.add_vertex( Vec3f( vertices[ faces[i][j] ][0],
          vertices[ faces[i][j] ][1], vertices[ faces[i][j] ][2] ) );
        new_face.push_back( new_vh );
        used_index.push_back( faces[i][j] );
        used_vh.push_back( new_vh );
      }
      else
      {
        ptrdiff_t idx = find( used_index.begin(), used_index.end(), faces[i][j] ) - used_index.begin();
        new_face.push_back( used_vh[idx] );
      }
    }
    mesh.add_face( new_face );
  }

  return mesh;
}


#endif
