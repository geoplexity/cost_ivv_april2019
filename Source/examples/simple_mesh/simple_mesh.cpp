/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <chrono>

#include "CoST.h"
#include "CoST.hpp"
#include "refinement.hpp"
#include "stiffening.hpp"
#include "Mapping/mapping.hpp"
#include "irregularity.hpp"
#include "gluing.hpp"
#include "rigidity.hpp"

int main( int _argc, char** _argv )
{
    /*
     *  Handle input and default arguments
     */
    if(_argc != 1)
    {
      std::cerr << "This executable takes no arguments.\n";
      return 1;
    }

    Vec3f origin( 0, 0, 0 );
    bool isTet = true;
    bool isHex = !isTet;
    int sideLength = 5;
    int largerSideLength = 20;

    cout << "\n\n";


    /*
     *  Fill a regular tetrahedron with a CoST
     */
    cout << "Filling a tetrahedron with a CoST.\n";
    CoST tet( sideLength, origin, isTet );
    tet.write_obj( "demo/tetrahedralCoST" );
    const auto originalMesh = tet.get_mesh();
   
    CoST largerTet( largerSideLength, origin, isTet );
    tet.write_obj( "demo/tetrahedralCoSTLarger" );
    const auto largerOriginalMesh = largerTet.get_mesh();


    /*
     *  Refinement 1
     */
    cout << "Applying refinement 1.\n";
    TetMeshV3f mesh = tet.get_mesh();
    Refinement::refinement1(mesh);
    tet.set_mesh(mesh);
    tet.write_obj( "demo/Refinement/refinement1Once" );

    cout << "Applying refinement 1 a second time.\n";
    mesh = tet.get_mesh();
    Refinement::refinement1(mesh);
    tet.set_mesh(mesh);
    tet.write_obj( "demo/Refinement/refinement1Twice" );

    tet.set_mesh(originalMesh);

    /*
     *  Refinement 2
     */
    cout << "Applying refinement 2.\n";
    mesh = tet.get_mesh();
    Refinement::refinement2(mesh);
    tet.set_mesh(mesh);
    tet.write_obj( "demo/Refinement/refinement2" );
    tet.set_mesh(originalMesh);

    /*
     *  Irregularities/hole inclusion
     *  Nearly done, but currently seg faults for certain input.
     */
    /*
    cout << "Creating hole in in a larger mesh via Stone-Wales defects.\n";
    mesh = largerTet.get_mesh();
    Irregularity::hole_by_irreg(mesh, largerSideLength);
    largerTet.set_mesh(mesh);
    largerTet.write_obj( "demo/holeInTet" );
    largerTet.set_mesh(largerOriginalMesh);
    */

    /*
     *  Boundary Stiffening
     */
    cout << "Boundary stiffening a tet.\n";
    mesh = tet.get_mesh();
    Stiffening::boundary_stiffen(mesh);
    tet.set_mesh(mesh);
    tet.write_obj( "demo/boundaryStiffening" );
    tet.set_mesh(originalMesh);

    /*
     *  Fill bubble mesh with CoST
     */
    cout << "Filling a tetrahedral complex with CoSTs.\n";
    Mapping::fill_hom_file_with_CoST(tet, "test_files/degree_3_no_negative_detJ_0052.hom");

    /*
     *  Rigidity Matrix
     */
    cout << "Constructing and solving the rigidity matrix.\n";
    sideLength = 5;
    CoST rigidTet( sideLength, origin, isTet );
    auto t_RigidMesh = rigidTet.get_mesh();

    // Boundary stiffen
    Stiffening::boundary_stiffen(t_RigidMesh);
    
    // Map the tet so that it is generic
    auto [t_Mesh, t_EdgeMap, t_FaceMap] = Mapping::read_hom_file("test_files/degree_3_no_negative_detJ_0052.hom");
    for(auto t_Tet_it = t_Mesh.cells_begin(); t_Tet_it != t_Mesh.cells_end(); ++t_Tet_it)
    {
        // Get mapping control points
        auto t_TetCpts = Mapping::get_cpts_from_cubic_hom_struct(t_Mesh, t_EdgeMap, t_FaceMap, *t_Tet_it);

        // Fill map the CoST forward and write to file
        t_RigidMesh = Mapping::fill_cubic_tet_with_CoST(t_RigidMesh, t_TetCpts);
        break;
    }
    rigidTet.set_mesh(t_RigidMesh);
    rigidTet.write_obj( "demo/RigidityMatrices/rigidMesh" );

    // Run test function
    testRigidityMatrixSolver(t_RigidMesh);







    cout << "\n\n";

    return 0;
}
















