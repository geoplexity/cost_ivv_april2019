/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONNECTIVITY_HH
#define CONNECTIVITY_HH

// Eigen for determinant
#include "Eigen/Dense"

// Include vector classes
#include <OpenVolumeMesh/Geometry/VectorT.hh>

#include <OpenVolumeMesh/Mesh/PolyhedralMesh.hh>
// #include <OpenVolumeMesh/Mesh/TetrahedralMesh.hh>

// garbage collection
#include <OpenVolumeMesh/Attribs/StatusAttrib.hh>

// Make some typedefs to facilitate your life
typedef OpenVolumeMesh::Geometry::Vec3f         Vec3f;
typedef OpenVolumeMesh::Geometry::Vec4f         Vec4f;
typedef OpenVolumeMesh::GeometricPolyhedralMeshV3f   TetMeshV3f;
// typedef OpenVolumeMesh::GeometricTetrahedralMeshV3f  TetrahedralMesh;

using namespace std;
using namespace OpenVolumeMesh;

// const double epsilon = 0.1;
// auto equalish = []( double a, double b ) { return fabs( a - b ) < epsilon; };

namespace Connectivity {

/**
 * returns true if vh is only incident to one cell
 */
bool
is_boundary_vertex(TetMeshV3f mesh, VertexHandle vh)
{
  size_t cell_count = 0;
  VertexCellIter vc_it = mesh.vc_iter(vh);
  for ( ; vc_it.valid(); vc_it++ )
  {
    cell_count++;
  }
  return cell_count == 1;
}

/**
 * returns the cell handle that vh is incident to that is not ch
 */
CellHandle
opposite_cell(TetMeshV3f mesh, VertexHandle vh, CellHandle ch)
{
  VertexCellIter vc_it = mesh.vc_iter(vh);
  if (*vc_it == ch) vc_it++;
  if (vc_it.valid())
  {
    return *vc_it;
  }
  else
  {
    return CellHandle(-1);
  }
}

/**
 * Returns true if vh is one of the vertices on the 'corners' of the tetrahedral mesh.
 * In other words,
 * it returns true if vh is a boundary vertex in a cell that is corner-sharing
 * with three other cells that are each only corner-sharing with two cells,
 * including the original cell.
 */
bool
is_corner(TetMeshV3f mesh, VertexHandle vh)
{
  CellHandle ch = *(mesh.vc_iter(vh));
  if (ch == CellHandle(-1)) return false;

  size_t incident_cells = 0;
  CellVertexIter cv_it = mesh.cv_iter(ch);
  for ( ; cv_it.valid(); cv_it++ )
  {
    CellHandle opp = opposite_cell(mesh, *cv_it, ch);
    if (opp != CellHandle(-1))
    {
      size_t incident_cells_opp = 0;
      CellVertexIter cv_it_opp = mesh.cv_iter(opp);
      for ( ; cv_it_opp.valid(); cv_it_opp++ )
      {
        if (opposite_cell(mesh, *cv_it_opp, opp) != CellHandle(-1)) 
          incident_cells_opp++;
      }
      if (incident_cells_opp != 2) return false;
      incident_cells++;
    }
  }
  return incident_cells == 3;
}

/**
 * Returns the four ACTUAL VERTICES of the tetrahedral mesh that lie at the 'corners'
 */
std::vector<VertexHandle>
get_tet_corners(TetMeshV3f mesh)
{
  std::vector<VertexHandle> corners;
  for (VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); v_it++)
  {
    if (is_boundary_vertex(mesh, *v_it))
    {
      if (is_corner(mesh, *v_it))
      {
        corners.push_back(*v_it);
      }
    }
  }
  return corners;
}

// TODO duplicate in CoST.cpp
double
dist_btw_pts( Vec3f v1, Vec3f v2 )
{
  return sqrt( ( ( v1[0] - v2[0] ) * ( v1[0] - v2[0] ) )
    + ( ( v1[1] - v2[1] ) * ( v1[1] - v2[1] ) )
    + ( ( v1[2] - v2[2] ) * ( v1[2] - v2[2] ) ) );
}

/**
 * returns the vertex in cell ch that is the furthest away from vh in the x->y direction
 */
VertexHandle
furthest_in_cell_xy_dir(TetMeshV3f mesh, CellHandle ch, VertexHandle vh, 
    VertexHandle x_vh, VertexHandle y_vh)
{
  Vec3f x = mesh.vertex(x_vh);
  Vec3f y = mesh.vertex(y_vh);
  Vec3f v = mesh.vertex(vh);
  // this vector represents a point such that the vectors v->xy_dir and a->b are parallel
  Vec3f xy_dir(v[0]+y[0]-x[0], v[1]+y[1]-x[1], v[2]+y[2]-x[2]);

  CellVertexIter cv_it = mesh.cv_iter(ch);
  VertexHandle closest = *cv_it;
  cv_it++;
  for ( ; cv_it.valid(); cv_it++ )
  {
    if ( dist_btw_pts(mesh.vertex(*cv_it), xy_dir)
        < dist_btw_pts(mesh.vertex(closest), xy_dir) )
    {
      closest = *cv_it;
    }
  }
  return closest;
}

/**
 * returns true if vh lies on the same plane as vertices a, b, c
 */
bool
is_coplanar(Vec3f v, Vec3f a, Vec3f b, Vec3f c)
{
  std::vector<Vec3f> tet;
  tet.push_back(a); tet.push_back(b); tet.push_back(c); tet.push_back(v);
  Eigen::MatrixXd m(4, 4);
  for (size_t i = 0; i < 4; i++)
  {
    for (size_t j = 0; j < 4; j++)
    {
      if (j==3) m(i, j) = 1;
      else
      {
        m(i, j) = tet[i][j];
      }
    }
  }
  return fabs(m.determinant()) < 0.1;
}

/**
 * returns true if vh lies on the same plane as vertices a, b, c
 */
bool
is_coplanar(TetMeshV3f mesh, VertexHandle v_vh, 
    VertexHandle a_vh, VertexHandle b_vh, VertexHandle c_vh)
{
  Vec3f v = mesh.vertex(v_vh);
  Vec3f a = mesh.vertex(a_vh);
  Vec3f b = mesh.vertex(b_vh);
  Vec3f c = mesh.vertex(c_vh);

  return is_coplanar(v, a, b, c);
}

/**
 * returns the boundary vertex on face abc that will be at index 0 for stiffening
 */
VertexHandle
find_first_vertex(TetMeshV3f mesh, 
    VertexHandle a_vh, VertexHandle b_vh, 
    VertexHandle c_vh, VertexHandle d_vh)
{
  CellHandle ch = *(mesh.vc_iter(a_vh));
  VertexHandle closest_to_b = furthest_in_cell_xy_dir(mesh, ch, a_vh, a_vh, b_vh);
  CellHandle opp_b = opposite_cell(mesh, closest_to_b, ch);
  if (opp_b == CellHandle(-1)) throw std::runtime_error("first_vertex() couldn't find next cell");

  VertexHandle first_vh = VertexHandle(-1);
  CellVertexIter cv_it = mesh.cv_iter(opp_b);
  for ( ; cv_it.valid(); cv_it++ )
  {
    if (is_boundary_vertex(mesh, *cv_it) && is_coplanar(mesh, *cv_it, a_vh, b_vh, d_vh))
    {
      first_vh = *cv_it;
      break;
    }
  }
  if (first_vh == VertexHandle(-1)) throw std::runtime_error("first_vertex() couldn't find vertex in opp_b");
  return first_vh;
}

/**
 * returns a vector of the vertex handles in the cell except for the connecting
 * vertex
 */
std::vector<VertexHandle>
get_vhandles_except_connecting(TetMeshV3f mesh, CellHandle cell, VertexHandle origin)
{
  std::vector<VertexHandle> vert_handles;

  CellVertexIter cv_it = mesh.cv_iter( cell );
  for ( ; cv_it.valid(); cv_it++ )
  {
    if ( *cv_it != origin )
    {
      vert_handles.push_back( *cv_it );
    }
  }

  return vert_handles;
}

// returns true if xy is parallel to uv TODO move LA
bool
is_parallel(TetMeshV3f mesh, VertexHandle x_vh, VertexHandle y_vh, VertexHandle u_vh, VertexHandle v_vh)
{
  Vec3f y = mesh.vertex(y_vh);
  Vec3f x = mesh.vertex(x_vh);
  Vec3f u = mesh.vertex(u_vh);
  Vec3f v = mesh.vertex(v_vh);

  double mag_xy = dist_btw_pts(x, y);
  double mag_uv = dist_btw_pts(u, v);

  Vec3f slope_xy((y[0] - x[0])/mag_xy, (y[1] - x[1])/mag_xy, (y[2] - x[2])/mag_xy);
  Vec3f slope_uv((v[0] - u[0])/mag_uv, (v[1] - u[1])/mag_uv, (v[2] - u[2])/mag_uv);

  return equalish(fabs(slope_xy[0]), fabs(slope_uv[0])) 
    && equalish(fabs(slope_xy[1]), fabs(slope_uv[1]))
    && equalish(fabs(slope_xy[2]), fabs(slope_uv[2]));
}

/*
  returns the closest BOUNDARY vertex from vh in the direction of the vector a->b
*/
VertexHandle
boundary_step(TetMeshV3f mesh, VertexHandle vh, VertexHandle a_vh, VertexHandle b_vh)
{
  CellHandle ch = *(mesh.vc_iter(vh));
  VertexHandle closest_to_b = furthest_in_cell_xy_dir(mesh, ch, vh, a_vh, b_vh);
  ch = opposite_cell(mesh, closest_to_b, ch);
  if (ch == CellHandle(-1)) return VertexHandle(-1);
  closest_to_b = furthest_in_cell_xy_dir(mesh, ch, closest_to_b, a_vh, b_vh);
  ch = opposite_cell(mesh, closest_to_b, ch);
  if (ch == CellHandle(-1)) return VertexHandle(-1);

  VertexHandle right_vh = VertexHandle(-1);
  CellVertexIter cv_it = mesh.cv_iter(ch);
  for ( ; cv_it.valid(); cv_it++ )
  {
    if (is_boundary_vertex(mesh, *cv_it) && is_parallel(mesh, vh, *cv_it, a_vh, b_vh))
    {
      right_vh = *cv_it;
      break;
    }
  }
  return right_vh;
}

// TODO move somewhere else
CellHandle
add_cell_polymesh(TetMeshV3f& mesh, std::vector<VertexHandle> verts)
{
    HalfFaceHandle hf0, hf1, hf2, hf3;

    std::vector<VertexHandle> vs;

    vs.push_back(verts[0]);
    vs.push_back(verts[1]);
    vs.push_back(verts[2]);
    hf0 = mesh.halfface(vs);
    if(!hf0.is_valid()) {
        FaceHandle fh = mesh.add_face(vs);
        hf0 = mesh.halfface_handle(fh, 0);
    }
    vs.clear();

    vs.push_back(verts[0]);
    vs.push_back(verts[2]);
    vs.push_back(verts[3]);
    hf1 = mesh.halfface(vs);
    if(!hf1.is_valid()) {
        FaceHandle fh = mesh.add_face(vs);
        hf1 = mesh.halfface_handle(fh, 0);
    }
    vs.clear();

    vs.push_back(verts[0]);
    vs.push_back(verts[3]);
    vs.push_back(verts[1]);
    hf2 = mesh.halfface(vs);
    if(!hf2.is_valid()) {
        FaceHandle fh = mesh.add_face(vs);
        hf2 = mesh.halfface_handle(fh, 0);
    }
    vs.clear();

    vs.push_back(verts[1]);
    vs.push_back(verts[3]);
    vs.push_back(verts[2]);
    hf3 = mesh.halfface(vs);
    if(!hf3.is_valid()) {
        FaceHandle fh = mesh.add_face(vs);
        hf3 = mesh.halfface_handle(fh, 0);
    }
    vs.clear();

    assert(hf0.is_valid());
    assert(hf1.is_valid());
    assert(hf2.is_valid());
    assert(hf3.is_valid());

    std::vector<HalfFaceHandle> hfs;
    hfs.push_back(hf0);
    hfs.push_back(hf1);
    hfs.push_back(hf2);
    hfs.push_back(hf3);

    CellHandle new_ch = mesh.add_cell(hfs, false);
    return new_ch;
}

Vec3f
midpt_btw_vert_handles( TetMeshV3f mesh, VertexHandle v1, VertexHandle v2 )
{
  double x, y, z;
  x = ( mesh.vertex( v1 )[0] + mesh.vertex( v2 )[0] ) / 2;
  y = ( mesh.vertex( v1 )[1] + mesh.vertex( v2 )[1] ) / 2;
  z = ( mesh.vertex( v1 )[2] + mesh.vertex( v2 )[2] ) / 2;

  return Vec3f( x, y, z );
}

/*
  returns the vertex incident to both cells TODO add to connectivity
*/
VertexHandle
find_connecting_vertex(TetMeshV3f mesh, CellHandle ref, CellHandle other )
{
  VertexHandle origin;

  bool found = false;
  CellVertexIter cv_it = mesh.cv_iter( ref );
  for ( ; cv_it.valid() && !found; cv_it++ )
  {
    VertexCellIter vc_it = mesh.vc_iter( *cv_it );
    for ( ; vc_it.valid() && !found; vc_it++ )
    {
      if ( ( *vc_it ) == other )
      {
        origin = ( *cv_it );
        found = true;
      }
    }
  }

  if ( !found ) throw std::runtime_error( "find_connecting_vertex() invalid input" );
  return origin;
}

// returns true if xy is non-negative scalar multiple to uv
bool
vec_same_dir(Vec3f x, Vec3f y, Vec3f u, Vec3f v)
{
  double mag_xy = dist_btw_pts(x, y);
  double mag_uv = dist_btw_pts(u, v);

  Vec3f slope_xy((y[0] - x[0])/mag_xy, (y[1] - x[1])/mag_xy, (y[2] - x[2])/mag_xy);
  Vec3f slope_uv((v[0] - u[0])/mag_uv, (v[1] - u[1])/mag_uv, (v[2] - u[2])/mag_uv);

  return equalish(slope_xy[0], slope_uv[0]) 
    && equalish(slope_xy[1], slope_uv[1])
    && equalish(slope_xy[2], slope_uv[2]);
}

VertexHandle
step_xy_dir_same_layer(TetMeshV3f mesh, VertexHandle curr, Vec3f x, Vec3f y)
{
  // find incident vertex 'next' s.t. curr->next is parallel to x->y
  VertexHandle next = VertexHandle(-1);
  VertexOHalfEdgeIter voh_it = mesh.voh_iter(curr);
  for(; voh_it.valid(); ++voh_it)
  {
    if (vec_same_dir(x, y,
      mesh.vertex(curr), mesh.vertex(mesh.halfedge(*voh_it).to_vertex())))
    {
      next = mesh.halfedge(*voh_it).to_vertex();
      break;
    }
  }

  // check that curr, x, y, and next are coplanar
  if (next != VertexHandle(-1) 
    && !is_coplanar(mesh.vertex(curr), mesh.vertex(next), x, y))
  {
    next = VertexHandle(-1);
  }

  return next;
}

/*
  Returns vector of vertices sorted in order necessary for boundary stiffening code,
  oriented counterclockwise from the outside, 012 face of the mesoregion
*/
vector<VertexHandle>
order_vertices_oriented(TetMeshV3f mesh, vector<VertexHandle> corners)
{  
  vector<VertexHandle> ordered_verts;
  VertexHandle curr = find_first_vertex(mesh, corners[0], corners[1], corners[2], corners[3]);
  VertexHandle next = curr;
  size_t k = 1; // counts the size of the outer layer
  // k starts at 1 because the outer layer is missing corners
  while ( next != VertexHandle(-1) )
  {
    curr = next;
    ordered_verts.push_back(curr);
    // going right
    next = boundary_step(mesh, curr, corners[0], corners[1]); 
    k++;
  }
  // one step diagonally up and to the right to skip over corner
  next = curr = boundary_step(mesh, curr, corners[0], corners[2]);
  while ( next != VertexHandle(-1) )
  {
    curr = next;
    ordered_verts.push_back(curr);
    // going diagonally UP and to the LEFT
    next = boundary_step(mesh, curr, corners[1], corners[2]);
  }
  // one step left to skip over corner
  next = curr = boundary_step(mesh, curr, corners[1], corners[0]);
  while ( next != VertexHandle(-1) )
  {
    curr = next;
    ordered_verts.push_back(curr);
    // going diagonally DOWN and LEFT
    next = boundary_step(mesh, curr, corners[2], corners[0]);
  }

  if ( k == 3)
  {
    curr = boundary_step(mesh, curr, corners[0], corners[1]);
    ordered_verts.push_back(curr);
    k = -1;
  }
  else if ( k > 3 )
  {
    curr = boundary_step(mesh, boundary_step(mesh, curr, corners[0], corners[1]), corners[0], corners[1]);
    k -= 3;
  }
  
  // now ordering the inner rings
  while ( k > 0 )
  {
    ordered_verts.push_back(curr);
    for (size_t i = 1; i < k; i++)
    {
      curr = boundary_step(mesh, curr, corners[0], corners[1]);
      ordered_verts.push_back(curr);
    }
    curr = boundary_step(mesh, curr, corners[1], corners[2]);
    ordered_verts.push_back(curr);
    for (size_t i = 1; i < k; i++)
    {
      curr = boundary_step(mesh, curr, corners[1], corners[2]);
      ordered_verts.push_back(curr);
    }
    curr = boundary_step(mesh, curr, corners[2], corners[0]);
    ordered_verts.push_back(curr);
    for (size_t i = 1; i < k; i++)
    {
      curr = boundary_step(mesh, curr, corners[2], corners[0]);
      ordered_verts.push_back(curr);
    }

    if (k<3)
    {
      k = -1; // why is this not changing the value of k??
      break;
    }
    else if (k==3)
    {
      // 1 step right and 1 step up-right
      curr = boundary_step(mesh, curr, corners[0], corners[1]);
      curr = boundary_step(mesh, curr, corners[0], corners[2]);
      ordered_verts.push_back(curr);
      k = -1;
      break;
    }
    else // k > 3
    {
      // 2 steps to the right and one up-right
      curr = boundary_step(mesh, boundary_step(mesh, curr, corners[0], corners[1]), corners[0], corners[1]);
      curr = boundary_step(mesh, curr, corners[0], corners[2]);
      k-=3;
    }
  }
  return ordered_verts;
}

/**
 *  returns [normal vector, point on plane, 
 *  normal vector of next face, point on plane of next face, ...]
 */
vector<Eigen::Vector3d>
get_planes_of_tet(TetMeshV3f mesh)
{
  vector<VertexHandle> corners = get_tet_corners(mesh);
  vector< vector<Eigen::Vector3d> > points_in_planes;
  for (size_t i = 0; i < 4; i++)
  {
    VertexHandle curr = find_first_vertex(mesh, corners[i], corners[(i+1)%4], corners[(i+2)%4], corners[(i+3)%4]);
    vector<Eigen::Vector3d> verts;
    verts.push_back(Eigen::Vector3d(mesh.vertex(curr)[0], mesh.vertex(curr)[1], mesh.vertex(curr)[2]));
    VertexHandle next = curr;
    while ( next != VertexHandle(-1) ) // find boundary vertex on same face further away from first vertex
    {
      curr = next;
      next = boundary_step(mesh, curr, corners[i], corners[(i+1)%4]); 
    }
    verts.push_back(Eigen::Vector3d(mesh.vertex(curr)[0], mesh.vertex(curr)[1], mesh.vertex(curr)[2]));
    // one step in the corners[i]=>corners[i+2] direction to skip over corner
    next = curr = boundary_step(mesh, curr, corners[i], corners[(i+2)%4]);
    while ( next != VertexHandle(-1) ) // find boundary vertex on same face far away from other two in verts
    {
      curr = next;
      next = boundary_step(mesh, curr, corners[(i+1)%4], corners[(i+2)%4]);
    }
    verts.push_back(Eigen::Vector3d(mesh.vertex(curr)[0], mesh.vertex(curr)[1], mesh.vertex(curr)[2]));
    points_in_planes.push_back(verts);
  }
  vector<Eigen::Vector3d> planes;
  for (size_t i = 0; i < 4; i++)
  {
    Eigen::Vector3d ab = points_in_planes[i][1] - points_in_planes[i][0];
    Eigen::Vector3d ac = points_in_planes[i][2] - points_in_planes[i][0];
    Eigen::Vector3d abXac = ab.cross(ac);
    abXac.normalize();
    planes.push_back(abXac);
    planes.push_back(points_in_planes[i][0]);
  }
  return planes;
}

/*
  finds the "real" corners of a tet mesoregion: the 4 points that 
  form a tetrahedron that the entire tet mesoregion fits snugly inside
*/
vector<Vec3f>
get_corners_tet_mesoregion(TetMeshV3f mesh)
{
  vector<Eigen::Vector3d> planes = get_planes_of_tet(mesh);
  vector<Vec3f> corners;
  for (size_t i = 0; i < 8; i+=2)
  {
    // http://mathworld.wolfram.com/Plane-PlaneIntersection.html
    Eigen::Vector3d n1 = planes[i];
    Eigen::Vector3d x1 = planes[i+1];
    Eigen::Vector3d n2 = planes[(i+2)%8];
    Eigen::Vector3d x2 = planes[(i+3)%8];
    Eigen::Vector3d n3 = planes[(i+4)%8];
    Eigen::Vector3d x3 = planes[(i+5)%8];

    Eigen::Vector3d p = (x1.dot(n1)*n2.cross(n3))
      + (x2.dot(n2)*n3.cross(n1)) + (x3.dot(n3)*n1.cross(n2));
    Eigen::Matrix3d mat;
    mat.col(0) = n1;
    mat.col(1) = n2;
    mat.col(2) = n3;
    p = p / mat.determinant();
    corners.push_back(Vec3f(p[0], p[1], p[2]));
  }
  /*std::cout << "***CORNERS:" << std::endl;
  for (size_t i =0; i <corners.size(); i++)
  {
    std::cout << corners[i] << "\n" << std::endl;
  }*/
  return corners;
}

} // namespace Connectivity

#endif // CONNECTIVITY_HH
