/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IRREGULARITY_HH
#define IRREGULARITY_HH

#include "connectivity.hpp"

// TODO threw segfault during irreg hole
namespace Irregularity {

void align_incident_tets(TetMeshV3f& mesh, const vector<VertexHandle>& ref_handles, vector<VertexHandle>& other_handles,
  VertexHandle origin_handle );
bool is_valid_pair_for_irreg(TetMeshV3f mesh, CellHandle ref, CellHandle other );
VertexHandle step_around_hole(TetMeshV3f& mesh, VertexHandle curr, VertexHandle prev, Vec3f hole_center);
VertexHandle irreg(TetMeshV3f& mesh, VertexHandle connecting_vertex);
VertexHandle avg_shift(TetMeshV3f& mesh, VertexHandle connectingv);
std::vector<VertexHandle> find_vertices_to_flip(TetMeshV3f& mesh, Vec3f hole_center, double side_length);
void hole_by_irreg(TetMeshV3f& mesh, double side_length, Vec3f hole_center);

/*
 *  Given two tets t1 and t2 which are adjacent on a vertex,
 *  order the vertices of t2 such that if you flip the vertex
 *  at index i of t2 about the origin, it is closest to vertex
 *  i of t1
 *
 *  First translate to origin based on vertex in common,
 *  then order, then translate back
 */
void
align_incident_tets(TetMeshV3f& mesh, 
  const vector<VertexHandle>& ref_handles,
  vector<VertexHandle>& other_handles,
  VertexHandle origin_handle )
{
    // extract vertices from vertex handles
    vector<Vec3f> ref_geom;
    vector<Vec3f> other_geom;
    for ( size_t i = 0; i < 3; i++ )
    {
      ref_geom.push_back( mesh.vertex( ref_handles[i] ) );
      other_geom.push_back( mesh.vertex( other_handles[i] ) );
    }

    vector<Vec3f> ref_geom_copy;
    // translate to origin
    Vec3f origin = mesh.vertex( origin_handle ); // cost is a TetMeshV3f from OVM
    for( int i=0; i<3; ++i )
    {
        ref_geom_copy.push_back( ref_geom[i] - origin );
        other_geom[i] -= origin;
    }

    typedef array<int, 3> permutation;
    vector<permutation> permute;

    permute.push_back( {{0,1,2}} );
    permute.push_back( {{0,2,1}} );
    permute.push_back( {{1,0,2}} );
    permute.push_back( {{1,2,0}} );
    permute.push_back( {{2,0,1}} );
    permute.push_back( {{2,1,0}} );

    vector<double> error;
    vector<Vec3f> temp_pos;
    temp_pos.resize( 3 );

    // for each permutation
    for( int i=0;  i<6; ++i )
    {
        // construct the geometry of the permutation
        for( int j=0; j<3; ++j ){
            temp_pos[j] = -1.0*other_geom[permute[i][j]];
        }
        double err=0.0;
        // for each vertex of the permuted tetrahedron
        for( int j=0; j<3; ++j ){
            // find how far it is from its "twin"
            Vec3f diff = temp_pos[j]-ref_geom_copy[j];
            double dist=0.0;
            for( auto pos : diff ){ // for x, y, z positions
                dist += pos*pos; // squared distance
            }
            err += dist;
        }
        error.push_back( err );
    }

    auto min = min_element( error.begin(), error.end() );
    int min_pos = distance( error.begin(), min );

    auto temp_other_geom = other_geom;
    auto temp_other_handles = other_handles;
    for( int j=0; j<3; ++j ){
        other_geom[j] = temp_other_geom[permute[min_pos][j]];
        other_handles[j] = temp_other_handles[permute[min_pos][j]];
    }

    // translate back
    for( int i=0; i<3; ++i )
    {
        other_geom[i] += origin;
    }
}

bool
is_valid_pair_for_irreg(TetMeshV3f mesh, CellHandle ref, CellHandle other )
{
  bool valid = true;
  if (ref == CellHandle(-1) || other == CellHandle(-1) || ref == other)
  {
    valid = false;
    return valid;
  }
  VertexHandle origin = Connectivity::find_connecting_vertex(mesh, ref, other );
  if (origin == VertexHandle(-1))
  {
    valid = false;
    return valid;
  }
  std::vector<VertexHandle> ref_handles, other_handles;
  ref_handles = Connectivity::get_vhandles_except_connecting(mesh, ref, origin );
  other_handles = Connectivity::get_vhandles_except_connecting(mesh, other, origin );
  
  /*
    check to make sure no two vertices of ref and other (with the exception of 
    the connecting vertex) are connected by an edge
  */
  for (size_t i = 0; i < 3; i++)
  {
    for (size_t j = 0; j < 3; j++)
    {
      if (mesh.halfedge(ref_handles[i], other_handles[j]) != HalfEdgeHandle(-1))
      {
        valid = false;
        return valid;
      }
    }
  }

  return valid;
}

/*
  returns incident vertex to curr that is not prev and is also on border of 
  hole defined by hole_center
*/
VertexHandle
step_around_hole(TetMeshV3f& mesh, VertexHandle curr, VertexHandle prev, Vec3f hole_center)
{
  VertexHandle next = VertexHandle(-1);
  VertexOHalfEdgeIter voh_it = mesh.voh_iter(curr);
  for(; voh_it.valid(); ++voh_it)
  {
    if (mesh.halfedge(*voh_it).to_vertex() != prev
      && equalish(Connectivity::dist_btw_pts(mesh.vertex(mesh.halfedge(*voh_it).to_vertex()), hole_center),
                  Connectivity::dist_btw_pts(mesh.vertex(curr), hole_center)))
    {
      next = mesh.halfedge(*voh_it).to_vertex();
      break;
    }
  }

  // check that curr, prev, hole_center, and next are coplanar
  if (next != VertexHandle(-1) && prev != VertexHandle(-1)
    && !Connectivity::is_coplanar(mesh.vertex(curr), mesh.vertex(prev),
                    hole_center, mesh.vertex(next)))
  {
    next = VertexHandle(-1);
  }

  return next;
}

VertexHandle
irreg(TetMeshV3f& mesh, VertexHandle connecting_vertex)
{
  VertexCellIter vc_it = mesh.vc_iter(connecting_vertex);
  CellHandle ref = *vc_it;
  CellHandle other = CellHandle(-1);
  vc_it++;
  if (vc_it.valid()) other = *vc_it;
  if ( ! is_valid_pair_for_irreg( mesh, ref, other ) )
  {
    // std::cout << "invalid pair!" << std::endl;
    return VertexHandle(-1);
  }
  else
  {
    std::vector<VertexHandle> ref_handles, other_handles;
    ref_handles = Connectivity::get_vhandles_except_connecting(mesh, ref, connecting_vertex );
    other_handles = Connectivity::get_vhandles_except_connecting(mesh, other, connecting_vertex );

    align_incident_tets( mesh, ref_handles, other_handles, connecting_vertex );

    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( connecting_vertex, ref_handles[0] ) ) );
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( connecting_vertex, ref_handles[1] ) ) );
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( connecting_vertex, ref_handles[2] ) ) );
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( ref_handles[0], ref_handles[1] ) ) );
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( ref_handles[0], ref_handles[2] ) ) );
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( ref_handles[1], ref_handles[2] ) ) );

    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( connecting_vertex, other_handles[0] ) ) );
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( connecting_vertex, other_handles[1] ) ) );
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( connecting_vertex, other_handles[2] ) ) );
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( other_handles[0], other_handles[1] ) ) );
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( other_handles[0], other_handles[2] ) ) );
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge( other_handles[1], other_handles[2] ) ) );

    std::vector<VertexHandle> new_cell;
    new_cell.push_back( connecting_vertex );
    new_cell.push_back( other_handles[0] );
    new_cell.push_back( ref_handles[1] );
    new_cell.push_back( ref_handles[2] );
    ref = Connectivity::add_cell_polymesh( mesh, new_cell );

    new_cell.clear();
    new_cell.push_back( connecting_vertex );
    new_cell.push_back( ref_handles[0] );
    new_cell.push_back( other_handles[1] );
    new_cell.push_back( other_handles[2] );
    other = Connectivity::add_cell_polymesh( mesh, new_cell );

    // TODO delete this
    ref_handles = Connectivity::get_vhandles_except_connecting(mesh, ref, connecting_vertex );
    other_handles = Connectivity::get_vhandles_except_connecting(mesh, other, connecting_vertex );

    vector<VertexHandle> z0ref, z0other;
    VertexHandle balance_later = connecting_vertex;
    for (size_t i = 0; i < 3; i++)
    {
      if (equalish(mesh.vertex(ref_handles[i])[2], mesh.vertex(connecting_vertex)[2]))
      {
        z0ref.push_back(ref_handles[i]);
      }
      else
      {
        if (mesh.vertex(ref_handles[i])[2] > mesh.vertex(balance_later)[2])
        {
          balance_later = ref_handles[i];
        }
      }
      if (equalish(mesh.vertex(other_handles[i])[2], mesh.vertex(connecting_vertex)[2]))
      {
        z0other.push_back(other_handles[i]);
      }
      else
      {
        if (mesh.vertex(other_handles[i])[2] > mesh.vertex(balance_later)[2])
        {
          balance_later = other_handles[i];
        }
      }
    }
    if (z0ref.size() != 2 || z0other.size() != 2)
    {
      return VertexHandle(-1);
    }

    Vec3f offsetref(mesh.vertex(z0ref[1])[0]-mesh.vertex(z0ref[0])[0], 
      mesh.vertex(z0ref[1])[1]-mesh.vertex(z0ref[0])[1], 
      mesh.vertex(z0ref[1])[2]-mesh.vertex(z0ref[0])[2]);
    Vec3f offsetother(mesh.vertex(z0other[1])[0]-mesh.vertex(z0other[0])[0], 
      mesh.vertex(z0other[1])[1]-mesh.vertex(z0other[0])[1], 
      mesh.vertex(z0other[1])[2]-mesh.vertex(z0other[0])[2]);
    mesh.set_vertex(z0ref[0], Vec3f(
      (0.4*offsetref[0])+mesh.vertex(z0ref[0])[0], 
      (0.4*offsetref[1])+mesh.vertex(z0ref[0])[1], 
      (0.4*offsetref[2])+mesh.vertex(z0ref[0])[2]));
    mesh.set_vertex(z0ref[1], Vec3f(
      (-0.4*offsetref[0])+mesh.vertex(z0ref[1])[0], 
      (-0.4*offsetref[1])+mesh.vertex(z0ref[1])[1], 
      (-0.4*offsetref[2])+mesh.vertex(z0ref[1])[2]));
    mesh.set_vertex(z0other[0], Vec3f(
      (0.4*offsetother[0])+mesh.vertex(z0other[0])[0], 
      (0.4*offsetother[1])+mesh.vertex(z0other[0])[1], 
      (0.4*offsetother[2])+mesh.vertex(z0other[0])[2]));
    mesh.set_vertex(z0other[1], Vec3f(
      (-0.4*offsetother[0])+mesh.vertex(z0other[1])[0], 
      (-0.4*offsetother[1])+mesh.vertex(z0other[1])[1], 
      (-0.4*offsetother[2])+mesh.vertex(z0other[1])[2]));
    
    mesh.collect_garbage();

    return balance_later;
    // return mesh;
  }
}

VertexHandle
avg_shift(TetMeshV3f& mesh, VertexHandle connectingv)
{
  if (connectingv == VertexHandle(-1)) return connectingv;
  VertexCellIter vc_it = mesh.vc_iter(connectingv);
  CellHandle ref = *vc_it;
  CellHandle other = CellHandle(-1);
  vc_it++;
  if (vc_it.valid()) other = *vc_it;
  if (ref == CellHandle(-1) || other == CellHandle(-1)) return VertexHandle(-1);
  vector<VertexHandle> ref_vs = Connectivity::get_vhandles_except_connecting(mesh, ref, connectingv);
  vector<VertexHandle> other_vs = Connectivity::get_vhandles_except_connecting(mesh, other, connectingv);
  if (ref_vs.size() != 3 || other_vs.size() != 3) throw std::runtime_error("avg_shift()");

  double avgx = 0, avgy = 0, avgz = 0;
  for (size_t i = 0; i < 3; i++)
  {
    avgx += mesh.vertex(ref_vs[i])[0] + mesh.vertex(other_vs[i])[0];
    avgy += mesh.vertex(ref_vs[i])[1] + mesh.vertex(other_vs[i])[1];
    avgz += mesh.vertex(ref_vs[i])[2] + mesh.vertex(other_vs[i])[2];
  }
  Vec3f avg(avgx/6, avgy/6, avgz/6);
  mesh.set_vertex(connectingv, avg);

  return connectingv;
}

// void
// balance_entire_CoST()
// {
//   Stiffen stiffen;
//   CellIter c_it = mesh.cells_begin();
//   for (; c_it != mesh.cells_end(); c_it++)
//   {
//     CellVertexIter cv_it = mesh.cv_iter(*c_it);
//     for (; cv_it.valid(); ++cv_it)
//     {
//       CellHandle opp = stiffen.opposite_cell(this, *cv_it, *c_it);
//       if (opp != CellHandle(-1)) avg_shift(*cv_it);
//     }
//   }
// }

std::vector<VertexHandle> 
find_vertices_to_flip(TetMeshV3f& mesh, Vec3f hole_center, double side_length)
{
  vector<VertexHandle> flips;
  vector<VertexHandle> boundary_flips;
  vector<Vec3f> hole_centers;
  hole_centers.push_back(hole_center);
  vector<VertexHandle> balance_later;

  for (size_t i = 1; i <= side_length; i++)
  {
    Vec3f temp = hole_centers[hole_centers.size()-1];
    temp[0] -= 1;
    temp[1] += 0.5774;
    temp[2] += 1.73205;
    hole_centers.push_back(temp);
  }

  vector< vector<Vec3f> > flip_dirs;
  size_t num_layers = 0;

  for (size_t layer = 0; layer < side_length; layer++)
  {
    VertexIter v_it = mesh.vertices_begin();
    VertexHandle min = *v_it;
    v_it++;
    for (; v_it != mesh.vertices_end(); v_it++) // find vertex closest to hole_center
    {
      if (Connectivity::dist_btw_pts(mesh.vertex(*v_it), hole_centers[layer]) 
        < Connectivity::dist_btw_pts(mesh.vertex(min), hole_centers[layer]))
      {
        min = *v_it;
      }
    }

    vector<VertexHandle> inner_ring;
    inner_ring.push_back(min);
    for (size_t i = 0; i < 5; i++) // collect vertices around hole_center
    {
      VertexHandle prev = VertexHandle(-1);
      if (i>0) prev = inner_ring[i-1];
      VertexHandle next = step_around_hole(mesh, inner_ring[i], prev, hole_centers[layer]);
      if (next == VertexHandle(-1)) break;
      else inner_ring.push_back(next);
    }

    if (inner_ring.size() == 6) // 6 vertices are around hole_center. regular case
    {
      for (size_t i = 0; i < 6; i++)
      {
        VertexHandle temp = Connectivity::step_xy_dir_same_layer(mesh, inner_ring[i], 
                                  mesh.vertex(inner_ring[(i+5)%6]), 
                                  mesh.vertex(inner_ring[i]));
        if (temp != VertexHandle(-1)) flips.push_back(temp);
      }
      vector<Vec3f> inner_vecs;
      for (size_t t = 0; t < 6; t++) inner_vecs.push_back(mesh.vertex(inner_ring[t]));
      flip_dirs.push_back(inner_vecs);
      num_layers++;
    }
    else // on the boundary
    {
      bool go = true;
      while (go)
      {
        VertexHandle next = step_around_hole(mesh, inner_ring[0], inner_ring[1], hole_centers[layer]);
        if (next == VertexHandle(-1) || next == inner_ring.back()) go=false;
        else
        {
          inner_ring.insert(inner_ring.begin(), next);
        }
      }
      for (size_t i = 0; i < inner_ring.size(); i++)
      {
        VertexOHalfEdgeIter voh_it = mesh.voh_iter(inner_ring[i]);
        for(; voh_it.valid(); ++voh_it)
        {
          VertexHandle vh = mesh.halfedge(*voh_it).to_vertex();
          if ( equalish(mesh.vertex(vh)[2], mesh.vertex(inner_ring[i])[2]) // TODO fix layer check
            && std::find(boundary_flips.begin(), boundary_flips.end(), vh) == boundary_flips.end()
            && std::find(inner_ring.begin(), inner_ring.end(), vh) == inner_ring.end())
          {
            boundary_flips.push_back(vh);
          }
        }
      }
      size_t boundary_flips_size = boundary_flips.size();
      for (size_t i =0; i < boundary_flips_size; i++)
      {
        VertexHandle temp = Connectivity::step_xy_dir_same_layer(mesh, boundary_flips[i], 
                                  mesh.vertex(inner_ring[i]), 
                                  mesh.vertex(boundary_flips[i]));
        if (temp != VertexHandle(-1)) {
          boundary_flips.push_back(temp);
        }
      }
      layer+=side_length; // exit enclosing for loop
    }
  }
  size_t hole_depth = 2;
  for (size_t d = 0; d < hole_depth; d++)
  {
    for (size_t l = 0; l < num_layers; l++)
    {
      for (size_t v = 0; v < 6; v++)
      {
        VertexHandle temp = Connectivity::step_xy_dir_same_layer(mesh, flips[v +(d*6*num_layers) +(6*l)], 
                                flip_dirs[l][(v+5)%6], 
                                flip_dirs[l][v]);

        if (temp != VertexHandle(-1)) flips.push_back(temp);
      }
    }
  }

  flips.insert(flips.end(), boundary_flips.begin(), boundary_flips.end());

  return flips;
}

void
hole_by_irreg(TetMeshV3f& mesh, double side_length, Vec3f hole_center)
{
  vector<VertexHandle> flips3 = find_vertices_to_flip(mesh, hole_center, side_length);

  vector<VertexHandle> balance_later;
  for (size_t i = 0; i < flips3.size(); i++)
  {
    VertexHandle b = irreg(mesh, flips3[i]);
    if (b != VertexHandle(-1)) balance_later.push_back(b);
  }

  // TODO balancing - gradual shift
  for (size_t b = 0; b < balance_later.size(); b++)
  {
    avg_shift(mesh, balance_later[b]);
  }
//   balance_entire_CoST(mesh);
}

void
hole_by_irreg(TetMeshV3f& mesh, double side_length)
{
  std::vector<VertexHandle> corners = Connectivity::get_tet_corners(mesh);
  for (size_t i=0; i<4; i++)
  {
    if (mesh.vertex(corners[i])[2] > sqrt(3))
    {
      VertexHandle erased = corners[i];
      corners.erase(corners.begin() + i);
      corners.push_back(erased);
      break;
    }
  }
  std::vector<VertexHandle> ordered = Connectivity::order_vertices_oriented(mesh, corners);
  size_t index = 9 * ordered.size() / 10;
  std::vector<VertexHandle> find_hole;
  find_hole.push_back(ordered[index]);
  VertexHandle next = Connectivity::boundary_step(mesh, ordered[index], corners[0], corners[1]);
  find_hole.push_back(next);
  next = Connectivity::boundary_step(mesh, next, corners[2], corners[0]);
  find_hole.push_back(next);
  Vec3f hole_center(0, 0, 0);
  for (VertexHandle vh : find_hole)
  {
    hole_center[0] += mesh.vertex(vh)[0];
    hole_center[1] += mesh.vertex(vh)[1];
  }
  for (size_t i=0; i<2; i++) hole_center[i] = hole_center[i] / 3.0;
  vector<VertexHandle> flips3 = find_vertices_to_flip(mesh, hole_center, side_length);

  vector<VertexHandle> balance_later;
  for (size_t i = 0; i < flips3.size(); i++)
  {
    VertexHandle b = irreg(mesh, flips3[i]);
    if (b != VertexHandle(-1)) balance_later.push_back(b);
  }

  // TODO balancing - gradual shift
  for (size_t b = 0; b < balance_later.size(); b++)
  {
    avg_shift(mesh, balance_later[b]);
  }
//   balance_entire_CoST(mesh);
}

} // namespace Irregularity

#endif // IRREGULARITY_HH