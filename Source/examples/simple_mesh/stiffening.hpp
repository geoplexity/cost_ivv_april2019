/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STIFFENING_H
#define STIFFENING_H

#include "connectivity.hpp"

namespace Stiffening {

  void boundary_stiffen(TetMeshV3f& mesh);
  void boundary_stiffen_one_face(TetMeshV3f& mesh, vector<VertexHandle> corners);
  void pin_corner_to_face(TetMeshV3f& mesh, VertexHandle a_Corner, vector<VertexHandle> a_Face);
  int find_k(TetMeshV3f mesh);
  vector< vector<VertexHandle> > order_vertices(TetMeshV3f, vector<VertexHandle> corners);
  void stiffen_rec(TetMeshV3f& mesh, const std::vector<VertexHandle>, const int, int);
  std::pair<std::vector<VertexHandle>, std::vector<VertexHandle>> peel(const std::vector<VertexHandle>, const int);
  void stiffen_outer_ring(TetMeshV3f& mesh, const std::vector<VertexHandle>,const int);
  std::vector<VertexHandle> merge(TetMeshV3f& mesh, const std::vector<VertexHandle>, const std::vector<VertexHandle>, const int, const int);
  void merge_case_0(TetMeshV3f& mesh, const std::vector<VertexHandle>, const int);
  void merge_case_1(TetMeshV3f& mesh, const std::vector<VertexHandle>, const int);
  void merge_base_case_2(TetMeshV3f& mesh, const std::vector<VertexHandle>);
  void merge_base_case_3(TetMeshV3f& mesh, const std::vector<VertexHandle>);
  void merge_base_case_6(TetMeshV3f& mesh, const std::vector<VertexHandle>);
  void merge_base_case_7(TetMeshV3f& mesh, const std::vector<VertexHandle>);
  void handle_base_case(TetMeshV3f& mesh, const std::vector<VertexHandle>, const int);
  void handle_k_equal_2(TetMeshV3f& mesh, const std::vector<VertexHandle>);
  void handle_k_equal_3(TetMeshV3f& mesh, const std::vector<VertexHandle>);
  void handle_k_equal_4(TetMeshV3f& mesh, const std::vector<VertexHandle>);
  void handle_k_equal_5(TetMeshV3f& mesh, const std::vector<VertexHandle>);
  void handle_k_equal_6(TetMeshV3f& mesh, const std::vector<VertexHandle>);
  void handle_k_equal_7(TetMeshV3f& mesh, const std::vector<VertexHandle>);

int find_k(TetMeshV3f mesh)
{
  vector<VertexHandle> corners = Connectivity::get_tet_corners(mesh);
  //std::cout << "corners" << std::endl;
  //for (VertexHandle vh : corners) std::cout << mesh.vertex(vh) << std::endl;
  vector< vector<VertexHandle> > vecsOfVertices = order_vertices(mesh, corners);
  //std::cout << "order_vertices" << std::endl;
  //for (VertexHandle vh : vecsOfVertices[0]) std::cout << mesh.vertex(vh) << std::endl;
  VertexHandle curr = Connectivity::find_first_vertex(mesh, corners[0], corners[1], corners[2], corners[3]);
  VertexHandle next = curr;
  size_t k = 1; // counts the size of the outer layer
  // k starts at 1 because the outer layer is missing corners
  while ( next != VertexHandle(-1) )
  {
    curr = next;
    // one step in the corners[0]=>corners[1] direction
    next = Connectivity::boundary_step(mesh, curr, corners[0], corners[1]); 
    k++;
  } // TODO delete this
  return k;
}

/*
 *  Calls recursive method to stiffen the graph
 */
void boundary_stiffen(TetMeshV3f& mesh)
{
  vector<VertexHandle> corners = Connectivity::get_tet_corners(mesh);
  //std::cout << "corners" << std::endl;
  vector< vector<VertexHandle> > vecsOfVertices = order_vertices(mesh, corners);
  //std::cout << "order_vertices" << std::endl;
  VertexHandle curr = Connectivity::find_first_vertex(mesh, corners[0], corners[1], corners[2], corners[3]);
  //std::cout << "find_first_vertex" << std::endl;
  VertexHandle next = curr;
  size_t k = 1; // counts the size of the outer layer
  // k starts at 1 because the outer layer is missing corners
  while ( next != VertexHandle(-1) )
  {
    curr = next;
    // one step in the corners[0]=>corners[1] direction
    next = Connectivity::boundary_step(mesh, curr, corners[0], corners[1]); 
    //std::cout << "boundary_step" << std::endl;
    k++;
  } // TODO delete this
  //std::cout << "k is " << k << std::endl;
  for (size_t f = 0; f < 4; f++) stiffen_rec(mesh, vecsOfVertices[f], k, 1);
}



/*
  Stiffens one face of the tetrahedral mesoregion.
  The vector corners[] must consist of the four corner vertex handles of the tetrahedral mesoregion
  in the following order:
  corners[0..2] are the corners that describe the face you want to stiffen,
  corners[3] is the corner vertex that is not part of the face you want to stiffen.
  NOTE: the corners are not actually on the face boundary, but it is obvious what the four corners are
  when looking at a 3D view of the tet mesoregion
*/
void boundary_stiffen_one_face(TetMeshV3f& mesh, vector<VertexHandle> corners)
{
  vector< vector<VertexHandle> > vecsOfVertices = order_vertices(mesh, corners);
  VertexHandle curr = Connectivity::find_first_vertex(mesh, corners[0], corners[1], corners[2], corners[3]);
  VertexHandle next = curr;
  size_t k = 1; // counts the size of the outer layer
  // k starts at 1 because the outer layer is missing corners
  while ( next != VertexHandle(-1) )
  {
    curr = next;
    // one step in the corners[0]=>corners[1] direction
    next = Connectivity::boundary_step(mesh, curr, corners[0], corners[1]); 
    k++;
  } // TODO delete this
  //std::cout << "k is " << k << std::endl;
  stiffen_rec(mesh, vecsOfVertices[0], k, 1);

}

/*
 *  Input: a CoST structure on which to perform the pinning
 *         the vertex to pin
 *         the corner vertices which make up the face on which to pin
 *
 *         We assume that a_Corner is in a_Face
 *
 *         TODO: This method was quickly written to get it done in
 *         time for the paper deadline.  It should be cleaned up!
 */
void pin_corner_to_face(TetMeshV3f& mesh, const VertexHandle a_Corner, const vector<VertexHandle> a_Face)
{
     // Find remaining corner of the CoST
     //      Collect all corners
     vector<VertexHandle> t_CoSTCorners = Connectivity::get_tet_corners(mesh);
     // Remove corners which are present in a_Face
     // Corner 0
     auto t_Location = find(t_CoSTCorners.begin(), t_CoSTCorners.end(), a_Face[0]);
     t_CoSTCorners.erase(t_Location);
     // Corner 1
     t_Location = find(t_CoSTCorners.begin(), t_CoSTCorners.end(), a_Face[1]);
     t_CoSTCorners.erase(t_Location);
     // Corner 2
     t_Location = find(t_CoSTCorners.begin(), t_CoSTCorners.end(), a_Face[2]);
     t_CoSTCorners.erase(t_Location);
     // Add corners back in.
     // We need the non-face vertex to be the last one in the vector
     // This is admittedly a slow method, we can optimize later
     for(auto t_FaceVert : a_Face){
         t_CoSTCorners.insert(t_CoSTCorners.begin(), t_FaceVert);
     }

     // Collect the vertices of the face
     //      Be slow but easy about it, call order_vertices
     //      Later: optimize if necessary
     auto t_BoundaryVerts = (order_vertices(mesh, t_CoSTCorners))[0];
     int k = find_k(mesh);
     // Add the new edges
     //      Find starting vertex (which corner of the face?)
     Vec3f t_CornerVert = mesh.vertex(a_Corner);
     vector<double> t_DistFromCorner;
     for(auto t_FaceVertHandle : a_Face){
         Vec3f t_FaceVert = mesh.vertex(t_FaceVertHandle);
         t_DistFromCorner.push_back((t_CornerVert - t_FaceVert).norm());
     }
     auto t_MinDist = min_element(t_DistFromCorner.begin(), t_DistFromCorner.end());
     auto t_MinDistIndex = distance(t_DistFromCorner.begin(), t_MinDist);
     //      Compute indices of relevant vertices (depends on k)
     /*
      *     Goal:
      *           a_Corner = 3
      *           /    |    \
      *          4     |     2
      *         /  \   |   / \
      *        /    \  |  /   \
      *       5        9       1
      *      /|\              /|\
      *     / | \            / | \
      *    6  |  10         8  |  0
      *     \ | /            \ | /
      *      \|/              \|/
      *       11      13       7
      *        \      /|\      /
      *         \    / | \    /
      *          \  /  |  \  /
      *           14   |   12
      *            \   |   /
      *             \  |  /
      *              \ | /
      *               15
      */
     int t_Vert4 = (k-1) * t_MinDistIndex;
     int t_Vert9 = (k-1)*3 + (k-2)*t_MinDistIndex - (t_MinDistIndex+1);
     int t_Vert13 = (k-1)*3 + ((k-2-1)*3) + (k-3)*t_MinDistIndex-(3*t_MinDistIndex+1);
     int t_Vert15 = (k-1)*3 + ((k-2-1)*3) + (k-3-2-1)*3 + (k-4)*t_MinDistIndex-(5*t_MinDistIndex+1);

     vector<int> t_VertIndices;
     t_VertIndices.reserve(16);
     t_VertIndices.push_back(t_Vert4-3);
     t_VertIndices.push_back(t_Vert4-2);
     t_VertIndices.push_back(t_Vert4-1);
     t_VertIndices.push_back(a_Corner);
     t_VertIndices.push_back(t_Vert4-0);
     t_VertIndices.push_back(t_Vert4+1);
     t_VertIndices.push_back(t_Vert4+2);
     
     t_VertIndices.push_back(t_Vert9-2);
     t_VertIndices.push_back(t_Vert9-1);
     t_VertIndices.push_back(t_Vert9-0);
     t_VertIndices.push_back(t_Vert9+1);
     t_VertIndices.push_back(t_Vert9+2);

     t_VertIndices.push_back(t_Vert13-1);
     t_VertIndices.push_back(t_Vert13-0);
     t_VertIndices.push_back(t_Vert13+1);
     
     t_VertIndices.push_back(t_Vert15);

     // Find the VertexHandles for the respective indices
     vector<VertexHandle> t_VertsToStiffen;
     t_VertsToStiffen.reserve(15);
     for(auto t_Index : t_VertIndices){
         if(t_Index == a_Corner){
             t_VertsToStiffen.push_back((VertexHandle)t_Index);
         }
         else{
             t_VertsToStiffen.push_back(t_BoundaryVerts[t_Index]);
         }
     }

     //      Add the edges
     mesh.add_edge(t_VertsToStiffen[0], t_VertsToStiffen[1]);
     mesh.add_edge(t_VertsToStiffen[1], t_VertsToStiffen[8]);
     mesh.add_edge(t_VertsToStiffen[8], t_VertsToStiffen[7]);
     mesh.add_edge(t_VertsToStiffen[7], t_VertsToStiffen[0]);
     mesh.add_edge(t_VertsToStiffen[1], t_VertsToStiffen[7]);
     mesh.add_edge(t_VertsToStiffen[1], t_VertsToStiffen[2]);

     mesh.add_edge(t_VertsToStiffen[2], t_VertsToStiffen[3]);
     mesh.add_edge(t_VertsToStiffen[3], t_VertsToStiffen[4]);
     mesh.add_edge(t_VertsToStiffen[4], t_VertsToStiffen[9]);
     mesh.add_edge(t_VertsToStiffen[2], t_VertsToStiffen[9]);
     mesh.add_edge(t_VertsToStiffen[3], t_VertsToStiffen[9]);
     mesh.add_edge(t_VertsToStiffen[4], t_VertsToStiffen[5]);

     mesh.add_edge(t_VertsToStiffen[5], t_VertsToStiffen[6]);
     mesh.add_edge(t_VertsToStiffen[6], t_VertsToStiffen[11]);
     mesh.add_edge(t_VertsToStiffen[11], t_VertsToStiffen[10]);
     mesh.add_edge(t_VertsToStiffen[10], t_VertsToStiffen[5]);
     mesh.add_edge(t_VertsToStiffen[5], t_VertsToStiffen[11]);
     mesh.add_edge(t_VertsToStiffen[11], t_VertsToStiffen[14]);

     mesh.add_edge(t_VertsToStiffen[14], t_VertsToStiffen[15]);
     mesh.add_edge(t_VertsToStiffen[15], t_VertsToStiffen[12]);
     mesh.add_edge(t_VertsToStiffen[12], t_VertsToStiffen[13]);
     mesh.add_edge(t_VertsToStiffen[13], t_VertsToStiffen[14]);
     mesh.add_edge(t_VertsToStiffen[13], t_VertsToStiffen[15]);
     mesh.add_edge(t_VertsToStiffen[12], t_VertsToStiffen[7]);

     // Move the outer vertices inwards a little
     //      For each vertex,
     for(VertexHandle t_Vert : t_VertsToStiffen)
     {
         if(t_Vert == a_Corner){
             continue;
         }
     //          Find tetrahedron it is a part of
        CellHandle t_IncidentCell = *(mesh.vc_iter(t_Vert));
     //          Find barycenter of that tet
        Vec3f t_BaryCenter{0.0, 0.0, 0.0};
        for(auto t_VertexIt=mesh.cv_iter(t_IncidentCell); t_VertexIt.valid(); ++t_VertexIt)
        {
            t_BaryCenter += mesh.vertex(*t_VertexIt);
        }
        t_BaryCenter /= 4.0;
     //          Move vertex towards barycenter
        Vec3f t_OriginalVert = (mesh.vertex(t_Vert));
        Vec3f t_NewVert = t_OriginalVert + 0.5*(t_BaryCenter-t_OriginalVert);
        mesh.set_vertex(t_Vert, t_NewVert);
     }
    
}


/*
  Returns vector of vector of vertices sorted in order necessary for boundary stiffening code
*/
vector< vector<VertexHandle> >
order_vertices(TetMeshV3f mesh, vector<VertexHandle> corners)
{
  vector< vector<VertexHandle> > out;
  
  for ( size_t j = 0; j < 4; j++)
  {
    vector<VertexHandle> ordered_verts;
    VertexHandle curr = Connectivity::find_first_vertex(mesh, corners[j], corners[(j+1)%4], corners[(j+2)%4], corners[(j+3)%4]);
    VertexHandle next = curr;
    size_t k = 1; // counts the size of the outer layer
    // k starts at 1 because the outer layer is missing corners
    while ( next != VertexHandle(-1) )
    {
      curr = next;
      ordered_verts.push_back(curr);
      // one step in the corners[j]=>corners[j+1] direction
      next = Connectivity::boundary_step(mesh, curr, corners[j], corners[(j+1)%4]); 
      k++;
    }

    // one step in the corners[j]=>corners[j+2] direction to skip over corner
    /* 
      (if j is 0, and the face is defined ccw, with the first vertex in the bottom left corner,
      then this is going diagonally up and to the right)
    */
    next = curr = Connectivity::boundary_step(mesh, curr, corners[j], corners[(j+2)%4]);
    while ( next != VertexHandle(-1) )
    {
      curr = next;
      ordered_verts.push_back(curr);
      // if j is 0, and the face is defined ccw, then this is going diagonally UP and to the LEFT
      next = Connectivity::boundary_step(mesh, curr, corners[(j+1)%4], corners[(j+2)%4]);
    }

    // if j is 0 and the face ccw, then this is one step left to skip over corner
    next = curr = Connectivity::boundary_step(mesh, curr, corners[(j+1)%4], corners[j]);
    while ( next != VertexHandle(-1) )
    {
      curr = next;
      ordered_verts.push_back(curr);
      // if j is 0 and the face ccw, then this is one step DOWN and LEFT
      next = Connectivity::boundary_step(mesh, curr, corners[(j+2)%4], corners[j]);
    }

    if ( k == 3)
    {
      curr = Connectivity::boundary_step(mesh, curr, corners[j], corners[(j+1)%4]);
      ordered_verts.push_back(curr);
      k = -1;
    }
    else if ( k > 3 )
    {
      curr = Connectivity::boundary_step(mesh, Connectivity::boundary_step(mesh, curr, corners[j], corners[(j+1)%4]), corners[j], corners[(j+1)%4]);
      k -= 3;
    }
    
    // now ordering the inner rings
    while ( k > 0 )
    {
      ordered_verts.push_back(curr);
      for (size_t i = 1; i < k; i++)
      {
        curr = Connectivity::boundary_step(mesh, curr, corners[j], corners[(j+1)%4]);
        ordered_verts.push_back(curr);
      }
      curr = Connectivity::boundary_step(mesh, curr, corners[(j+1)%4], corners[(j+2)%4]);
      ordered_verts.push_back(curr);
      for (size_t i = 1; i < k; i++)
      {
        curr = Connectivity::boundary_step(mesh, curr, corners[(j+1)%4], corners[(j+2)%4]);
        ordered_verts.push_back(curr);
      }
      curr = Connectivity::boundary_step(mesh, curr, corners[(j+2)%4], corners[j]);
      ordered_verts.push_back(curr);
      for (size_t i = 1; i < k; i++)
      {
        curr = Connectivity::boundary_step(mesh, curr, corners[(j+2)%4], corners[j]);
        ordered_verts.push_back(curr);
      }

      if (k<3)
      {
        k = -1; // why is this not changing the value of k??
        break;
      }
      else if (k==3)
      {
        // 1 step right and 1 step up-right
        curr = Connectivity::boundary_step(mesh, curr, corners[j], corners[(j+1)%4]);
        curr = Connectivity::boundary_step(mesh, curr, corners[j], corners[(j+2)%4]);
        ordered_verts.push_back(curr);
        k = -1;
        break;
      }
      else // k > 3
      {
        // 2 steps to the right and one up-right
        curr = Connectivity::boundary_step(mesh, Connectivity::boundary_step(mesh, curr, corners[j], corners[(j+1)%4]), corners[j], corners[(j+1)%4]);
        curr = Connectivity::boundary_step(mesh, curr, corners[j], corners[(j+2)%4]);
        k-=3;
      }
    }

    //std::cout << "face " << mesh.vertex(corners[j]) << " , " << mesh.vertex(corners[(j+1)%4]) << " , "
      //<< mesh.vertex(corners[(j+2)%4]) << std::endl;
    for (size_t i = 0; i < ordered_verts.size(); i++)
    {
      //std::cout << "ordered " << i << " is: " << mesh.vertex(ordered_verts[i]) << std::endl;
    }

    out.push_back(ordered_verts);
  }
  return out;
}

/*
 *  Recursive method to stiffen the graph
 */
/*std::vector<VertexHandle>*/
void stiffen_rec(TetMeshV3f& mesh, const std::vector<VertexHandle> verts, const int local_k, int merge_case)
{
  //std::cout << "stffen_rec" << verts.size() << " " << local_k << " " << merge_case << std::endl;
    /*
     *  Handle the base cases
     */
    if(local_k < 8){
        return handle_base_case(mesh, verts, local_k);
    }


    /*
     *  Split the graph into inner and outer components
     */
    auto [inner, outer] = peel(verts, local_k); // structured bindings require g++ 7 or later

    /*
     *  Stiffen outer
     */
    stiffen_outer_ring(mesh, outer, local_k);
    
    /*
     *  Stiffen inner recursively
     */
    merge_case = 1-merge_case; // case=0 -> outer contributes two edges, inner contributes one
                               // case=1 -> outer contributes one edge, inner contributes two 
    stiffen_rec(mesh, inner, local_k-6, merge_case);


    /*
     *  Combine inner and outer
     */
    std::vector<VertexHandle> result= merge(mesh, inner, outer, local_k, merge_case);
    
    // return result;
    
}

/*
 *  Splits the graph into two components:
 *  (1) The outer two ring of vertices plus the inner triangle vertices
 *  (2) The inner almost-hex
 */
pair<std::vector<VertexHandle>, std::vector<VertexHandle>> peel(const std::vector<VertexHandle> verts, const int local_k)
{
    std::vector<VertexHandle> inner;
    std::vector<VertexHandle> outer;

    size_t split_point = 3*(local_k-1) + 3*(local_k-3);
    size_t tri_point_1 = split_point + (local_k-7);
    size_t tri_point_2 = tri_point_1 + (local_k-6);
    size_t tri_point_3 = tri_point_2 + (local_k-6);

    // Outer ring
    for(size_t i=0; i<split_point; ++i)
    {
        outer.push_back(verts[i]);
    }
    // Corners of inner triangle
    outer.push_back(verts[tri_point_1]);
    outer.push_back(verts[tri_point_2]);
    outer.push_back(verts[tri_point_3]);
   
    // Inner almost-hex
    for(size_t i=split_point; i<verts.size(); ++i)
    {
        if(i==tri_point_1 || i==tri_point_2 || i==tri_point_3){
            continue;
        }
        inner.push_back(verts[i]);
    }

    return make_pair(inner, outer);
}


/*
 *  Given an outer two ring (plus the three triangle vertices
 *  which we ignore), generates a 3-regular graph on the vertices
 *
 *  Convention: The three points of the inner triangles are in 
 *  clockwise order starting at the lower left, and they are in 
 *  the last three indices of the verts vector
 */
void stiffen_outer_ring(TetMeshV3f& mesh, const std::vector<VertexHandle> verts, const int local_k)
{
    // Add outer ring of edges
    for(int i=0; i<3*(local_k-1)-1; ++i)
    {
      //std::cout << "adding edge outer ring" << std::endl;
        mesh.add_edge(verts[i], verts[i+1]);
    }
    mesh.add_edge(verts[3*(local_k-1)-1], verts[0]);

    // Inner ring parallel to outer ring of edges
    int offset = 3*(local_k-1);
    for(int i=offset; i< 3*(local_k-1) + 3*(local_k-3)-1; ++i)
    {
        if(i == offset + (local_k-3)-2){
            continue;
        }
        else if(i == offset + 2*(local_k-3)-2){
            continue;
        }
        else if(i == offset + 3*(local_k-3)-2){
            continue;
        }
        mesh.add_edge(verts[i], verts[i+1]);
    }
    mesh.add_edge(verts[offset], verts[offset + 3*(local_k-3)-1]);

    // Add the edges connecting the inner ring to the outer ring
    
    // Bottom row:
    mesh.add_edge(verts[offset-1], verts[offset + 3*(local_k-3)-1]);
    mesh.add_edge(verts[0], verts[offset + 3*(local_k-3)-1]);
    for(int i=1; i<(local_k-1)-2; ++i)
    {
        mesh.add_edge(verts[i], verts[i+offset-1]);
    }
    mesh.add_edge(verts[(local_k-1)-2], verts[(local_k-1)-2+offset-2]);
    
    // Right row:
    int right_offset = offset + local_k-4;
    mesh.add_edge(verts[right_offset], verts[local_k-2]);
    mesh.add_edge(verts[right_offset], verts[local_k-1]);
    for(int i=right_offset; i<right_offset+local_k-3; ++i)
    {
        mesh.add_edge(verts[i], verts[i-right_offset+local_k-1]);
    }
    mesh.add_edge(verts[right_offset+local_k-4], verts[2*(local_k-2)]);
    
    // Right row:
    int left_offset = right_offset + local_k-3;
    mesh.add_edge(verts[left_offset], verts[2*local_k-3]);
    for(int i=left_offset; i<left_offset+local_k-3; ++i)
    {
        mesh.add_edge(verts[i], verts[i-left_offset+2*local_k-2]);
    }
    mesh.add_edge(verts[left_offset+local_k-4], verts[3*(local_k-2)+1]);

    
}


/*
 *  Given and inner almost hex and an outer ring,
 *  merge them and connect them in a way such that
 *  the entire graph is rigid.
 *
 *  Two steps:
 *      1) Merge the two vectors into one vector
 *      2) Add stiffening edges
 */
std::vector<VertexHandle> merge(TetMeshV3f& mesh, const std::vector<VertexHandle> inner, std::vector<VertexHandle> outer, const int outer_k, const int merge_case)
{
    std::vector<VertexHandle> merged_verts;

    int inner_k = outer_k-6;

    VertexHandle tri_point_3 = outer.back();
    outer.erase(outer.end());

    VertexHandle tri_point_2 = outer.back();
    outer.erase(outer.end());

    VertexHandle tri_point_1 = outer.back();
    outer.erase(outer.end());

    // Step 1:
    // a) Merge the two vectors
    for(const auto v : outer){
        merged_verts.push_back(v);
    }
    for(const auto v : inner){
        merged_verts.push_back(v);
    }
    
    // b) Move the triangle verts to the correct location
    auto first_pos = merged_verts.begin() + (outer.size()-1);
    merged_verts.insert(first_pos + 1*inner_k, tri_point_1);
    merged_verts.insert(first_pos + 2*inner_k, tri_point_2);
    merged_verts.insert(first_pos + 3*inner_k, tri_point_3);

    // Step 2:
    switch (inner_k) {
        case 2 : merge_base_case_2(mesh, merged_verts);
                 break;
        case 3 : merge_base_case_3(mesh, merged_verts);
                 break;
        case 6 : merge_base_case_6(mesh, merged_verts);
                 break;
        case 7 : merge_base_case_7(mesh, merged_verts);
                 break;
        default: if     (merge_case==0) merge_case_0(mesh, merged_verts, outer_k);
                 else if(merge_case==1) merge_case_1(mesh, merged_verts, outer_k);
                 break;
    }

    return merged_verts;
}


/*
 *  Handles merging an outer ring with an inner
 *  almost-hex when the outer ring contributes
 *  two edges and the inner hex contributes one.
 *
 *  Sorry for the mess of indices.  It was really
 *  challenging to get it right...
 */
void merge_case_0(TetMeshV3f& mesh, const std::vector<VertexHandle> verts, const int local_k)
{
    /*
     *  Lower Left Vertex
     */
    int tri_1 = 3*(local_k-1) + 3*(local_k-3) + 3*(local_k-7) + 2;
    int inner_1 = tri_1-1;
    int outer_1 = 3*(local_k-1) + 3*(local_k-3) - 3;
    int outer_2 = outer_1 + 1;

    // Add the necessary edges
    mesh.add_edge(verts[tri_1], verts[inner_1]);
    mesh.add_edge(verts[tri_1], verts[outer_1]);
    mesh.add_edge(verts[tri_1], verts[outer_2]);
    
    // Delete the unnecessary edges
    auto halfedge = mesh.halfedge(verts[outer_1], verts[outer_2]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    /*
     *  Upper Vertex
     */
    tri_1 = 3*(local_k-1) + 3*(local_k-3) + 2*(local_k-7) + 1;
    inner_1 = tri_1-1;
    outer_1 = 3*(local_k-1) + 2*(local_k-3) - 3;
    outer_2 = outer_1 + 1;

    // Add the necessary edges
    mesh.add_edge(verts[tri_1], verts[inner_1]);
    mesh.add_edge(verts[tri_1], verts[outer_1]);
    mesh.add_edge(verts[tri_1], verts[outer_2]);
    
    // Delete the unnecessary edges
    halfedge = mesh.halfedge(verts[outer_1], verts[outer_2]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
   
    
    /*
     *  Lower Right Vertex
     */
    tri_1 = 3*(local_k-1) + 3*(local_k-3) + 1*(local_k-7) + 0;
    inner_1 = tri_1-1;
    outer_1 = 3*(local_k-1) + (local_k-3) - 3;
    outer_2 = outer_1 + 1;
    
    // Add the necessary edges
    mesh.add_edge(verts[tri_1], verts[inner_1]);
    mesh.add_edge(verts[tri_1], verts[outer_1]);
    mesh.add_edge(verts[tri_1], verts[outer_2]);
    
    // Delete the unnecessary edges
    halfedge = mesh.halfedge(verts[outer_1], verts[outer_2]);
    mesh.delete_edge(mesh.edge_handle(halfedge));

}


/*
 *  Handles merging an outer ring with an inner
 *  almost-hex when the outer ring contributes
 *  one edge and the inner hex contributes two.
 *
 *  Sorry for the mess of indices.  It was really
 *  challenging to get it right...
 */
void merge_case_1(TetMeshV3f& mesh, const std::vector<VertexHandle> verts, const int local_k)
{
    /*
     *  Lower Left Vertex
     */
    int tri_1 = 3*(local_k-1) + 3*(local_k-3) + 3*(local_k-7) + 2;

    int inner_1 = tri_1 - (3*(local_k-7) + 2);
    int inner_2 = tri_1 - 1;
    int inner_3 = tri_1 - 3*(local_k-5) + 2;

    int outer_1 = 3*(local_k-1) - 2;
    int outer_2 = tri_1 - 3*(local_k-6);
    int outer_3 = tri_1 - 3*(local_k-6) - 1;
   
    // Add the necessary edges
    mesh.add_edge(verts[tri_1], verts[inner_1]);
    mesh.add_edge(verts[tri_1], verts[inner_2]);
    mesh.add_edge(verts[tri_1], verts[inner_3]);
    
    mesh.add_edge(verts[outer_1], verts[outer_2]);

    // Delete the uneccessary ones
    auto halfedge = mesh.halfedge(verts[inner_1], verts[inner_2]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    halfedge = mesh.halfedge(verts[outer_1], verts[outer_3]);
    mesh.delete_edge(mesh.edge_handle(halfedge));

    halfedge = mesh.halfedge(verts[outer_1 + 1], verts[outer_3 + 1]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    /*
     *  Upper Vertex
     */
    tri_1 = 3*(local_k-1) + 3*(local_k-3) + 2*(local_k-7) + 1;

    inner_1 = tri_1 - (3*(local_k-5)) -1;
    inner_2 = tri_1 - 1;
    inner_3 = tri_1 + 1;

    outer_1 = inner_1;
    outer_2 = outer_1+1;
    outer_3 = 2*(local_k-2);

    // Add the necessary edges
    mesh.add_edge(verts[tri_1], verts[inner_1]);
    mesh.add_edge(verts[tri_1], verts[inner_2]);
    mesh.add_edge(verts[tri_1], verts[inner_3]);
    
    mesh.add_edge(verts[outer_2], verts[outer_3]);

    // Delete the uneccessary ones
    halfedge = mesh.halfedge(verts[inner_2], verts[inner_3]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    halfedge = mesh.halfedge(verts[outer_1], verts[outer_3]);
    mesh.delete_edge(mesh.edge_handle(halfedge));

    halfedge = mesh.halfedge(verts[outer_1 + 1], verts[outer_3 + 1]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    /*
     *  Lower Right Vertex
     */
    tri_1 = 3*(local_k-1) + 3*(local_k-3) + 1*(local_k-7) + 0;

    inner_1 = tri_1 - (3*(local_k-4)) - 1;
    inner_2 = tri_1 - 1;
    inner_3 = tri_1 + 1;

    outer_1 = inner_1;
    outer_2 = outer_1+1;
    outer_3 = (local_k-2)-1;

    // Add the necessary edges
    mesh.add_edge(verts[tri_1], verts[inner_1]);
    mesh.add_edge(verts[tri_1], verts[inner_2]);
    mesh.add_edge(verts[tri_1], verts[inner_3]);
    
    mesh.add_edge(verts[outer_2], verts[outer_3]);

    // Delete the uneccessary ones
    halfedge = mesh.halfedge(verts[inner_2], verts[inner_3]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    halfedge = mesh.halfedge(verts[outer_1], verts[outer_3]);
    mesh.delete_edge(mesh.edge_handle(halfedge));

    halfedge = mesh.halfedge(verts[outer_1 + 1], verts[outer_3 + 1]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    
}


/*
 *  The next 4 helpers merge an outer hex with an
 *  inner almost-hex of base cases k=2, 3, 6, 7.
 *
 *  For k=4, 5 we can use the usual algorithm
 */

void merge_base_case_2(TetMeshV3f& mesh, const std::vector<VertexHandle> verts)
{
    // Add the required edges
    mesh.add_edge(verts[34], verts[41]);
    mesh.add_edge(verts[33], verts[41]);
    mesh.add_edge(verts[40], verts[41]);

    mesh.add_edge(verts[23], verts[37]);
    mesh.add_edge(verts[24], verts[37]);
    mesh.add_edge(verts[36], verts[37]);

    mesh.add_edge(verts[28], verts[39]);
    mesh.add_edge(verts[29], verts[39]);
    mesh.add_edge(verts[38], verts[39]);

    // Delete the unnecessary edges
    auto halfedge = mesh.halfedge(verts[33], verts[34]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    halfedge = mesh.halfedge(verts[23], verts[24]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    halfedge = mesh.halfedge(verts[28], verts[29]);
    mesh.delete_edge(mesh.edge_handle(halfedge));

}

void merge_base_case_3(TetMeshV3f& mesh, const std::vector<VertexHandle> verts)
{
    // Add the required edges
    mesh.add_edge(verts[39], verts[50]);
    mesh.add_edge(verts[40], verts[50]);
    mesh.add_edge(verts[42], verts[50]);

    mesh.add_edge(verts[27], verts[44]);
    mesh.add_edge(verts[28], verts[44]);
    mesh.add_edge(verts[45], verts[44]);

    mesh.add_edge(verts[33], verts[47]);
    mesh.add_edge(verts[34], verts[47]);
    mesh.add_edge(verts[48], verts[47]);

    // Delete the unnecessary edges
    auto halfedge = mesh.halfedge(verts[39], verts[40]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    halfedge = mesh.halfedge(verts[27], verts[28]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    halfedge = mesh.halfedge(verts[33], verts[34]);
    mesh.delete_edge(mesh.edge_handle(halfedge));

    
}


void merge_base_case_6(TetMeshV3f& mesh, const std::vector<VertexHandle> verts)
{
    // Add the required edges
    mesh.add_edge(verts[57], verts[77]);
    mesh.add_edge(verts[58], verts[77]);
    mesh.add_edge(verts[76], verts[77]);

    mesh.add_edge(verts[39], verts[65]);
    mesh.add_edge(verts[40], verts[65]);
    mesh.add_edge(verts[64], verts[65]);

    mesh.add_edge(verts[48], verts[71]);
    mesh.add_edge(verts[49], verts[71]);
    mesh.add_edge(verts[70], verts[71]);

    // Delete the unnecessary edges
    auto halfedge = mesh.halfedge(verts[57], verts[58]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    halfedge = mesh.halfedge(verts[39], verts[40]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    halfedge = mesh.halfedge(verts[48], verts[49]);
    mesh.delete_edge(mesh.edge_handle(halfedge));

}


void merge_base_case_7(TetMeshV3f& mesh, const std::vector<VertexHandle> verts)
{
    // Add the required edges
    mesh.add_edge(verts[63], verts[86]);
    mesh.add_edge(verts[64], verts[86]);
    mesh.add_edge(verts[66], verts[86]);

    mesh.add_edge(verts[43], verts[72]);
    mesh.add_edge(verts[44], verts[72]);
    mesh.add_edge(verts[73], verts[72]);

    mesh.add_edge(verts[53], verts[79]);
    mesh.add_edge(verts[54], verts[79]);
    mesh.add_edge(verts[80], verts[79]);

    // Delete the unnecessary edges
    auto halfedge = mesh.halfedge(verts[63], verts[64]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    halfedge = mesh.halfedge(verts[43], verts[44]);
    mesh.delete_edge(mesh.edge_handle(halfedge));
    
    halfedge = mesh.halfedge(verts[53], verts[54]);
    mesh.delete_edge(mesh.edge_handle(halfedge));

}
/*
 *  Handles the base cases for k=2, 3, 4, 5, 6, 7
 */
/*std::vector<VertexHandle>*/
void handle_base_case(TetMeshV3f& mesh, const std::vector<VertexHandle> verts, const int local_k)
{
    switch (local_k) {
        case 2 : handle_k_equal_2(mesh, verts);
                 break;
        case 3 : handle_k_equal_3(mesh, verts);
                 break;
        case 4 : handle_k_equal_4(mesh, verts);
                 break;
        case 5 : handle_k_equal_5(mesh, verts);
                 break;
        case 6 : handle_k_equal_6(mesh, verts);
                 break;
        case 7 : handle_k_equal_7(mesh, verts);
                 break;
        default: //std::cout << "Unkown base case, local_k = " << local_k << std::endl;
                 break;
    }

    // return verts;
}

/*
 *  The next six functions are hard-coded
 *  to handle individual base cases.
 *
 *  To understand the convention for the vertex
 *  indices, see the file "vertex_inlabelling.ps"
 */
void handle_k_equal_2(TetMeshV3f& mesh, const std::vector<VertexHandle> verts)
{
    assert(verts.size() == 3);
   
    mesh.add_edge(verts[0], verts[1]);
    mesh.add_edge(verts[1], verts[2]);
    mesh.add_edge(verts[2], verts[0]);

}

void handle_k_equal_3(TetMeshV3f& mesh, const std::vector<VertexHandle> verts)
{
    assert(verts.size() == 7);
    
    for(int i=0; i<6; ++i)
    {
        mesh.add_edge(verts[i], verts[(i+1)%6]);
    }
    
    mesh.add_edge(verts[1], verts[6]);
    mesh.add_edge(verts[3], verts[6]);
    mesh.add_edge(verts[5], verts[6]);

}

/*
 *  TODO: k=4,5 can be implemented as an outer
 *        routine instead of an inner one
 */
void handle_k_equal_4(TetMeshV3f& mesh, const std::vector<VertexHandle> verts)
{
    assert(verts.size() == 12);
   
    for(int i=0; i<9; ++i)
    {
        mesh.add_edge(verts[i], verts[(i+1)%9]);
    }
    
    mesh.add_edge(verts[2], verts[9]);
    mesh.add_edge(verts[3], verts[9]);
    mesh.add_edge(verts[4], verts[9]);
    
    mesh.add_edge(verts[5], verts[10]);
    mesh.add_edge(verts[6], verts[10]);
    mesh.add_edge(verts[7], verts[10]);
    
    mesh.add_edge(verts[0], verts[11]);
    mesh.add_edge(verts[1], verts[11]);
    mesh.add_edge(verts[8], verts[11]);

}

void handle_k_equal_5(TetMeshV3f& mesh, const std::vector<VertexHandle> verts)
{
    assert(verts.size() == 18);
    vector<VertexHandle> face;
   
    for(int i=0; i<12; ++i)
    {
      //std::cout << "adding edge " << mesh.vertex(verts[i]) << " , " 
        //<< mesh.vertex(verts[(i+1)%12]) << std::endl;
      mesh.add_edge(verts[i], verts[(i+1)%12]);
      face.push_back(verts[i]); face.push_back(verts[(i+1)%12]);
      mesh.add_face(face);
      face.clear();
    }
    
    mesh.add_edge(verts[0], verts[17]);
    mesh.add_edge(verts[11], verts[17]);
    mesh.add_edge(verts[17], verts[12]);
    mesh.add_edge(verts[1], verts[12]);
    mesh.add_edge(verts[2], verts[12]);
    
    mesh.add_edge(verts[3], verts[13]);
    mesh.add_edge(verts[4], verts[13]);
    mesh.add_edge(verts[5], verts[14]);
    mesh.add_edge(verts[6], verts[14]);
    mesh.add_edge(verts[13], verts[14]);
    
    mesh.add_edge(verts[7], verts[15]);
    mesh.add_edge(verts[8], verts[15]);
    mesh.add_edge(verts[9], verts[16]);
    mesh.add_edge(verts[10], verts[16]);
    mesh.add_edge(verts[15], verts[16]);

    face.push_back(verts[0]); face.push_back(verts[17]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[11]); face.push_back(verts[17]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[17]); face.push_back(verts[12]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[1]); face.push_back(verts[12]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[2]); face.push_back(verts[12]);
    mesh.add_face(face);
    face.clear();

    face.push_back(verts[3]); face.push_back(verts[13]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[4]); face.push_back(verts[13]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[5]); face.push_back(verts[14]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[6]); face.push_back(verts[14]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[13]); face.push_back(verts[14]);
    mesh.add_face(face);
    face.clear();

    face.push_back(verts[7]); face.push_back(verts[15]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[8]); face.push_back(verts[15]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[9]); face.push_back(verts[16]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[10]); face.push_back(verts[16]);
    mesh.add_face(face);
    face.clear();
    face.push_back(verts[15]); face.push_back(verts[16]);
    mesh.add_face(face);
    face.clear();
}

void handle_k_equal_6(TetMeshV3f& mesh, const std::vector<VertexHandle> verts)
{
    assert(verts.size() == 25);
   
    for(int i=0; i<15; ++i)
    {
        mesh.add_edge(verts[i], verts[(i+1)%15]);
    }
    
    mesh.add_edge(verts[0], verts[23]);
    mesh.add_edge(verts[1], verts[15]);
    mesh.add_edge(verts[2], verts[16]);
    mesh.add_edge(verts[23], verts[15]);
    mesh.add_edge(verts[15], verts[16]);
    mesh.add_edge(verts[13], verts[23]);
    
    mesh.add_edge(verts[3], verts[17]);
    mesh.add_edge(verts[17], verts[18]);
    mesh.add_edge(verts[18], verts[19]);
    mesh.add_edge(verts[5], verts[17]);
    mesh.add_edge(verts[6], verts[18]);
    mesh.add_edge(verts[7], verts[19]);
    
    mesh.add_edge(verts[8], verts[20]);
    mesh.add_edge(verts[10], verts[20]);
    mesh.add_edge(verts[11], verts[21]);
    mesh.add_edge(verts[12], verts[22]);
    mesh.add_edge(verts[20], verts[21]);
    mesh.add_edge(verts[21], verts[22]);
    
    mesh.add_edge(verts[16], verts[24]);
    mesh.add_edge(verts[19], verts[24]);
    mesh.add_edge(verts[22], verts[24]);

}

void handle_k_equal_7(TetMeshV3f& mesh, const std::vector<VertexHandle> verts)
{
    assert(verts.size() == 33);
   
    for(int i=0; i<18; ++i)
    {
        if(i==0 || i==6 || i==12){
            continue;
        }
        mesh.add_edge(verts[i], verts[(i+1)%18]);
    }
    
    mesh.add_edge(verts[0], verts[29]);
    mesh.add_edge(verts[1], verts[18]);
    mesh.add_edge(verts[2], verts[19]);
    mesh.add_edge(verts[3], verts[20]);
    mesh.add_edge(verts[17], verts[29]);
    mesh.add_edge(verts[29], verts[1]);
    mesh.add_edge(verts[18], verts[19]);
    mesh.add_edge(verts[19], verts[20]);
    mesh.add_edge(verts[20], verts[4]);
    
    mesh.add_edge(verts[5], verts[21]);
    mesh.add_edge(verts[6], verts[21]);
    mesh.add_edge(verts[7], verts[21]);
    mesh.add_edge(verts[7], verts[22]);
    mesh.add_edge(verts[8], verts[23]);
    mesh.add_edge(verts[9], verts[24]);
    mesh.add_edge(verts[22], verts[23]);
    mesh.add_edge(verts[23], verts[24]);
    mesh.add_edge(verts[24], verts[10]);
    
    mesh.add_edge(verts[11], verts[25]);
    mesh.add_edge(verts[12], verts[25]);
    mesh.add_edge(verts[13], verts[25]);
    mesh.add_edge(verts[26], verts[27]);
    mesh.add_edge(verts[27], verts[28]);
    mesh.add_edge(verts[13], verts[26]);
    mesh.add_edge(verts[14], verts[27]);
    mesh.add_edge(verts[15], verts[28]);
    mesh.add_edge(verts[16], verts[28]);
    
    mesh.add_edge(verts[18], verts[32]);
    mesh.add_edge(verts[22], verts[30]);
    mesh.add_edge(verts[26], verts[31]);
    
    mesh.add_edge(verts[30], verts[31]);
    mesh.add_edge(verts[31], verts[32]);
    mesh.add_edge(verts[32], verts[30]);

}

} // namespace Stiffening

#endif // STIFFENING_H
