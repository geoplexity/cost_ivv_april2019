/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REFINEMENT_HH
#define REFINEMENT_HH

#include <cmath>

#include "connectivity.hpp"
#include "gluing.hpp"

namespace Refinement {

//===========================================================================
//==== REFINEMENT 1 =========================================================


void 
make_cell_into_four_cells(TetMeshV3f& mesh, CellHandle ch)
{
  vector<VertexHandle> verts;
  for (CellVertexIter cv_it = mesh.cv_iter(ch); cv_it.valid(); cv_it++) 
    verts.push_back(*cv_it);
  vector<EdgeHandle> del_edges;
  vector<VertexHandle> new_verts;
  for (size_t i = 1; i < 4; i++)
  {
    del_edges.push_back(mesh.edge_handle(mesh.halfedge(verts[0], verts[i])));
    del_edges.push_back(mesh.edge_handle(mesh.halfedge(verts[i], verts[(i==3)? 1:(i+1)])));
    new_verts.push_back(mesh.add_vertex(Connectivity::midpt_btw_vert_handles(mesh, verts[0], verts[i])));
  }
  new_verts.push_back(mesh.add_vertex(Connectivity::midpt_btw_vert_handles(mesh, verts[1], verts[2])));
  new_verts.push_back(mesh.add_vertex(Connectivity::midpt_btw_vert_handles(mesh, verts[1], verts[3])));
  new_verts.push_back(mesh.add_vertex(Connectivity::midpt_btw_vert_handles(mesh, verts[2], verts[3])));
  for (size_t i = 0; i < 6; i++) mesh.delete_edge(del_edges[i]);

  vector<VertexHandle> new_cell;
  new_cell.push_back(verts[0]); new_cell.push_back(new_verts[0]);
  new_cell.push_back(new_verts[1]); new_cell.push_back(new_verts[2]);
  Connectivity::add_cell_polymesh(mesh, new_cell);

  new_cell.clear();
  new_cell.push_back(verts[1]); new_cell.push_back(new_verts[0]);
  new_cell.push_back(new_verts[3]); new_cell.push_back(new_verts[4]);
  Connectivity::add_cell_polymesh(mesh, new_cell);

  new_cell.clear();
  new_cell.push_back(verts[2]); new_cell.push_back(new_verts[1]);
  new_cell.push_back(new_verts[3]); new_cell.push_back(new_verts[5]);
  Connectivity::add_cell_polymesh(mesh, new_cell);

  new_cell.clear();
  new_cell.push_back(verts[3]); new_cell.push_back(new_verts[2]);
  new_cell.push_back(new_verts[4]); new_cell.push_back(new_verts[5]);
  Connectivity::add_cell_polymesh(mesh, new_cell);

  mesh.collect_garbage();
}

void
refinement1(TetMeshV3f& mesh)
{
    std::vector<CellHandle> cells;
    for (CellIter c_it = mesh.cells_begin(); c_it != mesh.cells_end(); c_it++)
    {
        cells.push_back(*c_it);
    }
    for (size_t i = 0; i < cells.size(); i++)
    {
        make_cell_into_four_cells(mesh, cells[i]);
    }
}

//===========================================================================
//==== REFINEMENT 2 =========================================================

// TODO move to LA
Vec3f
eig2vec(Eigen::Vector3d eig)
{
  return Vec3f(eig[0], eig[1], eig[2]);
}

Eigen::Vector3d
vec2eig(Vec3f vec)
{
  return Eigen::Vector3d(vec[0], vec[1], vec[2]);
}

/*
 * TODO: Taken from Mapping
 */
double 
scalar_triple_product(Vec3f a_Vec1, Vec3f a_Vec2, Vec3f a_Vec3)
{
    Eigen::Vector3d t_Vec1 = vec2eig(a_Vec1);
    Eigen::Vector3d t_Vec2 = vec2eig(a_Vec2);
    Eigen::Vector3d t_Vec3 = vec2eig(a_Vec3);
    
    return t_Vec1.dot( t_Vec2.cross(t_Vec3) );
    // return dot( a_Vec1, cross(a_Vec2, a_Vec3));
    //return dot( cross(a_Vec1, a_Vec2), a_Vec3);
}

/*
 *  See https://stackoverflow.com/questions/38545520/barycentric-coordinates-of-a-tetrahedron
 *  TODO: taken from Mapping
 */
array<double, 4> 
find_tetrahedron_bary_coords(const Vec3f a_Point, const vector<Vec3f> a_Tet)
{
    Vec3f a = a_Tet[0];
    Vec3f b = a_Tet[1];
    Vec3f c = a_Tet[2];
    Vec3f d = a_Tet[3];
    Vec3f p = a_Point;

    Vec3f vap = p-a;
    Vec3f vbp = p-b;

    Vec3f vab = b-a;
    Vec3f vac = c-a;
    Vec3f vad = d-a;

    Vec3f vbc = c-b;
    Vec3f vbd = d-b;
    float va6 = scalar_triple_product(vbp, vbd, vbc);
    float vb6 = scalar_triple_product(vap, vac, vad);
    float vc6 = scalar_triple_product(vap, vad, vab);
    float vd6 = scalar_triple_product(vap, vab, vac);
    float v6 = 1.0 / scalar_triple_product(vab, vac, vad);

    return {va6*v6, vb6*v6, vc6*v6, vd6*v6};
}

/*
 *  Given a linear tetrahedron, fill it with a CoST
 *  TODO: taken from Mapping
 */
TetMeshV3f 
fill_linear_tet_with_CoST(TetMeshV3f& a_Mesh, const vector<Vec3f> a_Tet)
{
    TetMeshV3f t_CostTet(a_Mesh);

    /*
     * From blender:
     *      Multiply y by -1
     *      Then swap y and z
     */
    vector<Vec3f> t_TetCorners = Connectivity::get_corners_tet_mesoregion(t_CostTet); //filestructure

    for(auto v_it=t_CostTet.vertices_begin(); v_it!=t_CostTet.vertices_end(); ++v_it)
    {
        Vec3f t_Vert = t_CostTet.vertex(*v_it);
        // Find barycentric coordinates with respect to original tetrahedron
        array<double, 4> t_BaryCoords = find_tetrahedron_bary_coords(t_Vert, t_TetCorners);

        // Find new vertex position with respect to new tetrahedron
        Vec3f t_NewVert{0.0, 0.0, 0.0};
        for(int i=0; i<4; ++i)
        {
            t_NewVert += t_BaryCoords[i]*a_Tet[i];
        }
        t_CostTet.set_vertex(*v_it, t_NewVert);
    }

    return t_CostTet;
}

void
refinement2(TetMeshV3f& mesh)
{
  TetMeshV3f origMesh(mesh);
  for (CellIter c_it = origMesh.cells_begin(); c_it != origMesh.cells_end(); c_it++)
  {
    std::vector<VertexHandle> cv;
    cv = Connectivity::get_vhandles_except_connecting(mesh, *c_it, VertexHandle(-1));

    std::vector<VertexHandle> cv_to_merge;
    for (size_t i = 0; i < 4; i++)
    {
      VertexCellIter vc_it = mesh.vc_iter(cv[i]);
      vc_it++;
      if (vc_it.valid())
      {
        //std::cout << "cv_to_merge[" << cv_to_merge.size() << "]  " << mesh.vertex(cv[i]) << std::endl;
        cv_to_merge.push_back(cv[i]);
      }
    }

    vector<Vec3f> cv_vec;
    for (size_t i = 0; i < 4; i++) cv_vec.push_back(mesh.vertex(cv[i]));

    // fill cv with the original tet mesoregion
    TetMeshV3f mini = fill_linear_tet_with_CoST(origMesh, cv_vec);
    std::vector<VertexHandle> mini_cor = Connectivity::get_tet_corners(mini);

    // copy over mini into mesh
    std::unordered_map<int, VertexHandle> vert_map = Gluing::copy_mesh_into_mesh(mesh, mini);

    // delete edges of refinement cell cv
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge(cv[0], cv[1])));
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge(cv[0], cv[2])));
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge(cv[0], cv[3])));
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge(cv[1], cv[2])));
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge(cv[1], cv[3])));
    mesh.delete_edge( mesh.edge_handle( mesh.halfedge(cv[2], cv[3])));

    //std::cout << "deleted edges" << std::endl;

    std::vector<VertexHandle> ordered_mini_cor;
    for (size_t i = 0; i < cv_to_merge.size(); i++)
    {
      VertexHandle closest = mini_cor[0];
      for (size_t j = 1; j < mini_cor.size(); j++)
      {
        double dist = Connectivity::dist_btw_pts(mesh.vertex(cv_to_merge[i]), mini.vertex(mini_cor[j]));
        if ( dist < Connectivity::dist_btw_pts(mesh.vertex(cv_to_merge[i]), mini.vertex(closest)))
        {
          closest = mini_cor[j];
        }
      }
      ordered_mini_cor.push_back(closest);
    }

    //std::cout << "made ordered_mini_cor" << std::endl;

    for (size_t i = 0; i < cv_to_merge.size(); i++)
    {
      if (ordered_mini_cor[i] != VertexHandle(-1))
      {
        std::ostringstream oss;
        oss << ordered_mini_cor[i];
        int index = std::stoi(oss.str());
        Gluing::merge_vertices(mesh, vert_map[index], cv_to_merge[i]);

        //std::cout << "mini[" << i << "]  " << mesh.vertex(vert_map[index]) << std::endl;
        //std::cout << "cv[" << i << "]  " << mesh.vertex(cv[i]) << std::endl;
      }
    }

  }

  mesh.collect_garbage();
}

} // end namespace Refinement

#endif //REFINEMENT_HH
