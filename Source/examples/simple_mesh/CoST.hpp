/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CoST.h"
#include "CoST.FileIO.h"
// #include "CoST.Stiffen.h"
#include <iterator>

/*
  This constructor takes a vector of coordinates that make up a cube that will
  be filled with cristobalite. The diagram below shows the order that the
  points should be in the vector. The point at index 0 will become the 'origin'
  of the box.
*/
/*
 *        z
 *        ^
 *        |
 *        |
 *        5--------6
 *       /|       /|
 *      / |      / |
 *     4-------7|  |
 *     |  0-----|--3-------> x
 *     | /      | /
 *     1--------2
 *     /
 *    /
 *   /
 * |/_
 * y
 *
*/
CoST::CoST( vector<Vec3f> pts )
  : side_length(dist_btw_pts(pts[0], pts[1])), origin(pts[0])
{
  mesh.enable_bottom_up_incidences( true );
  generate_box();
}


/*
  creates the cristobalite that would fit inside the hole at box_origin in a
  large box of cristobalite with origin (0, 0, 0)
  TODO: right now this only works if the box_origin z coordinate is positive
*/
void
CoST::generate_box()
{
  double x_offset = fmod( origin[0], 2 );
  double y_offset = fmod( origin[1], 2*sqrt(3) );

  size_t layer = ceil( fmod( ( origin[2] / sqrt(3)), 3 ) );
  layer = layer % 3;
  double z_offset = ( ceil( origin[2]/sqrt(3) ) * sqrt(3) ) - origin[2];

  // create 2D trihex layers
  vector<TetMeshV3f> layers;
  layers = make_layers_012( side_length + 2*sqrt(3) );

  /*
    order the layers with the first type of layer appearing in the box as first
    in the vector
  */
  for ( size_t i = 0; i < layer; ++i)
  {
    layers.push_back( layers[0] );
    layers.erase( layers.begin() );
  }

  for ( size_t i = 0; i < 3; i++)
  {
    for ( VertexIter v_it = layers[i].vertices_begin();
      v_it != layers[i].vertices_end(); v_it++ )
    {
      Vec3f temp( layers[i].vertex( *v_it )[0],
        layers[i].vertex( *v_it )[1], layers[i].vertex( *v_it )[2] );
      layers[i].set_vertex( *v_it,
        Vec3f( temp[0] - x_offset, temp[1] - y_offset, temp[2] + z_offset ) );
    }
  }

  auto start = chrono::steady_clock::now();
  // Create tetrahedral mesh and stack 2D trihex layers
  stack_layers( layers );
  mesh.enable_bottom_up_incidences( true );
  auto end = chrono::steady_clock::now();
  auto time_for_stack_layers = end-start;

  start = chrono::steady_clock::now();
  // connect 2D trihex layers to form cristobalite
  connect_layers();
  end = chrono::steady_clock::now();
  auto time_for_connect_layers = end-start;

  //cout << "Time to stack layers: " << chrono::duration<double>( time_for_stack_layers ).count() << " seconds" << endl;
  //cout << "Time to connect layers: " << chrono::duration<double>( time_for_connect_layers ).count() << " seconds" << endl;

  add_border_tets();

  remove_extra_vertices(); // TODO necessary?
  remove_extra_faces(); // TODO necessary?

  mesh.collect_garbage();

  shift_everything();
}

void
CoST::shift_everything()
{
  for ( VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); v_it++ )
  {
    Vec3f old_v = mesh.vertex( *v_it );
    mesh.set_vertex( *v_it, Vec3f( old_v[0] + origin[0], old_v[1] + origin[1],
      old_v[2] + origin[2] ) );
  }
}


/*
    TODO: Do we need an obj reader?  Will be more difficult to implement
          ==> Challenge: faces will be easy, but cells will be difficult
*/
void
CoST::write_obj( string out_name )
{
  //cout << "Writing " << out_name << endl;
  std::ofstream out_file;
  out_file.open( out_name+".obj" );

  // Print name of mesh
  out_file << out_name << endl;

  // Print positions of vertices
  for( VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it )
  {
      out_file << "v " << mesh.vertex( *v_it ) << std::endl;
  }

  /*
     Print vertex indices for each face
     Must make sure to not hit faces twice.  Because the polycube mesh does
     not have any cells in it, just faces, we must use a look-up table to
     ensure we don't have duplicate faces.
  */
  set<FaceHandle> visited_faces;
  for( HalfFaceIter hf_it = mesh.halffaces_begin(); hf_it!=mesh.halffaces_end(); ++hf_it )
  {
      // Check for duplicates
      FaceHandle face = mesh.face_handle( *hf_it );
      if( visited_faces.find( face ) != visited_faces.end() ){ // Already wrote out this face
          continue;
      }
      visited_faces.insert( face );

      // Write out
      out_file << "f ";

      for( HalfFaceVertexIter v_it=mesh.hfv_iter( *hf_it ); v_it.valid(); ++v_it )
      {
          out_file << ( ( *v_it ).idx()+1 ) << " ";
      }

      out_file << std::endl;
  }

  for(auto t_EdgeIt = mesh.edges_begin(); t_EdgeIt != mesh.edges_end(); ++t_EdgeIt)
  {
      // If there are any adjacent halffaces, break
      HalfEdgeHandle t_HalfEdge = mesh.halfedge_handle(*t_EdgeIt, 0);
      for(auto t_HalfFace = mesh.hehf_iter(t_HalfEdge); t_HalfFace.valid(); ++t_HalfFace){
          break;
      }
      // This edge is part of the boundary stiffening!
      // Write it out
      OpenVolumeMeshEdge t_Edge = mesh.edge(*t_EdgeIt);
      out_file << "f " << (t_Edge.to_vertex())+1 << " " << (t_Edge.from_vertex())+1 << endl;
  }

  out_file.close();
}

/*
    TODO: Do we need an obj reader?  Will be more difficult to implement
          ==> Challenge: faces will be easy, but cells will be difficult
*/
void
CoST::write_obj( string out_name, TetMeshV3f current_mesh )
{
  //cout << "Writing " << out_name << endl;
  std::ofstream out_file;
  out_file.open( out_name+".obj" );

  // Print name of mesh
  out_file << out_name << endl;

  // Print positions of vertices
  for( VertexIter v_it = current_mesh.vertices_begin(); v_it != current_mesh.vertices_end(); ++v_it )
  {
      out_file << "v " << current_mesh.vertex( *v_it ) << std::endl;
  }

  /*
     Print vertex indices for each face
     Must make sure to not hit faces twice.  Because the polycube mesh does
     not have any cells in it, just faces, we must use a look-up table to
     ensure we don't have duplicate faces.
  */
  set<FaceHandle> visited_faces;
  for( HalfFaceIter hf_it = current_mesh.halffaces_begin(); hf_it!=current_mesh.halffaces_end(); ++hf_it )
  {
      // Check for duplicates
      FaceHandle face = current_mesh.face_handle( *hf_it );
      if( visited_faces.find( face ) != visited_faces.end() ){ // Already wrote out this face
          continue;
      }
      visited_faces.insert( face );

      // Write out
      out_file << "f ";

      for( HalfFaceVertexIter v_it=current_mesh.hfv_iter( *hf_it ); v_it.valid(); ++v_it )
      {
          out_file << ( ( *v_it ).idx()+1 ) << " ";
      }

      out_file << std::endl;
  }

  out_file.close();
}



/*
  returns the centroid of the given face as a Vec3f point
*/
Vec3f
CoST::find_center( FaceHandle fh, TetMeshV3f current_mesh )
{
  std::vector<Vec3f> vertices_around_face;
  HalfFaceVertexIter hfv_it = current_mesh.hfv_iter( current_mesh.halfface_handle( fh, 1 ) );
  for ( ; hfv_it.valid(); ++hfv_it )
  {
    vertices_around_face.push_back( current_mesh.vertex( *hfv_it ) );
  }

  Vec3f centroid( 0, 0, 0 );

  centroid[0] = ( vertices_around_face[0][0]
    + vertices_around_face[1][0]
    + vertices_around_face[2][0] ) / 3;
  centroid[1] = ( vertices_around_face[0][1]
    + vertices_around_face[1][1]
    + vertices_around_face[2][1] ) / 3;
  centroid[2] = ( ( vertices_around_face[0][2]
    + vertices_around_face[1][2]
    + vertices_around_face[2][2] ) / 3 );

  return centroid;
}

/*
  takes the stacked layers of 2D trihex and carves out a cube to correspond
  with the side length of the desired cristobalite
*/
TetMeshV3f
CoST::remove_excess_2D_trihex( double len, TetMeshV3f mesh )
{
  double new_side_length = ( sqrt(3) / 2 ) * floor( len / ( sqrt(3) / 2 ) );

  std::vector<VertexHandle> del_handles;
  for ( VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it )
  {
    Vec3f vert = mesh.vertex( *v_it );
    if ( vert[0] > new_side_length + ( ( 2 / sqrt(3) ) / 3 )
      || vert[0] < -( ( 2 / sqrt(3) ) / 3 )
      || vert[1] > new_side_length + ( ( 2 / sqrt(3) ) / 3 )
      || vert[1] < -( ( 2 / sqrt(3) ) / 3 )
      || vert[2] > side_length )
    {
      del_handles.push_back( *v_it );
    }
  }

  for ( size_t i = 0; i < del_handles.size(); ++i )
  {
    mesh.delete_vertex( del_handles[i] );
  }
  mesh.collect_garbage();

  for ( EdgeIter e_it = mesh.edges_begin(); e_it != mesh.edges_end(); ++e_it )
  {
    if ( mesh.valence( *e_it ) == 0 )
    {
      mesh.delete_edge( *e_it );
    }
  }
  mesh.collect_garbage();

  return mesh;
}

vector<TetMeshV3f>
CoST::make_layers_012( double len )
{
  // read in 2D trihex layer
  TetMeshV3f trihex;
  trihex.enable_bottom_up_incidences( true );
  string infile( "test_files/trihex.off" );
  FileIO fileio;
  trihex = fileio.read_OFF( infile );

  Vec3f origin = trihex.vertex( *( trihex.vertices_begin() ) );
  for ( VertexIter v_it = trihex.vertices_begin(); v_it != trihex.vertices_end();
    v_it++ )
  {
    if ( trihex.vertex( *v_it )[0] > 1
      && trihex.vertex( *v_it )[0] <= origin[0]
      && trihex.vertex( *v_it )[1] <= origin[1] )
    {
      origin = trihex.vertex( *v_it );
    }
  }

  vector<TetMeshV3f> layers;

  for ( size_t i = 0; i < 3; i++ )
  {
    for ( VertexIter v_it = trihex.vertices_begin(); v_it != trihex.vertices_end();
      v_it++ )
    {
      Vec3f temp( trihex.vertex( *v_it )[0], trihex.vertex( *v_it )[1],
        trihex.vertex( *v_it )[2] );
      if ( i == 0 )
      {
        trihex.set_vertex( *v_it,
          Vec3f( temp[0] - origin[0], temp[1] - origin[1], temp[2] - origin[2] ) );
      }
      else
      {
        trihex.set_vertex( *v_it,
          Vec3f( temp[0], temp[1] - ( 2 / sqrt(3) ), temp[2] ) );
      }
    }

    layers.push_back( trihex );
  }

  for ( size_t i = 0; i < 3; i++ )
  {
    layers[i] = remove_excess_2D_trihex( len, layers[i] );
  }

  return layers;
}

/*
  stacks 2D trihex layers 0 through 2 in the z direction.
  stacks first layer 0, then layer 1, then layer 2, then layer 0, ...
  until the side length is reached
*/
void
CoST::stack_layers( vector<TetMeshV3f> layers, bool tet_domain )
{
  size_t num_layers; // TODO const?
  if (tet_domain) num_layers = layers.size();
  else num_layers = floor( side_length / sqrt(3) ) + 2;
  for ( size_t l = 0; l < num_layers; l++)
  {
    size_t current_layer = l % layers.size();
    for ( FaceIter f_it = layers[current_layer].faces_begin(); f_it != layers[current_layer].faces_end(); ++f_it )
    {
      // potential speedup: maintain a set of the faces from the previous layer
      std::vector<VertexHandle> v_handles;

      HalfFaceVertexIter hfv_it = layers[current_layer].hfv_iter( layers[current_layer].halfface_handle( *f_it, 1 ) );
      for ( ; hfv_it.valid(); ++hfv_it )
      {
        double v_x, v_y, v_z;
        // set coordinates of new vertex
        v_x = layers[current_layer].vertex( *hfv_it )[0];
        v_y = layers[current_layer].vertex( *hfv_it )[1];
        v_z = layers[current_layer].vertex( *hfv_it )[2] + ( l * sqrt(3) );

        bool already_has_point = false;
        VertexIter v_it( mesh.vertices_begin() ), v_end( mesh.vertices_end() );
        for ( ; v_it != v_end; ++v_it )
        {
          if ( equalish( mesh.vertex( *v_it )[0], v_x )
            && equalish( mesh.vertex( *v_it )[1], v_y )
            && equalish( mesh.vertex( *v_it )[2], v_z ) )
          {
            already_has_point = true;
            v_handles.push_back( *v_it );
            break;
          }
        }
        if ( !already_has_point )
        {
          VertexHandle new_v = mesh.add_vertex( Vec3f( v_x, v_y, v_z ) );
          v_handles.push_back( new_v );
        }
      }

      mesh.add_face( v_handles );

    }
  }

  if (!tet_domain) mesh = remove_excess_2D_trihex( side_length, mesh );
}

/*
  connects the faces in one layer, represented by the faces in f_handles, with
  the faces in the layer above it, represented by the faces in f_handles_top.
  the faces are each connected by two tetrahedral cells to the face directly
  above it
*/
void
CoST::add_tetrahedra( vector<FaceHandle> f_handles,
  vector<FaceHandle> f_handles_top )
{
  for ( size_t k = 0; k < f_handles.size(); k++ )
  {
    vector<VertexHandle> vertices_around_face;
    HalfFaceVertexIter hfv_it = mesh.hfv_iter( mesh.halfface_handle( f_handles[k], 1 ) );
    for ( ; hfv_it.valid(); ++hfv_it )
    {
      vertices_around_face.push_back( *hfv_it );
    }

    vector<VertexHandle> vertices_around_face_top;
    hfv_it = mesh.hfv_iter( mesh.halfface_handle( f_handles_top[k], 1 ) );
    for ( ; hfv_it.valid(); ++hfv_it )
    {
      vertices_around_face_top.push_back( *hfv_it );
    }

    double x, y, z;
    // set coordinates of new vertex
    x = ( mesh.vertex( vertices_around_face[0] )[0]
      + mesh.vertex( vertices_around_face[1] )[0]
      + mesh.vertex( vertices_around_face[2] )[0] ) / 3;
    y = ( mesh.vertex( vertices_around_face[0] )[1]
      + mesh.vertex( vertices_around_face[1] )[1]
      + mesh.vertex( vertices_around_face[2] )[1] ) / 3;
    z = mesh.vertex( vertices_around_face[0] )[2] + ( sqrt(3) / 2 ); // TODO calculate height

    VertexHandle new_v = mesh.add_vertex( Vec3f( x, y, z ) );

    vector<VertexHandle> v_handles;

    v_handles.push_back( new_v );
    v_handles.push_back( vertices_around_face[0] );
    v_handles.push_back( vertices_around_face[1] );
    FaceHandle fb0 = mesh.add_face( v_handles );
    v_handles.clear();

    v_handles.push_back( new_v );
    v_handles.push_back( vertices_around_face[1] );
    v_handles.push_back( vertices_around_face[2] );
    FaceHandle fb1 = mesh.add_face( v_handles );
    v_handles.clear();

    v_handles.push_back( new_v );
    v_handles.push_back( vertices_around_face[2] );
    v_handles.push_back( vertices_around_face[0] );
    FaceHandle fb2 = mesh.add_face( v_handles );
    v_handles.clear();

    v_handles.push_back( vertices_around_face_top[2] );
    v_handles.push_back( vertices_around_face_top[0] );
    v_handles.push_back( new_v );
    FaceHandle ft0 = mesh.add_face( v_handles );
    v_handles.clear();

    v_handles.push_back( vertices_around_face_top[0] );
    v_handles.push_back( vertices_around_face_top[1] );
    v_handles.push_back( new_v );
    FaceHandle ft1 = mesh.add_face( v_handles );
    v_handles.clear();

    v_handles.push_back( vertices_around_face_top[1] );
    v_handles.push_back( vertices_around_face_top[2] );
    v_handles.push_back( new_v );
    FaceHandle ft2 = mesh.add_face( v_handles );
    v_handles.clear();

    vector<HalfFaceHandle> halffaces;

    halffaces.push_back( mesh.halfface_handle( f_handles[k], 1 ) );
    halffaces.push_back( mesh.halfface_handle( fb0, 1 ) );
    halffaces.push_back( mesh.halfface_handle( fb1, 0 ) );
    halffaces.push_back( mesh.halfface_handle( fb2, 1 ) );
    mesh.add_cell( halffaces );
    halffaces.clear();

    halffaces.push_back( mesh.halfface_handle( f_handles_top[k], 1 ) );
    halffaces.push_back( mesh.halfface_handle( ft0, 1 ) );
    halffaces.push_back( mesh.halfface_handle( ft1, 0 ) );
    halffaces.push_back( mesh.halfface_handle( ft2, 1 ) );
    mesh.add_cell( halffaces );
  }
}

void
CoST::connect_layers()
{
  auto hash_func = [] ( const Vec3f& key )
  {
    return ( std::hash<double>()( trunc( key[0] * 100 ) )
      ^ ( std::hash<double>()( trunc( key[1] * 100 ) ) << 1 ) )
      ^ std::hash<double>()( trunc( key[2] * 100 ) );
  };

  auto key_equals_func = [] ( const Vec3f& lhs, const Vec3f& rhs )
  {
    return equalish( lhs[0], rhs[0] )
      && equalish( lhs[1], rhs[1] )
      && equalish( lhs[2], rhs[2] );
  };

  // create map with key: centroid of face (Vec3f) and value: face handle
  std::unordered_map< Vec3f, FaceHandle, std::function<double( const Vec3f& )>,
    std::function<bool( const Vec3f&, const Vec3f& )> >
  face_map( 5, hash_func, key_equals_func );

  auto start = chrono::steady_clock::now(); // timer for debugging

  // go to each face and map the centroid of each face to the face
  for ( FaceIter f_it = mesh.faces_begin(); f_it != mesh.faces_end(); ++f_it )
  {
    Vec3f centroid = find_center( *f_it, mesh );
    face_map[centroid] = *f_it;
  }

  auto end = chrono::steady_clock::now();
  auto time_for_mapping = end-start;
  //cout << "Time to map centroids to faces inside connect_layers(): " << chrono::duration<double>( time_for_mapping ).count() << " seconds" << endl;

  std::vector<FaceHandle> f_handles;
  std::vector<FaceHandle> f_handles_top;

  /* match up each face with the face directly above it that will make up the
    top and bottom of two corner-sharing tetrahedra */
  for ( auto element : face_map )
  {
    Vec3f centroid = element.first;
    Vec3f centroid_top( centroid[0], centroid[1], centroid[2] + sqrt(3) );
    try
    {
      FaceHandle top_pair = face_map.at( centroid_top );
      f_handles.push_back( element.second );
      f_handles_top.push_back( top_pair );
    }
    catch ( const std::out_of_range& )
    {

    }
  }

  // connect each matching pair of faces with corner-sharing tetrahedra
  add_tetrahedra( f_handles, f_handles_top );
}

void
CoST::remove_extra_faces()
{
  vector<FaceHandle> del_handles;
  for ( FaceIter f_it = mesh.faces_begin(); f_it != mesh.faces_end(); f_it++ )
  {
    if ( mesh.incident_cell( mesh.halfface_handle( *f_it, 1 ) ) == CellHandle( -1 )
      && mesh.incident_cell( mesh.halfface_handle( *f_it, 0 ) ) == CellHandle( -1 ) )
    {
      del_handles.push_back( *f_it );
    }
    else
    {
      HalfFaceVertexIter hfv_it = mesh.hfv_iter( mesh.halfface_handle( *f_it, 1 ) );
      for ( ; hfv_it.valid(); ++hfv_it )
      {
        size_t count = 0;
        VertexFaceIter vf_it = mesh.vf_iter( *hfv_it );
        for ( ; vf_it.valid(); ++vf_it )
        {
          count++;
        }
        if ( count < 3 )
        {
          del_handles.push_back( *f_it );
          break;
        }
      }
    }
  }

  for ( size_t i = 0; i < del_handles.size(); i++ )
  {
    mesh.delete_face( del_handles[i] );
  }
  mesh.collect_garbage();

  for ( EdgeIter e_it = mesh.edges_begin(); e_it != mesh.edges_end(); ++e_it )
  {
    if ( mesh.valence( *e_it ) == 0 )
    {
      mesh.delete_edge( *e_it );
    }
  }
  mesh.collect_garbage();
}

void
CoST::remove_extra_vertices()
{
  vector<VertexHandle> del_handles;
  for ( VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); v_it++ )
  {
    Vec3f v = mesh.vertex( *v_it );
    if ( v[0] < -sqrt(3)/2 || v[0] > side_length + sqrt(3)/2
      || v[1] < -sqrt(3)/2 || v[1] > side_length + sqrt(3)/2
      || v[2] < -sqrt(3)/2 || v[2] > side_length + sqrt(3)/2 )
    {
      del_handles.push_back( *v_it );
    }
    else
    {
      VertexCellIter vc_it = mesh.vc_iter( *v_it );
      if ( !vc_it.valid() )
      {
        del_handles.push_back( *v_it );
      }
    }
  }

  for ( size_t i = 0; i < del_handles.size(); i++ )
  {
    mesh.delete_vertex( del_handles[i] );
  }

  mesh.collect_garbage();
}






double
CoST::dist_btw_pts( Vec3f v1, Vec3f v2 )
{
  return sqrt( ( ( v1[0] - v2[0] ) * ( v1[0] - v2[0] ) )
    + ( ( v1[1] - v2[1] ) * ( v1[1] - v2[1] ) )
    + ( ( v1[2] - v2[2] ) * ( v1[2] - v2[2] ) ) );
}

double
CoST::dist_btw_pts( VertexHandle v1_vh, VertexHandle v2_vh )
{
  Vec3f v1 = mesh.vertex(v1_vh);
  Vec3f v2 = mesh.vertex(v2_vh);
  return sqrt( ( ( v1[0] - v2[0] ) * ( v1[0] - v2[0] ) )
    + ( ( v1[1] - v2[1] ) * ( v1[1] - v2[1] ) )
    + ( ( v1[2] - v2[2] ) * ( v1[2] - v2[2] ) ) );
}






// ----------------------------- tet domain ------------------------------------

double
CoST::area_of_triangle( Vec3f a, Vec3f b, Vec3f c )
{
  return abs( ( a[0]*(b[1]-c[1]) + b[0]*(c[1]-a[1]) + c[0]*(a[1]-b[1]) ) /2 );
}

/*
  taken from:
  https://www.geeksforgeeks.org/check-whether-a-given-point-lies-inside-a-triangle-or-not/
*/
bool
CoST::is_inside_triangle( Vec3f point, vector<Vec3f> triangle )
{
  double abc = area_of_triangle( triangle[0], triangle[1], triangle[2] );
  double pbc = area_of_triangle( point, triangle[1], triangle[2] );
  double apc = area_of_triangle( triangle[0], point, triangle[2] );
  double abp = area_of_triangle( triangle[0], triangle[1], point );
  return equalish(abc, pbc+apc+abp);
}

vector<TetMeshV3f>
CoST::make_layers_tet_domain( vector<TetMeshV3f> trihex_layers )
{
  vector<TetMeshV3f> layers;
  size_t num_layers = side_length; // TODO calculate num_layers from real side length
  for ( size_t i = 0; i < num_layers; i++ )
  {
    TetMeshV3f temp( trihex_layers[i%3] );
    layers.push_back( temp );
  }

  vector< vector<Vec3f> > layer_domains;
  for ( size_t i = 0; i < num_layers; i++ )
  {
    double max_len = (2*(num_layers+1));
    double y_offset = ( (3.0+(2*i)) /6.0 ) * sqrt(3);
    vector<Vec3f> temp {
      Vec3f(-0.5 + i - 0.2, y_offset - 0.2, 0),
      Vec3f(-0.5 + max_len - i + 0.2, y_offset - 0.2, 0),
      Vec3f(-0.5 + max_len/2, (max_len/2 - i)*sqrt(3) + y_offset + 0.2, 0) };
    //std::cout << "layer" << i << " is " << temp[0] << ' ' << temp[1] << ' ' << temp[2] << std::endl;
    layer_domains.push_back(temp);
  }

  for ( VertexIter v_it = layers[0].vertices_begin();
    v_it != layers[0].vertices_end(); v_it++ )
  {
    Vec3f curr = layers[0].vertex(*v_it);
    if ( !is_inside_triangle( curr, layer_domains[0] )
      || dist_btw_pts( layer_domains[0][0], curr ) < 1.5 // bottom layer is without corner faces
      || dist_btw_pts( layer_domains[0][1], curr ) < 1.5
      || dist_btw_pts( layer_domains[0][2], curr ) < 1.5 )
    {
      layers[0].delete_vertex( *v_it );
    }
  }
  layers[0].collect_garbage();
  for ( size_t i = 1; i < layers.size(); i++ )
  {
    for ( VertexIter v_it = layers[i].vertices_begin();
      v_it != layers[i].vertices_end(); v_it++ )
    {
      if ( !is_inside_triangle( layers[i].vertex(*v_it), layer_domains[i] ) )
      {
        layers[i].delete_vertex( *v_it );
      }
    }
    layers[i].collect_garbage();
  }

  for ( size_t i = 0; i < layers.size(); i++ )
  {
    write_obj( "tetlayer" + std::to_string(i), layers[i] );
  }

  return layers;
}

void
CoST::add_border_tets()
{
  vector< vector<VertexHandle> > border_tets;
  for ( FaceIter f_it = mesh.faces_begin(); f_it != mesh.faces_end(); f_it++ )
  {
    if ( mesh.incident_cell( mesh.halfface_handle( *f_it, 1 ) ) == CellHandle( -1 )
      && mesh.incident_cell( mesh.halfface_handle( *f_it, 0 ) ) == CellHandle( -1 ) )
    {
      vector<VertexHandle> new_cell;

      // push vertices around face to new cell
      for ( HalfFaceVertexIter hfv_it = mesh.hfv_iter( mesh.halfface_handle(*f_it, 1) );
        hfv_it.valid(); hfv_it++ )
      {
        new_cell.push_back( *hfv_it );
      }

      // add fourth vertex to new cell, which is centered above face
      Vec3f center = find_center( *f_it, mesh );
      // TODO fix hack, this just to test code
      if ( center[2] < 5 ) center[2] = center[2] - ( sqrt(3) / 2 );
      else center[2] = center[2] + ( sqrt(3) / 2 );
      new_cell.push_back( mesh.add_vertex( center ) );

      border_tets.push_back( new_cell );
    }
  }

  for ( size_t i = 0; i < border_tets.size(); i++ )
  {
    add_cell_polymesh( border_tets[i] );
  }
}

void
CoST::generate_tet_domain()
{
  // create 2D trihex layers
  vector<TetMeshV3f> layers, layers_tet_domain;
  layers = make_layers_012( (side_length*3) ); // TODO change back
  layers_tet_domain = make_layers_tet_domain( layers );

  stack_layers( layers_tet_domain, true );
  connect_layers();
  add_border_tets();
}
// ------------------------- end tet domain ------------------------------------




CellHandle
CoST::add_cell_polymesh(std::vector<VertexHandle> verts)
{
    HalfFaceHandle hf0, hf1, hf2, hf3;

    std::vector<VertexHandle> vs;

    vs.push_back(verts[0]);
    vs.push_back(verts[1]);
    vs.push_back(verts[2]);
    hf0 = mesh.halfface(vs);
    if(!hf0.is_valid()) {
        FaceHandle fh = mesh.add_face(vs);
        hf0 = mesh.halfface_handle(fh, 0);
    }
    vs.clear();

    vs.push_back(verts[0]);
    vs.push_back(verts[2]);
    vs.push_back(verts[3]);
    hf1 = mesh.halfface(vs);
    if(!hf1.is_valid()) {
        FaceHandle fh = mesh.add_face(vs);
        hf1 = mesh.halfface_handle(fh, 0);
    }
    vs.clear();

    vs.push_back(verts[0]);
    vs.push_back(verts[3]);
    vs.push_back(verts[1]);
    hf2 = mesh.halfface(vs);
    if(!hf2.is_valid()) {
        FaceHandle fh = mesh.add_face(vs);
        hf2 = mesh.halfface_handle(fh, 0);
    }
    vs.clear();

    vs.push_back(verts[1]);
    vs.push_back(verts[3]);
    vs.push_back(verts[2]);
    hf3 = mesh.halfface(vs);
    if(!hf3.is_valid()) {
        FaceHandle fh = mesh.add_face(vs);
        hf3 = mesh.halfface_handle(fh, 0);
    }
    vs.clear();

    assert(hf0.is_valid());
    assert(hf1.is_valid());
    assert(hf2.is_valid());
    assert(hf3.is_valid());

    std::vector<HalfFaceHandle> hfs;
    hfs.push_back(hf0);
    hfs.push_back(hf1);
    hfs.push_back(hf2);
    hfs.push_back(hf3);

    return mesh.add_cell(hfs, false);
}





// =============================================================================
// =============================================================================


// =============================================================================
// =============================================================================


// =============================================================================
// =============================================================================


// =============================================================================
// =============================================================================

// ------------------------------------- irregularities






