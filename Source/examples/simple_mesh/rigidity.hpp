/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef __RIGIDITY_HH__
#define __RIGIDITY_HH__

// Eigen Headers
#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/LU>

#include <OpenVolumeMesh/Mesh/PolyhedralMesh.hh>
typedef OpenVolumeMesh::GeometricPolyhedralMeshV3f   TetMeshV3f;

typedef Eigen::SparseMatrix<double> SparseMatrixd;
typedef Eigen::SparseQR<SparseMatrixd, Eigen::COLAMDOrdering<int>> SparseQRCol;

template <typename M>
class RigidityMatrix
{
public:
  RigidityMatrix(M &mesh);

  ~RigidityMatrix();

  SparseMatrixd &get_matrix();

  unsigned get_rank();
  Eigen::MatrixXd solve(Eigen::MatrixXd &rhs);

private:
  void mesh_to_matrix();
  SparseMatrixd r_matrix;
  M mesh;
  SparseQRCol qr_matrix;
};

// Convert an aribitrary mesh into a RigidityMatrix
template <typename M>
RigidityMatrix<M>::RigidityMatrix(M &mesh)
{
  this->mesh = mesh;
  unsigned vertices = mesh.n_vertices();
  unsigned edges = mesh.n_edges();

  //r_matrix.resize(3 * vertices, edges);
  //              rows     columns
  r_matrix.resize(edges, 3 * vertices);
  mesh_to_matrix();
}

template <typename M>
RigidityMatrix<M>::~RigidityMatrix() {}

template <typename M>
void
RigidityMatrix<M>::mesh_to_matrix()
{
  for (auto e_it = mesh.edges_begin(); e_it != mesh.edges_end(); ++e_it) {
    // edge number for row calculation
    unsigned e_num   = e_it->idx();
    auto edge        = mesh.edge(*e_it);

    // vertex number and actual vertex
    auto v_start_idx = edge.from_vertex().idx();
    auto v_start     = mesh.vertex(edge.from_vertex());
    auto v_end_idx   = edge.to_vertex().idx();
    auto v_end       = mesh.vertex(edge.to_vertex());

    for (unsigned i = 0; i < 3; ++i) {
      // calculate rigidity matrix
      //r_matrix.insert(3 * v_start_idx + i, e_num) = v_start[i] - v_end[i];
      //r_matrix.insert(3 * v_end_idx + i, e_num) = v_end[i] - v_start[i];
      r_matrix.insert(e_num, 3 * v_start_idx + i) = v_start[i] - v_end[i];
      r_matrix.insert(e_num, 3 * v_end_idx + i) = v_end[i] - v_start[i];
    }
  }

  r_matrix.makeCompressed();
  qr_matrix.compute(r_matrix);
}

template <typename M>
SparseMatrixd &
RigidityMatrix<M>::get_matrix()
{
  return r_matrix;
}

template <typename M>
unsigned
RigidityMatrix<M>::get_rank()
{
  return qr_matrix.rank();
}


void removeColumn(Eigen::MatrixXd& matrix, unsigned int colToRemove)
{
    unsigned int numRows = matrix.rows();
    unsigned int numCols = matrix.cols()-1;

    if( colToRemove < numCols )
        matrix.block(0,colToRemove,numRows,numCols-colToRemove) = matrix.rightCols(numCols-colToRemove);

    matrix.conservativeResize(numRows,numCols);
}

void removeRow(Eigen::MatrixXd& matrix, unsigned int rowToRemove)
{
    unsigned int numRows = matrix.rows()-1;
    unsigned int numCols = matrix.cols();

    if( rowToRemove < numRows )
        matrix.block(rowToRemove,0,numRows-rowToRemove,numCols) = matrix.bottomRows(numRows-rowToRemove);

    matrix.conservativeResize(numRows,numCols);
}

/*
 *  Solves F + omega*r_mat = 0 for omega
 *  omega = -F*(r_mat.inverse())
 */
template <typename M>
Eigen::MatrixXd
RigidityMatrix<M>::solve(Eigen::MatrixXd &F)
{
  Eigen::MatrixXd rigid_mat = r_matrix;
  Eigen::MatrixXd pseudo_inv = rigid_mat.completeOrthogonalDecomposition().pseudoInverse();
  
  return -1.0*F*pseudo_inv;
}


template <typename MATRIX>
void write_matrix_to_file(MATRIX& a_Matrix, string a_FileName)
{
    std::ofstream out_file;
    out_file.open( a_FileName );
    out_file << a_Matrix;
    out_file.close();
}




void testRigidityMatrixSolver(TetMeshV3f t_RigidMesh)
{
    int t_NumVerts = t_RigidMesh.n_vertices();   
    RigidityMatrix<TetMeshV3f> t_RigidityMatrix(t_RigidMesh);
    
    // create force vector
    auto t_Forces = Eigen::MatrixXd();
    t_Forces.setZero(1, 3*t_NumVerts);
    //      z-component of top vertex
    t_Forces(0, 3*(t_NumVerts-1)+2) = -1.0e-4;
    
    /*
     *  Write out edges for visualization in Matlab
     */
    std::ofstream out_file;
    out_file.open( "demo/RigidityMatrices/edges.csv");
    for (auto e_it = t_RigidMesh.edges_begin(); e_it != t_RigidMesh.edges_end(); ++e_it)
    {
        auto edge = t_RigidMesh.edge(*e_it);
        auto from_vertex = t_RigidMesh.vertex(edge.from_vertex());
        out_file << from_vertex[0] << ", " << from_vertex[1] << ", " << from_vertex[2] << endl;
        
        auto to_vertex = t_RigidMesh.vertex(edge.to_vertex());
        out_file << to_vertex[0] << ", " << to_vertex[1] << ", " << to_vertex[2] << endl;
    }
    out_file.close();
    
    /*
     *  Solve!
     */
    auto t_Solution = t_RigidityMatrix.solve(t_Forces);
    auto t_Solution_T = t_Solution.transpose();
    auto t_DenseRM = Eigen::MatrixXd(t_RigidityMatrix.get_matrix());

    auto t_Result = t_Solution*t_RigidityMatrix.get_matrix();
    auto t_Error = t_Forces + t_Result;

    
    write_matrix_to_file(t_Solution_T, "demo/RigidityMatrices/solution.txt");
    write_matrix_to_file(t_Solution.normalized(), "demo/RigidityMatrices/solution_normalized.txt");
    write_matrix_to_file(t_DenseRM, "demo/RigidityMatrices/dense_rigidity_matrix.txt");
    write_matrix_to_file(t_RigidityMatrix.get_matrix(), "demo/RigidityMatrices/sparse_rigidity_matrix.txt");
    write_matrix_to_file(t_Forces, "demo/RigidityMatrices/forces.txt");
}



#endif // __RIGIDITY_HH__
