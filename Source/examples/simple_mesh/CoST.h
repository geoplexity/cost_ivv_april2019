/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _COST_H_
#define _COST_H_

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cmath>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>
#include <limits>
#include <stdexcept>
#include <math.h>
#include <chrono>
#include <functional>

// Linear Alg
//#include "Eigen/Dense"
#include <Eigen/Dense>

// Include vector classes
#include <OpenVolumeMesh/Geometry/VectorT.hh>

// Include tetrahedral mesh kernel
// #include <OpenVolumeMesh/Mesh/TetrahedralMesh.hh>
#include <OpenVolumeMesh/Mesh/PolyhedralMesh.hh>

// File IO
#include <OpenVolumeMesh/FileManager/FileManager.hh>

// garbage collection
#include <OpenVolumeMesh/Attribs/StatusAttrib.hh>

// Make some typedefs to facilitate your life
typedef OpenVolumeMesh::Geometry::Vec3f         Vec3f;
typedef OpenVolumeMesh::Geometry::Vec4f         Vec4f;
// typedef OpenVolumeMesh::GeometricTetrahedralMeshV3f   TetMeshV3f;
typedef OpenVolumeMesh::GeometricPolyhedralMeshV3f   TetMeshV3f;

using namespace std;
using namespace OpenVolumeMesh;

const double epsilon = 0.1;
auto equalish = []( double a, double b ) { return fabs( a - b ) < epsilon; };

class CoST
{
private:
  TetMeshV3f mesh;
  double side_length;
  Vec3f origin;

public:
  /*
    Constructor:
    Takes the origin of the box,
    and the side length of the box
  */
  CoST( double side_length, Vec3f origin = Vec3f(0, 0, 0),
    bool tet = false )
    : side_length( side_length ), origin( origin )
  {
    mesh.enable_bottom_up_incidences( true );
    if ( tet )
    {
      generate_tet_domain();
    }
    else
    {
      generate_box();
    }
  };
  CoST( vector<Vec3f> );

  void write_obj( string );
  TetMeshV3f get_mesh() { return mesh; }
  void set_mesh(TetMeshV3f mesh) { this->mesh = mesh; }

private:
  class Stiffen;
public:
  void stiffen();
  void stiffen_one_face(vector<VertexHandle> corners);
  void pin_corner_to_face(VertexHandle a_Corner, vector<VertexHandle> a_Face);
  vector<VertexHandle> get_tet_corners();

private:
  class FileIO;

private:
  void generate_box();
  void shift_everything();
  void write_obj( string, TetMeshV3f );

  // ------------------------------ tet domain ---------------------------------
  double area_of_triangle( Vec3f, Vec3f, Vec3f );
  bool is_inside_triangle( Vec3f, vector<Vec3f> );
  vector<TetMeshV3f> make_layers_tet_domain( vector<TetMeshV3f> );
  void add_border_tets();
  void generate_tet_domain();
  // -------------------------- end tet domain ---------------------------------

  Vec3f find_center( FaceHandle, TetMeshV3f );
  TetMeshV3f remove_excess_2D_trihex( double, TetMeshV3f );
  vector<TetMeshV3f> make_layers_012( double );
  void stack_layers( vector<TetMeshV3f> layers, bool tet_domain = false );
  void add_tetrahedra( vector<FaceHandle>, vector<FaceHandle> );
  void connect_layers();
  void remove_extra_faces();
  void remove_extra_vertices();

  // ------------------------------------- irregularities
  void align_incident_tets( const vector<VertexHandle>&, vector<VertexHandle>&,
    VertexHandle );
  VertexHandle find_connecting_vertex( CellHandle, CellHandle );
  std::vector<VertexHandle> get_vhandles_except_connecting( CellHandle,
    VertexHandle );
  bool is_valid_pair_for_irreg( CellHandle, CellHandle );
  VertexHandle irreg( VertexHandle );
  void generate_irregularities();
  // ------------------------------------- end irregularities

  Vec3f midpt_btw_vert_handles( VertexHandle, VertexHandle );
  double dist_btw_pts( Vec3f v1, Vec3f v2 );
  double dist_btw_pts( VertexHandle, VertexHandle );

  CellHandle add_cell_polymesh(std::vector<VertexHandle> verts);

  VertexHandle step_around_hole(VertexHandle curr, VertexHandle prev, Vec3f hole_center);
  VertexHandle step_xy_dir_same_layer(VertexHandle curr, Vec3f x, Vec3f y);
  bool is_coplanar(CoST* cost, Vec3f v, Vec3f a, Vec3f b, Vec3f c);
  bool vec_same_dir(CoST* cost, Vec3f x, Vec3f y, Vec3f u, Vec3f v);
  void balance_entire_CoST();
  void avg_shift(VertexHandle connectingv);
  std::vector<VertexHandle> find_vertices_to_flip(Vec3f hole_center);
  vector<Eigen::Vector3d> get_planes_of_tet();

  // ----------------- refinements
  void make_cell_into_four_cells(CellHandle ch);
  Eigen::Vector3d vec2eig(Vec3f vec);
  Vec3f eig2vec(Eigen::Vector3d eig);
  TetMeshV3f scale_mesh(TetMeshV3f m, double scale);
  TetMeshV3f translate_mesh(TetMeshV3f m, Vec3f trans);

  Vec3f cross(Vec3f a_Vec1, Vec3f a_Vec2);
  double dot(Vec3f a_Vec1, Vec3f a_Vec2);
  double scalar_triple_product(Vec3f a_Vec1, Vec3f a_Vec2, Vec3f a_Vec3);
  array<double, 4> find_tetrahedron_bary_coords(const Vec3f a_Point, const vector<Vec3f> a_Tet);
  TetMeshV3f fill_linear_tet_with_CoST(const TetMeshV3f& a_Mesh, const vector<Vec3f> a_Tet);

public:
  
  void refinement2();

  void hole_by_irreg();

  vector<Eigen::Vector3d> get_corners_tet_mesoregion(); // TODO move to private
};


#endif
