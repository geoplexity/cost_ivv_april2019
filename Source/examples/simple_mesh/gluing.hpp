/*
This file is part of CoST.


CoST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


CoST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLUING_HH
#define GLUING_HH

#include <limits>
#include <cassert>
#include "connectivity.hpp"

namespace Gluing {

void merge_vertices(TetMeshV3f& mesh, VertexHandle vh1, VertexHandle vh2);
void get_adj_tet_corners(TetMeshV3f mesh1, std::vector<VertexHandle>& face1,
    TetMeshV3f mesh2, std::vector<VertexHandle>& face2);
void glue_two_tet_mesoregions(TetMeshV3f& mesh1, TetMeshV3f mesh2);
std::unordered_map<int, VertexHandle> copy_mesh_into_mesh(TetMeshV3f& mesh, TetMeshV3f other_mesh);

/**
 * copies other_mesh into mesh
 */
std::unordered_map<int, VertexHandle> 
copy_mesh_into_mesh(TetMeshV3f& mesh, TetMeshV3f other_mesh)
{
  std::unordered_map<int, VertexHandle> vert_map;
  for (VertexIter v_it=other_mesh.vertices_begin(); v_it!=other_mesh.vertices_end(); v_it++)
  {
    VertexHandle new_v = mesh.add_vertex(other_mesh.vertex(*v_it));
    std::ostringstream oss;
    oss << *v_it;
    int index = std::stoi(oss.str());
    vert_map[index] = new_v;
  }
  for (CellIter c_it=other_mesh.cells_begin(); c_it!=other_mesh.cells_end(); c_it++)
  {
    std::vector<VertexHandle> cell, new_cell;
    cell = Connectivity::get_vhandles_except_connecting(other_mesh, *c_it, VertexHandle(-1));
    for (size_t i = 0; i < 4; i++) 
    {
      std::ostringstream oss;
      oss << cell[i];
      int index = std::stoi(oss.str());
      new_cell.push_back(vert_map[index]);
    }
    Connectivity::add_cell_polymesh(mesh, new_cell);
  }
  return vert_map;
}

void
merge_vertices(TetMeshV3f& mesh, VertexHandle vh1, VertexHandle vh2)
{
    if (vh1 == VertexHandle(-1) || vh2 == VertexHandle(-1)) return;
    std::vector<VertexHandle> cell1 = Connectivity::get_vhandles_except_connecting(
        mesh, *mesh.vc_iter(vh1), vh1);
    Vec3f v1, v2;
    v1 = mesh.vertex(vh1); v2 = mesh.vertex(vh2);
    Vec3f new_coord( (v1[0]+v2[0])/2, (v1[1]+v2[1])/2, (v1[2]+v2[2])/2 );

    mesh.delete_vertex(vh1);

    cell1.push_back(vh2);
    Connectivity::add_cell_polymesh(mesh, cell1);
    mesh.set_vertex(vh2, new_coord);
}

/**
 * fills vectors face1 and face2 with the mapped corners of the faces of the
 * corresponding mesoregions that are adjacent (to be glued)
 */
void get_adj_tet_corners(TetMeshV3f mesh1, std::vector<VertexHandle>& face1,
    TetMeshV3f mesh2, std::vector<VertexHandle>& face2)
{
    std::vector<VertexHandle> corners1 = Connectivity::get_tet_corners(mesh1);
    std::vector<VertexHandle> corners2 = Connectivity::get_tet_corners(mesh2);
    // get rid of corners not on face
    std::pair<int, int> non_adj = std::make_pair(-1, -1);
    double max_dist = 0;
    for (size_t i = 0; i < 4; i++)
    {
        Vec3f c1 = mesh1.vertex(corners1[i]);
        for (size_t j = 0; j < 4; j++)
        {
            double temp = Connectivity::dist_btw_pts(c1, mesh2.vertex(corners2[j]));
            if (temp > max_dist)
            {
                max_dist = temp;
                non_adj = std::make_pair(i, j);
            }
        }
    }
    // std::cout << "non_adj: " << mesh1.vertex(*(corners1.begin() + non_adj.first)) << std::endl;
    // std::cout << "non_adj: " << mesh2.vertex(*(corners2.begin() + non_adj.second)) << std::endl;
    VertexHandle erased1 = corners1[non_adj.first];
    VertexHandle erased2 = corners2[non_adj.second];
    corners1.erase(corners1.begin() + non_adj.first);
    corners2.erase(corners2.begin() + non_adj.second);

    // pair off the rest
    for (size_t i = 0; i < 3; i++)
    {
        double min_dist = std::numeric_limits<double>::max();
        int closest = -1;
        Vec3f c1 = mesh1.vertex(corners1[i]);
        for (size_t j = i; j < 3; j++)
        {
            double temp = Connectivity::dist_btw_pts(c1, mesh2.vertex(corners2[j]));
            if (temp < min_dist)
            {
                min_dist = temp;
                closest = j;
            }
        }
        face1.push_back(corners1[i]);
        face2.push_back(corners2[closest]);
    }
    // push corner not in face to ends
    face1.push_back(erased1); face2.push_back(erased2);
}

/**
 * copies mesh2 into mesh1 and glues
 */
void glue_two_tet_mesoregions(TetMeshV3f& mesh1, TetMeshV3f mesh2)
{
    // put matched tet corners into face1, face2
    std::vector<VertexHandle> face1, face2;
    get_adj_tet_corners(mesh1, face1, mesh2, face2);
    // get ordered verts of gluing face
    std::vector<VertexHandle> ordered1, ordered2;
    ordered1 = Connectivity::order_vertices_oriented(mesh1, face1);
    ordered2 = Connectivity::order_vertices_oriented(mesh2, face2);
    // glue
    std::unordered_map<int, VertexHandle> vert_map = copy_mesh_into_mesh(mesh1, mesh2);
    for (size_t i=0; i<ordered1.size(); i++)
    {
        std::ostringstream oss;
        oss << ordered2[i];
        int index = std::stoi(oss.str());
        //std::cout << "merging " << mesh1.vertex(vert_map[index]) << "   " << mesh1.vertex(ordered1[i]) << std::endl;
        merge_vertices(mesh1, vert_map[index], ordered1[i]);
    }
    mesh1.collect_garbage();
}

TetMeshV3f test_adj(TetMeshV3f& mesh)
{
    std::vector<Vec3f> corners = Connectivity::get_corners_tet_mesoregion(mesh);
    Vec3f closest_to_origin = corners[0];
    for (size_t i=1; i<4; i++)
    {
        if (Connectivity::dist_btw_pts(Vec3f(0, 0, 0), corners[i]) 
                < Connectivity::dist_btw_pts(Vec3f(0, 0, 0), closest_to_origin))
            closest_to_origin = corners[i];
    }
    for (VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); v_it++)
    {
        Vec3f oldv = mesh.vertex(*v_it);
        mesh.set_vertex(*v_it, Vec3f(oldv[0]-closest_to_origin[0], 
            oldv[1]-closest_to_origin[1], oldv[2]-closest_to_origin[2]));
    }

    TetMeshV3f mirror(mesh);
    for (VertexIter v_it = mirror.vertices_begin(); v_it != mirror.vertices_end(); v_it++)
    {
        Vec3f oldv = mirror.vertex(*v_it);
        mirror.set_vertex(*v_it, Vec3f(oldv[0], oldv[1], -1*oldv[2]));
    }

    std::vector<VertexHandle> face1, face2;
    get_adj_tet_corners(mesh, face1, mirror, face2);
    //for (VertexHandle vh : face1) std::cout << "face1 " << mesh.vertex(vh) << std::endl;
    //for (VertexHandle vh : face2) std::cout << "face2 " << mirror.vertex(vh) << std::endl;
    //std::cout << std::endl;
    // order vertices
    // std::vector<VertexHandle> ordered1, ordered2;
    // ordered1 = Connectivity::order_vertices_oriented(mesh, face1);
    // ordered2 = Connectivity::order_vertices_oriented(mirror, face2);
    // assert(ordered1.size() == ordered2.size());
    // for (size_t i=0; i<ordered1.size(); i++)
    // {
    //     std::cout << "ordered1 " << mesh.vertex(ordered1[i]) << std::endl;
    //     std::cout << "ordered2 " << mirror.vertex(ordered2[i]) << std::endl;
    // }

    glue_two_tet_mesoregions(mesh, mirror);

    return mirror;
}

} // namespace Gluing

#endif
